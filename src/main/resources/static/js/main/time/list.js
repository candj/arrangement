

			var colNames = [
				"シフトID",
				"会社ID",
				"開始時間",
				"终了時間",
				"false:未削除 true:削除",
			];
			var colModel = [
				{
					name: "timeId",
					index: "timeId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "companyId",
					index: "companyId",
					width: 100,
					hidden: true,
					search : false,
					align: "right",
				},
				{
					name: "startTime",
					index: "startTime",
					width: 100,
					search: false,
					align: "left",
					formatter : function(cellvalue, options, rowObject) {
						var date = new Date(cellvalue);
						return date.Format('hh:mm');
					},
					sortable: false,
				},
				{
					name: "endTime",
					index: "endTime",
					width: 100,
					search: false,
					align: "left",
					formatter : function(cellvalue, options, rowObject) {
						var date = new Date(cellvalue);
						return date.Format('hh:mm');
					},
					sortable: false,
				},
				{
					name: "deleteFlag",
					index: "deleteFlag",
					width: 100,
					hidden: true,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	