var context_path = $('meta[name=basepath]').attr('content');



var colNames = [
    "ID",
    "社員番号",
    "シフト名前",
    "社員名前",
    "優先順位",
    "時給単価(円)",
    "雇用契約書",
    "秘密保守契約",
    "年末調整",
    "在留カード",
    "签证",
    "签证终了日",
    "40時間证明期限",
    "申请状態(开发者用)",
    "申请状態",
    "false:未削除 true:削除",
];
var colModel = [
    {
        name: "id",
        index: "id",
        width: 100,
        search : false,
        key: false,
        hidden: true,
        align: "center",
    },
    {
        name: "staff.staffCode",
        index: "staff.staffCode",
        width: 100,
        search : false,
        key: true,
        hidden: false,
        align: "left",
    },
    {
        name: "shiftwork.shiftworkName",
        index: "shiftwork.shiftworkName",
        hidden: true,
        width: 70,
        search : false,
        align: "center",
    },
    {
        name: "staff.staffName",
        index: "staff.staffName",
        width: 55,
        search : false,
        key: false,
        align: "left",
    },
    {
        name: "staff.priority",
        index: "staff.priority",
        width: 50,
        search : false,
        align: "right",
    },
    {
        name: "staff.salary",
        index: "staff.salary",
        width: 60,
        search : false,
        align: "right",
        formatter : function(cellvalue, options, rowObject){
            return "¥" + cellvalue;
        }
    },
    {
        name: "staff.contractFlag",
        index: "staff.contractFlag",
        width: 55,
        search : false,
        align: "center",
        formatter : function(cellvalue, options, rowObject){
            if(cellvalue = true){
                return "○"
            }else{
                return "X"
            }
        },
    },
    {
        name: "staff.ndaFlag",
        index: "staff.ndaFlag",
        width: 65,
        search : false,
        align: "center",
        formatter : function(cellvalue, options, rowObject){
            if(cellvalue = true){
                return "○"
            }else{
                return "X"
            }
        },
    },
    {
        name: "staff.yearEndAdjustmentFlag",
        index: "staff.yearEndAdjustmentFlag",
        width: 50,
        search : false,
        align: "center",
        formatter : function(cellvalue, options, rowObject){
            if(cellvalue = true){
                return "○"
            }else{
                return "X"
            }
        },
    },
    {
        name: "staff.residentCardFlag",
        index: "staff.residentCardFlag",
        width: 60,
        search : false,
        align: "center",
        formatter : function(cellvalue, options, rowObject){
            if(cellvalue = true){
                return "○"
            }else{
                return "X"
            }
        },
    },

    {
        name: "visaName",
        index: "visaName",
        width: 50,
        search : false,
        align: "left",
    },
    {
        name: "visaEndTime",
        index: "visaEndTime",
        width: 80,
        search : false,
        align: "left",
        formatter : function(cellvalue, options, rowObject) {
            if(cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM-dd');
            }else{
                return "";
            }
        },
    },{
        name: "staff.expireDateOf40Hour",
        index: "staff.expireDateOf40Hour",
        width: 80,
        search: false,
        align: "left",
        formatter : function(cellvalue, options, rowObject) {
            if(cellvalue != null) {
                var date = new Date(cellvalue);
                return date.Format('yyyy-MM-dd');
            }else{
                return "";
            }
        },
        sortable: false,
    },
    {
        name: "state",
        index: "state",
        width: 80,
        hidden: true,
    },
    {
        name: "stateForScrollBar",
        index: "stateForScrollBar",
        width: 80,
        formatter : function(cellvalue, options, rowObject){
            if(cellvalue == 0){
                return "未読";
            } else if(cellvalue == 1){
                return "未承認";
            } else if(cellvalue == 2){
                return "承認";
            } else{
                return "未申请";
            }
        },
        editable: true,
        edittype:"select",
        editoptions:{value:"0:未読;1:未承認;2:承認;\0:未申请"},
    },
    {
        name: "deleteFlag",
        index: "deleteFlag",
        width: 100,
        search : false,
        hidden: true,
        formatter: "checkbox",
        align: "center",
    },
];




openLayer = function (url) {
    var width = '800px'
    var height = '75%'
    layer.open({
        type: 2,
        title: ' ',
        shadeClose: false,
        shade: 0.6,
        area: [width, height],
        content: [url],
    });
};

var staffID = $('#staffId').val();

function detail(staffId) {

    var url = 'arrangement/detail/'+staffId;
    openLayer(url);

};

// function change(val){
//     var a =$(o).closeset('tr');
//     alert(rowId);
// };

var postData = {
    shiftworkName : $('#shiftworkName').val(),
};


// var onSelectRow= function(){
//     var r=jQuery("#grid").jqGrid('getRowData','staff1');//r为当前数据行
//         jQuery('#grid').editRow('staff1', true);
// };

var lastsel2;
var id1 = $('#staff.staffCode').val();

var onSelectRow = function(id1) {
    alert(id1);
    alert(lastsel2 + " lastSel2");
    if (id1 && (id1 != lastsel2)) {
        jQuery('#grid').jqGrid('restoreRow', lastsel2);
        jQuery('#grid').jqGrid('editRow', id1, true);
        lastsel2 = id1;
    } else{
        jQuery('#grid').jqGrid('restoreRow', id1);
    }
};

function change(val){
    var $grid= $("#grid");
    var rowKey = $grid.jqGrid('getGridParam',"selrow");
    // console.info(val);
    // alert(obj.value());
    $grid.setCell(rowKey,"state",val);
};

var editurl = "/api/commuteRest/editStatus";
var caption = "Input Types";

$(document).ready(function(){
    $.jgrid.defaults.styleUI = "Bootstrap";
    initGrid('grid', colNames, colModel, postData, onSelectRow, editurl, caption);
});
