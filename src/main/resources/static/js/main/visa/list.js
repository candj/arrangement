

			var colNames = [
				"签证ID",
				"签证名前",
				"最大労働時間",
				"已削除",
			];
			var colModel = [
				{
					name: "visaId",
					index: "visaId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "visaName",
					index: "visaName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "permittedWorkHourPerWeek",
					index: "permittedWorkHourPerWeek",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "deleteFlag",
					index: "deleteFlag",
					width: 100,
					search : false,
					hidden: true,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	