

			var colNames = [
				"ID",
				"シフトID",
				"スタッフID",
				"状態 0: 已申请 1:拒否 2:同意",
				"false:未削除 true:削除",
			];
			var colModel = [
				{
					name: "id",
					index: "id",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "shiftworkId",
					index: "shiftworkId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "staffId",
					index: "staffId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "state",
					index: "state",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "deleteFlag",
					index: "deleteFlag",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	