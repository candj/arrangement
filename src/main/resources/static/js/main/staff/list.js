

			var colNames = [
				"ID",
				"社員番号",
				"社員名前",
				"片仮名",
				"短期採用",
				"優先順位",
				"時給単価(円)",
				"希望支払い方式",
				"雇用契約書",
				"秘密保守契約",
				"年末調整",
				"在留カード",
				"在留カードコピー",
				"40時間证明期限",
				"签证",
				"签证终了日",
				"ステータス",
			];
			var colModel = [
				{
					name: "staffId",
					index: "staffId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "staffCode",
					index: "staffCode",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "staffName",
					index: "staffName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "katakana",
					index: "katakana",
					width: 100,
					hidden: true,
					search : false,
					align: "left",
				},
				{
					name: "shortTermFlag",
					index: "shortTermFlag",
					width: 100,
					search : false,
					align: "center",
					formatter : function(cellvalue, options, rowObject){
						if(cellvaue = true){
							return "○"
						}else{
							return "✕"
						}
					},
				},
				{
					name: "priority",
					index: "priority",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "salary",
					index: "salary",
					width: 100,
					search : false,
					align: "right",
					formatter : function(cellvalue, options, rowObject){
						return "¥" + cellvalue;
					},
				},
				{
					name: "payMethod",
					index: "payMethod",
					width: 120,
					search : false,
					align: "left",
					formatter : function(cellvalue, options, rowObject){
						if(cellvaue = 1){
							return "现金"
						}else{
							return "銀行"
						}
					},
				},
				{
					name: "contractFlag",
					index: "contractFlag",
					width: 100,
					search : false,
					align: "center",
					formatter : function(cellvalue, options, rowObject){
						if(cellvaue = true){
							return "○"
						}else{
							return "✕"
						}
					},
				},
				{
					name: "ndaFlag",
					index: "ndaFlag",
					width: 110,
					search : false,
					align: "center",
					formatter : function(cellvalue, options, rowObject){
						if(cellvaue = true){
							return "○"
						}else{
							return "✕"
						}
					},
				},
				{
					name: "yearEndAdjustmentFlag",
					index: "yearEndAdjustmentFlag",
					width: 100,
					search : false,
					align: "center",
					formatter : function(cellvalue, options, rowObject){
						if(cellvaue = true){
							return "○"
						}else{
							return "✕"
						}
					},
				},
				{
					name: "residentCardFlag",
					index: "residentCardFlag",
					width: 100,
					search : false,
					align: "center",
					formatter : function(cellvalue, options, rowObject){
						if(cellvaue = true){
							return "○"
						}else{
							return "✕"
						}
					},
				},
				{
					name: "residentCardCopyFlag",
					index: "residentCardCopyFlag",
					width: 130,
					search : false,
					align: "center",
					formatter : function(cellvalue, options, rowObject){
						if(cellvaue = true){
							return "○"
						}else{
							return "✕"
						}
					},
				},
				{
					name: "expireDateOf40Hour",
					index: "expireDateOf40Hour",
					width: 120,
					search: false,
					align: "left",
					formatter : function(cellvalue, options, rowObject) {
						if(cellvalue != null) {
							var date = new Date(cellvalue);
							return date.Format('yyyy-MM-dd');
						}else{
							return "";
						}
					},
					sortable: false,
				},
				{
					name: "visaName",
					index: "visaName",
					width: 60,
					search : false,
					align: "left",
				},
				{
					name: "visaEndTime",
					index: "visaEndTime",
					width: 100,
					search : false,
					align: "left",
					formatter : function(cellvalue, options, rowObject) {
						if(cellvalue != null) {
							var date = new Date(cellvalue);
							return date.Format('yyyy-MM-dd');
						}else{
							return "";
						}
					},
				},
				{
					name: "deleteFlag",
					index: "deleteFlag",
					hidden: true,
					width: 100,
					search : false,
					align: "left",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	

