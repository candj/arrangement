

			var colNames = [
				"ID",
				"会社コード",
				"会社名",
				"住所",
				"郵便番号 郵便番号 郵便番号",
				"電話番号",
				"単金",
				"交通費",
				"ステータス",
			];
			var colModel = [
				{
					name: "companyId",
					index: "companyId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "companyCode",
					index: "companyCode",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "companyName",
					index: "companyName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "companyAdress",
					index: "companyAdress",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "postCode",
					index: "postCode",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "telephoneNumber",
					index: "telephoneNumber",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "salary",
					index: "salary",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "commutingCost",
					index: "commutingCost",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "status",
					index: "status",
					width: 100,
					search : false,
					align: "left",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	

