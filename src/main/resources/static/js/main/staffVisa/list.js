

			var colNames = [
				"ID",
				"スタッフ名前",
				"签证名前",
				"终了日",
				"false:未削除 true:削除",
			];
			var colModel = [
				{
					name: "id",
					index: "id",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "staff.staffName",
					index: "staff.staffName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "visa.visaName",
					index: "visa.visaName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "endTime",
					index: "endTime",
					width: 100,
					search: false,
					align: "left",
					formatter : function(cellvalue, options, rowObject) {
						var date = new Date(cellvalue);
						return date.Format('yyyy-MM-dd');
					},
					sortable: false,
				},
				{
					name: "deleteFlag",
					index: "deleteFlag",
					width: 100,
					hidden: true,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	