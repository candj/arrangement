

			var colNames = [
				"ファイルID",
				"ストレージ パス",
				"カテゴリ:1:資格外証明書  2:40時間证明 ",
				"状態 0:未読 1:未承認　2:承認",
				"スタッフID",
				"false:未削除 true:削除",
			];
			var colModel = [
				{
					name: "fileId",
					index: "fileId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "storePath",
					index: "storePath",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "category",
					index: "category",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "state",
					index: "state",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "staffId",
					index: "staffId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "deleteFlag",
					index: "deleteFlag",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	