

			var colNames = [
				"ID",
				"ユーザーID",
				"ロールID",
			];
			var colModel = [
				{
					name: "id",
					index: "id",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "userId",
					index: "userId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "roleId",
					index: "roleId",
					width: 100,
					search : false,
					align: "right",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	

