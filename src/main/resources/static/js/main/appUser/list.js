

			var colNames = [
				"管理者ID",
				"ロールコード",
				"管理者名",
				"パスワード",
				"会社ID",
				"false:未削除 true:削除",
			];
			var colModel = [
				{
					name: "userId",
					index: "userId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "userCode",
					index: "userCode",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "userName",
					index: "userName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "password",
					index: "password",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "companyId",
					index: "companyId",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "deleteFlag",
					index: "deleteFlag",
					width: 100,
					search : false,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	