

			var colNames = [
				"タスクID",
				"名前",
				"開始日",
				"终了日",
				"予想人数",
				"会社ID",
				"false:未削除 true:削除",
			];
			var colModel = [
				{
					name: "taskId",
					index: "taskId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "right",
				},
				{
					name: "taskName",
					index: "taskName",
					width: 100,
					search : false,
					align: "left",
				},
				{
					name: "startTime",
					index: "startTime",
					width: 100,
					search: false,
					align: "left",
					formatter : function(cellvalue, options, rowObject) {
						var date = new Date(cellvalue);
						return date.Format('yyyy-MM-dd');
					},
					sortable: false,
				},
				{
					name: "endTime",
					index: "endTime",
					width: 100,
					search: false,
					align: "left",
					formatter : function(cellvalue, options, rowObject) {
						var date = new Date(cellvalue);
						return date.Format('yyyy-MM-dd');
					},
					sortable: false,
				},
				{
					name: "expectWorkforce",
					index: "expectWorkforce",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "companyId",
					index: "companyId",
					width: 100,
					search : false,
					hidden: true,
					align: "right",
				},
				{
					name: "deleteFlag",
					index: "deleteFlag",
					width: 100,
					search : false,
					hidden: true,
					formatter: "checkbox",
					align: "center",
				},
			];

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel);
});	
	