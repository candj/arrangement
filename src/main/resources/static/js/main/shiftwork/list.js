

			var colNames = [
				"シフトID",
				"名前",
				"開始時間",
				"終了時間",
				"会社ID",
				"タスクID",
				"需要人数",
				"已承認",
				"已拒否",
				"承認待ち",
				"シフト作業管理",
				"false:未削除 true:削除",
			];
			var colModel = [
				{
					name: "shiftworkId",
					index: "shiftworkId",
					width: 100,
					search : false,
					key: true,
					hidden: true,
					align: "center",
				},
				{
					name: "shiftworkName",
					index: "shiftworkName",
					width: 140,
					search : false,
					align: "left",
				},
				{
					name: "startTime",
					index: "startTime",
					width: 100,
					search: false,
					align: "left",
					formatter : function(cellvalue, options, rowObject) {
						var date = new Date(cellvalue);
						return date.Format('yyyy-MM-dd hh:mm');
					},
					sortable: false,
				},
				{
					name: "endTime",
					index: "endTime",
					width: 100,
					search: false,
					align: "left",
					formatter : function(cellvalue, options, rowObject) {
						var date = new Date(cellvalue);
						return date.Format('yyyy-MM-dd hh:mm');
					},
					sortable: false,
				},
				{
					name: "companyId",
					index: "companyId",
					width: 100,
					search : false,
					hidden: true,
					align: "center",
				},
				{
					name: "taskId",
					index: "taskId",
					width: 100,
					hidden: true,
					search : false,
					align: "center",
				},
				{
					name: "task.expectWorkforce",
					index: "task.expectWorkforce",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "enrolled",
					index: "enrolled",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "rejected",
					index: "rejected",
					width: 100,
					search : false,
					align: "right",

				},
				{
					name: "unread",
					index: "unread",
					width: 100,
					search : false,
					align: "right",
				},
				{
					name: "setNull",
					index: "setNull",
					width: 100,
					search : false,
					align: "left",
					// formatter: function(cellvalue,options,rowObject){
					// 	cellvalue = '<button class="btn btn-success m-l-lg m-t-xs" type="button" ' +
					// 		'onclick="arrange('+rowObject.shiftworkId+')" id="status" data--grid-id="grid">詳細<button>'
					// 	return cellvalue;
					// },
					formatter: function(cellvalue,options,rowObject){
						var selectHtml = '<a herf="javascript:void(0);"' +
							'onclick = "arrange('+rowObject.shiftworkId+')">詳細</a>';
						return selectHtml;
					},
				},
				{
					name: "deleteFlag",
					index: "deleteFlag",
					width: 100,
					search : false,
					hidden: true,
					formatter: "checkbox",
					align: "center",
				},
			];

			openLayer = function (url) {
				var width = '1200px'
				var height = '85%'
				layer.open({
					type: 2,
					title: 'スタッフ_シフト一覧画面',
					shadeClose: false,
					shade: 0.6,
					area: [width, height],
					content: [url],
				});
			};
			function arrange(id){
				var url = 'arrangement/list/'+id;
				openLayer(url);
			}



			var gridComplete = function() {
				$('.emptyCell').each(
					function(index, element) {
						$(this).closest('td').addClass('btn-update emptyTdCell').attr(
							'data-url', $(this).attr('data-url'));
					})
				$('.btn-update').click(function() {
					var width = '640px'
					var height = '90%'
					layer.open({
						type : 2,
						title : ' ',
						shadeClose : false,
						shade : 0.6,
						area : [ width, height ],
						content : [ $(this).attr("data-url") ],
					})
					return false;

				});
			};

$(document).ready(function(){
	$.jgrid.defaults.styleUI = "Bootstrap";
	initGrid('grid', colNames, colModel,gridComplete);
});	
	