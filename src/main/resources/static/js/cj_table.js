;(function (factory) {
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory)
  } else {
    factory(jQuery)
  }
})(function ($) {
  $.extend($.fn, {
    cjtable: function (options) {
      if (!this.length) {
        if (options && options.debug && window.console) {
          console.warn("Nothing selected, can't xctable, returning nothing.")
        }
        return
      }

      return $.cjtable(options, this)
    },
  })

  $.cjtable = function (options, table) {
    swal = top.swal
    toast = top.toastr
    layer = top.layer

    var defaults = {
        layer: {},
        add: '.btn-add',
        delete: '.btn-delete',
        update: '.btn-update',
        pull: '.btn-pull',
        postingIcon: 'fa fa-refresh fa-spin fa-fw margin-bottom',
        deleteConfirm: 'データを削除してもよろしいですか?',
      },
      init = function () {
        var pagerId = `#${table[0].id}_pager`
        // tableにadd,delete,updateボタンを追加する
        // ツイルボタンを作成します
    	if (defaults.opts.detailUrl) {
            table.navButtonAdd(`${pagerId}`, {
              caption: '詳細',
              buttonicon: 'glyphicon-plus ui-icon-detail',
              onClickButton: ondetail,
              position: 'first',
            })
        }
        if (defaults.opts.deleteUrl) {
          table.navButtonAdd(`${pagerId}`, {
            caption: '削除',
            buttonicon: 'glyphicon-trash ui-icon-delete',
            onClickButton: ondelete,
            position: 'first',
          })
        }
        if (defaults.opts.editUrl) {
          table.navButtonAdd(`${pagerId}`, {
            caption: '編集',
            buttonicon: 'glyphicon-edit ui-icon-edit',
            onClickButton: onupdate,
            position: 'first',
          })
        }
        if (defaults.opts.addUrl) {
          table.navButtonAdd(`${pagerId}`, {
            caption: '追加',
            buttonicon: 'glyphicon-plus ui-icon-add',
            onClickButton: onadd,
            position: 'first',
          })
        }
        return false
      },
      getKeyValue = function () {
        var rowid = table.jqGrid('getGridParam', 'selrow')
        var key = table.data('key')
        if (!key) {
          layer.msg('主キーを設置をしてください。')
          return null
        }
        if (!rowid) {
          layer.msg('データを選択してください。')
          return null
        }

        var rowData = table.jqGrid('getRowData', rowid)
        var vals = {}
        var keys = key.split(',')
        for (var i = 0; i < keys.length; i++) {
          vals[keys[i]] = rowData[keys[i]]
        }
        return vals
      },
      openLayer = function (url) {
        var width = defaults.layer.width || '640px'
        var height = defaults.layer.height || '90%'
        layer.open({
          type: 2,
          title: ' ',
          shadeClose: false,
          shade: 0.6,
          area: [width, height],
          content: [url],
        })
      },
      resetSubmit = function (btn) {
        btn.removeAttr('disabled')
        return false
      },
      disabledSubmit = function (btn) {
        btn.attr('disabled', 'disabled')
        return false
      },
      del = function (options) {
        var ld,
          layer = top.layer
        $.ajax({
          url: options.url,
          type: 'DELETE',
          data: options.data,
          beforeSend: function () {
            ld = layer.load(3)
            disabledSubmit(options.target)
          },
          success: function (response) {
            if (top.toastr)
              top.toastr.success(
                null,
                '操作完了しました。'
              )
              
              swal("削除しました", "", "success");

            $('.glyphicon-refresh').click()
          },
          error: function (e) {
            var message = null
            var res = e.responseJSON
            if (res) {
              message = res.message || '処理を失敗しました。'
              if (top.swal) top.swal(message, '', 'warning')
            } else {
              message = 'システムエラーが発生しました。'
              if (top.swal) top.swal(message, '', 'error')
            }
          },
          complete: function () {
            layer.close(ld)
            resetSubmit(options.target)
          },
        })
        return false
      },
      onadd = function () {
        openLayer(defaults.basepath + defaults.opts.addUrl)
        return false
      },
      onupdate = function () {
        var url = defaults.basepath + defaults.opts.editUrl
        var data = getKeyValue()
        if (!data) {
          return false
        }

        var params = []
        $.each(data, function (n, v) {
          params.push(v)
        })

        url = url + '/' + params.join('/')
        openLayer(url)
        return false
      },
      ondetail = function () {
          var url = defaults.basepath + defaults.opts.detailUrl
          var data = getKeyValue()
          if (!data) {
            return false
          }
          url = url + '?' + $.param(data);
          parent.$("#managerOrderProduct").attr("href",url);
          parent.$("#managerOrderProduct").click();
          
          return false
      },
      ondelete = function () {
        var url = defaults.basepath + defaults.opts.deleteUrl
        var confirm = defaults.deleteConfirm
        var txt = '削除してもよろしいですか。'

        var data = getKeyValue()
        if (!data) {
          return false
        }
        var params = []
        $.each(data, function (n, v) {
          params.push(v)
        })
        url = url + '/' + params.join('/')
        swal(
          {
            title: confirm,
            text: txt,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: '削除',
            cancelButtonText: 'キャンセル',
            closeOnConfirm: false,
          },
          function (isConfirm) {
            if (isConfirm) {
              del({
                target: top.$('.showSweetAlert').find('.confirm'),
                url: url
              })
            }
          }
        )

        return false
      }

    $.extend(defaults, options)

    init()
  }
})


$(document).ready(function() {

	$('#searchForGrid').click(function(){		
		var submitData = $(this).closest("form").serializeJSON();
		var gridId = $(this).data("grid-id");
		$("#"+ gridId).setGridParam({
			postData:JSON.parse(submitData)
		}).trigger("reloadGrid");  
		
		return false;
	});
	
})