function initGrid(grid_id, grid_colNames, grid_colModel,grid_params,grid_onSelectRow, grid_editurl, grid_caption) {
  var jGridId = '#' + grid_id
  var jGridPagerId = '#' + grid_id + '_pager'
  var basepath = top.$("meta[name='basepath']").attr('content') || '/'
  var jGrid = $(jGridId)
  var grid_height = $(document.body).height() - 240;
  eval(`var opts=${jGrid.data('opt')}`)
  if (!opts) {
    opts = { loadUrl: '', addUrl: '', editUrl: '', deleteUrl: '' }
  };

  grid_params = grid_params || {};

  // insert page div
  var url = opts.loadUrl;
  if(url.startsWith("/")) {
	  url = url.substr(1);
  }

  $('<div id="' + grid_id + '_pager"></div>').insertAfter(jGridId)

  var jqgrid = jGrid.jqGrid({
    mtype: 'post',
    url: basepath + url,
    datatype: 'json',
    postData:grid_params,
    height: grid_height,
    autowidth: true,
    shrinkToFit: true,
    rowNum: 20,
    rowList: [10, 20, 30],
    colNames: grid_colNames,
    colModel: grid_colModel,
    onSelectRow: grid_onSelectRow,
    editurl: grid_editurl,
    caption: grid_caption,
    jsonReader: {
      repeatitems: false,
      root: 'list',
      page: 'page_num',
      total: 'pages',
      records: 'total',
    },
    pager: jGridPagerId,
    viewrecords: true,
    hidegrid: false,
    loadError: function (xhr, status, error) {
     // top.swal('データを取得できません。', '', 'error')
    },
  })

  jqgrid.jqGrid(
    'navGrid',
    jGridPagerId,
    {
      edit: false,
      add: false,
      del: false,
      search: false,
      refresh: true,
    },
    {
      height: grid_height,
      reloadAfterSubmit: true,
    }
  )

  // テーブルの初期化
  $(jGridId).cjtable({ opts: opts, basepath: basepath })

  $(".ui-jqgrid-bdiv").height("auto");
  $(".ui-jqgrid-bdiv").css("min-height","300px");
  return jqgrid
}
