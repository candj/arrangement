package com.candj.arrangement.controller;

import com.candj.arrangement.dao.mapper.StaffDao;
import com.candj.arrangement.dao.mapper.StaffShiftDao;
import com.candj.arrangement.dao.mapper.TaskDao;
import com.candj.arrangement.dto.ShiftworkDto;
import com.candj.arrangement.dto.StaffDto;
import com.candj.arrangement.dto.StaffShiftDto;
import com.candj.arrangement.mapper.model.Shiftwork;
import com.candj.arrangement.mapper.model.Staff;
import com.candj.arrangement.mapper.model.StaffShift;
import com.candj.arrangement.mapper.model.Task;
import com.candj.arrangement.service.ShiftworkService;
import com.candj.arrangement.service.StaffShiftService;
import java.util.List;
import javax.validation.Valid;

import com.candj.arrangement.webservice.ArrangementRestController;
import com.candj.webpower.web.core.model.WhereCondition;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * スタッフ_シフト情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/arrangement")
@Controller
public class ArrangementController {
    @Autowired
    StaffShiftService staffShiftService;

    @Autowired
    StaffDao staffDao;

    @Autowired
    StaffShiftDao staffShiftDao;

    @Autowired
    ShiftworkService shiftworkService;

    @Autowired
    ArrangementRestController arrangementRestController;

    @Autowired
    TaskDao taskDao;

    /**
     * スタッフ_シフトを新規追加する。
     * @param staffShiftDto スタッフ_シフト
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(StaffShiftDto staffShiftDto, ModelMap model) {
        return new ModelAndView("arrangement/insert", model);
    }

    /**
     * スタッフ_シフトを新規追加(確認)する。
     * @param staffShiftDto スタッフ_シフト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid StaffShiftDto staffShiftDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffShift/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("staffShift/insertConfirm", model);
    }

    /**
     * スタッフ_シフトを新規追加(終了)する。
     * @param staffShiftDto スタッフ_シフト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid StaffShiftDto staffShiftDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffShift/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //スタッフ_シフトを新規追加する。
        int ret = staffShiftService.insertSelective(staffShiftDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/staffShift/insertFinish", model);
    }

    /**
     * スタッフ_シフトを編集する。
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{staffCode}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable String staffCode, ModelMap model) {
        //プライマリーキーでスタッフ_シフトを検索する。
//        StaffShiftDto staffShiftDto = staffShiftService.selectByPrimaryKey(id);
//        model.addAttribute("staffShiftDto", staffShiftDto);
//        return new ModelAndView("staffShift/edit", model);
        int id1 = 0;
        WhereCondition whereCondition = new WhereCondition();
        whereCondition.createCriteria()
                .andEqualTo("staff.staff_code", staffCode);
        List<Staff> staffs = staffDao.selectByExample(whereCondition);
        Staff staff = staffs.get(0);
        StaffShiftDto staffShiftDto = staffShiftService.selectByPrimaryKey(id1);
        model.addAttribute("staffShiftDto", staffShiftDto);
        return new ModelAndView("arrangement/change", model);

    }

    /**
     * スタッフ_シフトを編集(確認)する。
     * @param staffShiftDto スタッフ_シフト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid StaffShiftDto staffShiftDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffShift/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("staffShift/editConfirm", model);
    }

    /**
     * スタッフ_シフトを編集(終了)する。
     * @param staffShiftDto スタッフ_シフト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid StaffShiftDto staffShiftDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffShift/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでスタッフ_シフトを更新する。
        int ret = staffShiftService.updateByPrimaryKeySelective(staffShiftDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("staffShift/editFinish", model);
    }

    /**
     * スタッフ_シフト一覧画面を表示する。
     * @param staffShiftDto スタッフ_シフト
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list/{shiftworkId}", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("staffShiftDto") StaffShiftDto staffShiftDto,@PathVariable Integer shiftworkId, ModelMap model) {
        DateTime now = new DateTime();
        model.addAttribute("startTime", now);
        ShiftworkDto shiftworkDto = shiftworkService.selectByPrimaryKey(shiftworkId);
        Task task = taskDao.selectByPrimaryKey(shiftworkDto.getTaskId());
        int required = task.getExpectWorkforce();
        WhereCondition whereCondition1 = new WhereCondition();
        whereCondition1.createCriteria()
                .andEqualTo("staff_shift.shiftwork_id", shiftworkDto.getShiftworkId())
                .andEqualTo("staff_shift.state", 2);
        int enrolled = staffShiftDao.selectCountByExample(whereCondition1);
        WhereCondition whereCondition2 = new WhereCondition();
        whereCondition2.createCriteria()
                .andEqualTo("staff_shift.shiftwork_id", shiftworkDto.getShiftworkId())
                .andEqualTo("staff_shift.state", 1);
        int rejected = staffShiftDao.selectCountByExample(whereCondition2);
        WhereCondition whereCondition3 = new WhereCondition();
        whereCondition3.createCriteria()
                .andEqualTo("staff_shift.shiftwork_id", shiftworkDto.getShiftworkId())
                .andEqualTo("staff_shift.state", 0);
        int unread = staffShiftDao.selectCountByExample(whereCondition3);
        shiftworkDto.setRequired(required);
        shiftworkDto.setEnrolled(enrolled);
        shiftworkDto.setRejected(rejected);
        shiftworkDto.setUnread(unread);
        model.addAttribute("shiftworkDto", shiftworkDto);
        return new ModelAndView("arrangement/list", model);
    }

    @RequestMapping(value = "/detail/{staffId}")
    public ModelAndView detail(@PathVariable Integer staffId, ModelMap model){
        Staff staff = staffDao.selectByPrimaryKey(staffId);
        model.addAttribute("staffDto", staff);
        return new ModelAndView("arrangement/detail", model);
    }

    @RequestMapping(value = "list/arrangement/change/{staffShiftId}")
    public void change(@PathVariable Integer staffShiftId, ModelMap model){
        //TODO change mapping path
        StaffShift staffShift = staffShiftDao.selectByPrimaryKey(staffShiftId);
        arrangementRestController.insertInto(staffShift);
    }
}
