package com.candj.arrangement.controller;

import com.candj.arrangement.dto.FileDto;
import com.candj.arrangement.service.FileService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * ファイル情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/file")
@Controller
public class FileController {
    @Autowired
    FileService fileService;

    /**
     * ファイルを新規追加する。
     * @param fileDto ファイル
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(FileDto fileDto, ModelMap model) {
        return new ModelAndView("file/insert", model);
    }

    /**
     * ファイルを新規追加(確認)する。
     * @param fileDto ファイル
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid FileDto fileDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/file/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("file/insertConfirm", model);
    }

    /**
     * ファイルを新規追加(終了)する。
     * @param fileDto ファイル
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid FileDto fileDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/file/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //ファイルを新規追加する。
        int ret = fileService.insertSelective(fileDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/file/insertFinish", model);
    }

    /**
     * ファイルを編集する。
     * @param fileId ファイルID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{fileId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer fileId, ModelMap model) {
        //プライマリーキーでファイルを検索する。
        FileDto ileDto = fileService.selectByPrimaryKey(fileId);
        model.addAttribute("ileDto", ileDto);
        return new ModelAndView("file/edit", model);
    }

    /**
     * ファイルを編集(確認)する。
     * @param fileDto ファイル
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid FileDto fileDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/file/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("file/editConfirm", model);
    }

    /**
     * ファイルを編集(終了)する。
     * @param fileDto ファイル
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid FileDto fileDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/file/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでファイルを更新する。
        int ret = fileService.updateByPrimaryKeySelective(fileDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("file/editFinish", model);
    }

    /**
     * ファイル一覧画面を表示する。
     * @param fileDto ファイル
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("fileDto") FileDto fileDto, ModelMap model) {
        return new ModelAndView("file/list", model);
    }
}
