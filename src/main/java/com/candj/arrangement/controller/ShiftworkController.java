package com.candj.arrangement.controller;

import com.candj.arrangement.dto.ShiftworkDto;
import com.candj.arrangement.service.ShiftworkService;
import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * シフト情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/shiftwork")
@Controller
public class ShiftworkController {
    @Autowired
    ShiftworkService shiftworkService;

    /**
     * シフトを新規追加する。
     * @param shiftworkDto シフト
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(ShiftworkDto shiftworkDto, ModelMap model) {
        return new ModelAndView("shiftwork/insert", model);
    }

    /**
     * シフトを新規追加(確認)する。
     * @param shiftworkDto シフト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid ShiftworkDto shiftworkDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/shiftwork/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("shiftwork/insertConfirm", model);
    }

    /**
     * シフトを新規追加(終了)する。
     * @param shiftworkDto シフト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid ShiftworkDto shiftworkDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/shiftwork/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //シフトを新規追加する。
        int ret = shiftworkService.insertSelective(shiftworkDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/shiftwork/insertFinish", model);
    }

    /**
     * シフトを編集する。
     * @param shiftworkId シフトID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{shiftworkId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer shiftworkId, ModelMap model) {
        //プライマリーキーでシフトを検索する。
        ShiftworkDto hiftworkDto = shiftworkService.selectByPrimaryKey(shiftworkId);
        model.addAttribute("shiftworkDto", hiftworkDto);
        return new ModelAndView("shiftwork/edit", model);
    }

    /**
     * シフトを編集(確認)する。
     * @param shiftworkDto シフト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid ShiftworkDto shiftworkDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/shiftwork/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("shiftwork/editConfirm", model);
    }

    /**
     * シフトを編集(終了)する。
     * @param shiftworkDto シフト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid ShiftworkDto shiftworkDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/shiftwork/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでシフトを更新する。
        int ret = shiftworkService.updateByPrimaryKeySelective(shiftworkDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("shiftwork/editFinish", model);
    }

    /**
     * シフト一覧画面を表示する。
     * @param shiftworkDto シフト
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("shiftworkDto") ShiftworkDto shiftworkDto, ModelMap model) {
        model.addAttribute("startTime", null);
        return new ModelAndView("shiftwork/list", model);
    }
}
