package com.candj.arrangement.controller;

import com.candj.arrangement.dto.CompanyDto;
import com.candj.arrangement.service.CompanyService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 会社情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/company")
@Controller
public class CompanyController {
    @Autowired
    CompanyService companyService;

    /**
     * 会社を新規追加する。
     * @param companyDto 会社
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(CompanyDto companyDto, ModelMap model) {
        return new ModelAndView("company/insert", model);
    }

    /**
     * 会社を新規追加(確認)する。
     * @param companyDto 会社
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid CompanyDto companyDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/company/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("company/insertConfirm", model);
    }

    /**
     * 会社を新規追加(終了)する。
     * @param companyDto 会社
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid CompanyDto companyDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/company/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //会社を新規追加する。
        int ret = companyService.insertSelective(companyDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/company/insertFinish", model);
    }

    /**
     * 会社を編集する。
     * @param companyId 会社ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{companyId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer companyId, ModelMap model) {
        //プライマリーキーで会社を検索する。
        CompanyDto ompanyDto = companyService.selectByPrimaryKey(companyId);
        model.addAttribute("ompanyDto", ompanyDto);
        return new ModelAndView("company/edit", model);
    }

    /**
     * 会社を編集(確認)する。
     * @param companyDto 会社
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid CompanyDto companyDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/company/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("company/editConfirm", model);
    }

    /**
     * 会社を編集(終了)する。
     * @param companyDto 会社
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid CompanyDto companyDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/company/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで会社を更新する。
        int ret = companyService.updateByPrimaryKeySelective(companyDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("company/editFinish", model);
    }

    /**
     * 会社一覧画面を表示する。
     * @param companyDto 会社
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("companyDto") CompanyDto companyDto, ModelMap model) {
        return new ModelAndView("company/list", model);
    }
}
