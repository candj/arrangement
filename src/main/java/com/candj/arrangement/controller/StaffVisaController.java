package com.candj.arrangement.controller;

import com.candj.arrangement.dto.StaffVisaDto;
import com.candj.arrangement.service.StaffVisaService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * スタッフ_签证種類情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/staffVisa")
@Controller
public class StaffVisaController {
    @Autowired
    StaffVisaService staffVisaService;

    /**
     * スタッフ_签证種類を新規追加する。
     * @param staffVisaDto スタッフ_签证種類
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(StaffVisaDto staffVisaDto, ModelMap model) {
        return new ModelAndView("staffVisa/insert", model);
    }

    /**
     * スタッフ_签证種類を新規追加(確認)する。
     * @param staffVisaDto スタッフ_签证種類
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid StaffVisaDto staffVisaDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffVisa/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("staffVisa/insertConfirm", model);
    }

    /**
     * スタッフ_签证種類を新規追加(終了)する。
     * @param staffVisaDto スタッフ_签证種類
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid StaffVisaDto staffVisaDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffVisa/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //スタッフ_签证種類を新規追加する。
        int ret = staffVisaService.insertSelective(staffVisaDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/staffVisa/insertFinish", model);
    }

    /**
     * スタッフ_签证種類を編集する。
     * @param id ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer id, ModelMap model) {
        //プライマリーキーでスタッフ_签证種類を検索する。
        StaffVisaDto taffVisaDto = staffVisaService.selectByPrimaryKey(id);
        model.addAttribute("staffVisaDto", taffVisaDto);
        return new ModelAndView("staffVisa/edit", model);
    }

    /**
     * スタッフ_签证種類を編集(確認)する。
     * @param staffVisaDto スタッフ_签证種類
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid StaffVisaDto staffVisaDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffVisa/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("staffVisa/editConfirm", model);
    }

    /**
     * スタッフ_签证種類を編集(終了)する。
     * @param staffVisaDto スタッフ_签证種類
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid StaffVisaDto staffVisaDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffVisa/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでスタッフ_签证種類を更新する。
        int ret = staffVisaService.updateByPrimaryKeySelective(staffVisaDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("staffVisa/editFinish", model);
    }

    /**
     * スタッフ_签证種類一覧画面を表示する。
     * @param staffVisaDto スタッフ_签证種類
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("staffVisaDto") StaffVisaDto staffVisaDto, ModelMap model) {
        return new ModelAndView("staffVisa/list", model);
    }
}
