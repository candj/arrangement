package com.candj.arrangement.controller;

import com.candj.arrangement.dto.TimeDto;
import com.candj.arrangement.service.TimeService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 時間情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/time")
@Controller
public class TimeController {
    @Autowired
    TimeService timeService;

    /**
     * 時間を新規追加する。
     * @param timeDto 時間
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(TimeDto timeDto, ModelMap model) {
        return new ModelAndView("time/insert", model);
    }

    /**
     * 時間を新規追加(確認)する。
     * @param timeDto 時間
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid TimeDto timeDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/time/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("time/insertConfirm", model);
    }

    /**
     * 時間を新規追加(終了)する。
     * @param timeDto 時間
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid TimeDto timeDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/time/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //時間を新規追加する。
        int ret = timeService.insertSelective(timeDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/time/insertFinish", model);
    }

    /**
     * 時間を編集する。
     * @param timeId シフトID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{timeId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer timeId, ModelMap model) {
        //プライマリーキーで時間を検索する。
        TimeDto imeDto = timeService.selectByPrimaryKey(timeId);
        model.addAttribute("timeDto", imeDto);
        return new ModelAndView("time/edit", model);
    }

    /**
     * 時間を編集(確認)する。
     * @param timeDto 時間
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid TimeDto timeDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/time/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("time/editConfirm", model);
    }

    /**
     * 時間を編集(終了)する。
     * @param timeDto 時間
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid TimeDto timeDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/time/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで時間を更新する。
        int ret = timeService.updateByPrimaryKeySelective(timeDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("time/editFinish", model);
    }

    /**
     * 時間一覧画面を表示する。
     * @param timeDto 時間
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("timeDto") TimeDto timeDto, ModelMap model) {
        return new ModelAndView("time/list", model);
    }
}
