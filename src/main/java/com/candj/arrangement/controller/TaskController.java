package com.candj.arrangement.controller;

import com.candj.arrangement.dto.TaskDto;
import com.candj.arrangement.service.TaskService;
import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * タスク情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/task")
@Controller
public class TaskController {
    @Autowired
    TaskService taskService;

    /**
     * タスクを新規追加する。
     * @param taskDto タスク
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(TaskDto taskDto, ModelMap model) {
        return new ModelAndView("task/insert", model);
    }

    /**
     * タスクを新規追加(確認)する。
     * @param taskDto タスク
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid TaskDto taskDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/task/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("task/insertConfirm", model);
    }

    /**
     * タスクを新規追加(終了)する。
     * @param taskDto タスク
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid TaskDto taskDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/task/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //タスクを新規追加する。
        int ret = taskService.insertSelective(taskDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/task/insertFinish", model);
    }

    /**
     * タスクを編集する。
     * @param taskId タスクID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{taskId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer taskId, ModelMap model) {
        //プライマリーキーでタスクを検索する。
        TaskDto askDto = taskService.selectByPrimaryKey(taskId);
        model.addAttribute("taskDto", askDto);
        return new ModelAndView("task/edit", model);
    }

    /**
     * タスクを編集(確認)する。
     * @param taskDto タスク
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid TaskDto taskDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/task/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("task/editConfirm", model);
    }

    /**
     * タスクを編集(終了)する。
     * @param taskDto タスク
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid TaskDto taskDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/task/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでタスクを更新する。
        int ret = taskService.updateByPrimaryKeySelective(taskDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("task/editFinish", model);
    }

    /**
     * タスク一覧画面を表示する。
     * @param taskDto タスク
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("taskDto") TaskDto taskDto, ModelMap model) {
        model.addAttribute("timeSearch", null);
        return new ModelAndView("task/list", model);
    }
}
