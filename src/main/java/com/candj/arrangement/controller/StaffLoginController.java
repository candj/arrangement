package com.candj.arrangement.controller;

import com.candj.arrangement.dto.StaffShiftDto;
import com.candj.arrangement.service.StaffService;
import com.candj.arrangement.service.StaffShiftService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * スタッフ_シフト情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/staffLogin")
@Controller
public class StaffLoginController {
    @Autowired
    StaffService staffService;

    @RequestMapping(method = RequestMethod.PUT)
    public ModelAndView login(@RequestParam("username") String username, @RequestParam("password") String password) {
        return new ModelAndView("staffInterface/staffMain");
    }




}
