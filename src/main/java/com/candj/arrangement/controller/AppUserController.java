package com.candj.arrangement.controller;

import com.candj.arrangement.dto.AppUserDto;
import com.candj.arrangement.service.AppUserService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 管理者情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/appUser")
@Controller
public class AppUserController {
    @Autowired
    AppUserService appUserService;

    /**
     * 管理者を新規追加する。
     * @param appUserDto 管理者
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(AppUserDto appUserDto, ModelMap model) {
        return new ModelAndView("appUser/insert", model);
    }

    /**
     * 管理者を新規追加(確認)する。
     * @param appUserDto 管理者
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid AppUserDto appUserDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/appUser/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("appUser/insertConfirm", model);
    }

    /**
     * 管理者を新規追加(終了)する。
     * @param appUserDto 管理者
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid AppUserDto appUserDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/appUser/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //管理者を新規追加する。
        int ret = appUserService.insertSelective(appUserDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/appUser/insertFinish", model);
    }

    /**
     * 管理者を編集する。
     * @param userId 管理者ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{userId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer userId, ModelMap model) {
        //プライマリーキーで管理者を検索する。
        AppUserDto ppUserDto = appUserService.selectByPrimaryKey(userId);
        model.addAttribute("ppUserDto", ppUserDto);
        return new ModelAndView("appUser/edit", model);
    }

    /**
     * 管理者を編集(確認)する。
     * @param appUserDto 管理者
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid AppUserDto appUserDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/appUser/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("appUser/editConfirm", model);
    }

    /**
     * 管理者を編集(終了)する。
     * @param appUserDto 管理者
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid AppUserDto appUserDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/appUser/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで管理者を更新する。
        int ret = appUserService.updateByPrimaryKeySelective(appUserDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("appUser/editFinish", model);
    }

    /**
     * 管理者一覧画面を表示する。
     * @param appUserDto 管理者
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("appUserDto") AppUserDto appUserDto, ModelMap model) {
        return new ModelAndView("appUser/list", model);
    }
}
