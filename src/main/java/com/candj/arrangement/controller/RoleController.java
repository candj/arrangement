package com.candj.arrangement.controller;

import com.candj.arrangement.dto.RoleDto;
import com.candj.arrangement.service.RoleService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * ロール情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/role")
@Controller
public class RoleController {
    @Autowired
    RoleService roleService;

    /**
     * ロールを新規追加する。
     * @param roleDto ロール
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(RoleDto roleDto, ModelMap model) {
        return new ModelAndView("role/insert", model);
    }

    /**
     * ロールを新規追加(確認)する。
     * @param roleDto ロール
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid RoleDto roleDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/role/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("role/insertConfirm", model);
    }

    /**
     * ロールを新規追加(終了)する。
     * @param roleDto ロール
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid RoleDto roleDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/role/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //ロールを新規追加する。
        int ret = roleService.insertSelective(roleDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/role/insertFinish", model);
    }

    /**
     * ロールを編集する。
     * @param roleId ロールID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{roleId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer roleId, ModelMap model) {
        //プライマリーキーでロールを検索する。
        RoleDto oleDto = roleService.selectByPrimaryKey(roleId);
        model.addAttribute("oleDto", oleDto);
        return new ModelAndView("role/edit", model);
    }

    /**
     * ロールを編集(確認)する。
     * @param roleDto ロール
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid RoleDto roleDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/role/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("role/editConfirm", model);
    }

    /**
     * ロールを編集(終了)する。
     * @param roleDto ロール
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid RoleDto roleDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/role/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでロールを更新する。
        int ret = roleService.updateByPrimaryKeySelective(roleDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("role/editFinish", model);
    }

    /**
     * ロール一覧画面を表示する。
     * @param roleDto ロール
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("roleDto") RoleDto roleDto, ModelMap model) {
        return new ModelAndView("role/list", model);
    }
}
