package com.candj.arrangement.controller;

import com.candj.arrangement.dto.StaffDto;
import com.candj.arrangement.service.StaffService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * スタッフ情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/staff")
@Controller
public class StaffController {
    @Autowired
    StaffService staffService;

    /**
     * スタッフを新規追加する。
     * @param staffDto スタッフ
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(StaffDto staffDto, ModelMap model) {
        return new ModelAndView("staff/insert", model);
    }

    /**
     * スタッフを新規追加(確認)する。
     * @param staffDto スタッフ
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid StaffDto staffDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staff/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("staff/insertConfirm", model);
    }

    /**
     * スタッフを新規追加(終了)する。
     * @param staffDto スタッフ
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid StaffDto staffDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staff/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //スタッフを新規追加する。
        int ret = staffService.insertSelective(staffDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/staff/insertFinish", model);
    }

    /**
     * スタッフを編集する。
     * @param staffId スタッフID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{staffId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer staffId, ModelMap model) {
        //プライマリーキーでスタッフを検索する。
        StaffDto taffDto = staffService.selectByPrimaryKey(staffId);
        model.addAttribute("staffDto", taffDto);
        return new ModelAndView("staff/edit", model);
    }

    /**
     * スタッフを編集(確認)する。
     * @param staffDto スタッフ
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid StaffDto staffDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staff/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("staff/editConfirm", model);
    }

    /**
     * スタッフを編集(終了)する。
     * @param staffDto スタッフ
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid StaffDto staffDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staff/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでスタッフを更新する。
        int ret = staffService.updateByPrimaryKeySelective(staffDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("staff/editFinish", model);
    }

    /**
     * スタッフ一覧画面を表示する。
     * @param staffDto スタッフ
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("staffDto") StaffDto staffDto, ModelMap model) {
        model.addAttribute("thisUser", 1);
        return new ModelAndView("staff/list", model);
    }
}
