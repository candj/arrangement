package com.candj.arrangement.controller;

import com.candj.arrangement.dto.VisaDto;
import com.candj.arrangement.service.VisaService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * 签证種類情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/visa")
@Controller
public class VisaController {
    @Autowired
    VisaService visaService;

    /**
     * 签证種類を新規追加する。
     * @param visaDto 签证種類
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(VisaDto visaDto, ModelMap model) {
        return new ModelAndView("visa/insert", model);
    }

    /**
     * 签证種類を新規追加(確認)する。
     * @param visaDto 签证種類
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid VisaDto visaDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/visa/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("visa/insertConfirm", model);
    }

    /**
     * 签证種類を新規追加(終了)する。
     * @param visaDto 签证種類
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid VisaDto visaDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/visa/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //签证種類を新規追加する。
        int ret = visaService.insertSelective(visaDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/visa/insertFinish", model);
    }

    /**
     * 签证種類を編集する。
     * @param visaId 签证ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{visaId}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer visaId, ModelMap model) {
        //プライマリーキーで签证種類を検索する。
        VisaDto isaDto = visaService.selectByPrimaryKey(visaId);
        model.addAttribute("visaDto", isaDto);
        return new ModelAndView("visa/edit", model);
    }

    /**
     * 签证種類を編集(確認)する。
     * @param visaDto 签证種類
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid VisaDto visaDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/visa/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("visa/editConfirm", model);
    }

    /**
     * 签证種類を編集(終了)する。
     * @param visaDto 签证種類
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid VisaDto visaDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/visa/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーで签证種類を更新する。
        int ret = visaService.updateByPrimaryKeySelective(visaDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("visa/editFinish", model);
    }

    /**
     * 签证種類一覧画面を表示する。
     * @param visaDto 签证種類
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("visaDto") VisaDto visaDto, ModelMap model) {
        return new ModelAndView("visa/list", model);
    }
}
