package com.candj.arrangement.controller;

import com.candj.arrangement.dto.StaffShiftDto;
import com.candj.arrangement.service.StaffShiftService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * スタッフ_シフト情報追加、更新、削除、検索処理を行うコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/staffShift")
@Controller
public class StaffShiftController {
    @Autowired
    StaffShiftService staffShiftService;

    /**
     * スタッフ_シフトを新規追加する。
     * @param staffShiftDto スタッフ_シフト
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public ModelAndView insert(StaffShiftDto staffShiftDto, ModelMap model) {
        return new ModelAndView("staffShift/insert", model);
    }

    /**
     * スタッフ_シフトを新規追加(確認)する。
     * @param staffShiftDto スタッフ_シフト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/confirm", method = RequestMethod.POST)
    public ModelAndView insertConfirm(@Valid StaffShiftDto staffShiftDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffShift/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("staffShift/insertConfirm", model);
    }

    /**
     * スタッフ_シフトを新規追加(終了)する。
     * @param staffShiftDto スタッフ_シフト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/insert/finish", method = RequestMethod.POST)
    public ModelAndView insertFinish(@Valid StaffShiftDto staffShiftDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffShift/insert");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //スタッフ_シフトを新規追加する。
        int ret = staffShiftService.insertSelective(staffShiftDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("/staffShift/insertFinish", model);
    }

    /**
     * スタッフ_シフトを編集する。
     * @param id ID
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer id, ModelMap model) {
        //プライマリーキーでスタッフ_シフトを検索する。
        StaffShiftDto taffShiftDto = staffShiftService.selectByPrimaryKey(id);
        model.addAttribute("taffShiftDto", taffShiftDto);
        return new ModelAndView("staffShift/edit", model);
    }

    /**
     * スタッフ_シフトを編集(確認)する。
     * @param staffShiftDto スタッフ_シフト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/confirm", method = RequestMethod.POST)
    public ModelAndView editConfirm(@Valid StaffShiftDto staffShiftDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffShift/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        return new ModelAndView("staffShift/editConfirm", model);
    }

    /**
     * スタッフ_シフトを編集(終了)する。
     * @param staffShiftDto スタッフ_シフト
     * @param bindingResult バンディング結果
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/edit/finish", method = RequestMethod.POST)
    public ModelAndView editFinish(@Valid StaffShiftDto staffShiftDto, BindingResult bindingResult, ModelMap model) {
        if (bindingResult.hasErrors()) {
            ModelAndView mav = new ModelAndView("/staffShift/edit");
            mav.getModel().putAll(bindingResult.getModel());
            return mav;
        }
        //プライマリーキーでスタッフ_シフトを更新する。
        int ret = staffShiftService.updateByPrimaryKeySelective(staffShiftDto);
        model.addAttribute("ret", ret);
        return new ModelAndView("staffShift/editFinish", model);
    }

    /**
     * スタッフ_シフト一覧画面を表示する。
     * @param staffShiftDto スタッフ_シフト
     * @param model モデルマップ
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@ModelAttribute("staffShiftDto") StaffShiftDto staffShiftDto, ModelMap model) {
        return new ModelAndView("staffShift/list", model);
    }
}
