package com.candj.arrangement.service;

import com.candj.arrangement.dao.mapper.FileDao;
import com.candj.arrangement.dto.FileDto;
import com.candj.arrangement.mapper.model.File;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ファイル情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class FileService {
    @Autowired
    FileDao fileDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーでファイルを検索する。
     * @param fileId ファイルID
     * @return 結果
     */
    public FileDto selectByPrimaryKey(Integer fileId) {
        //プライマリーキーでファイルを検索する。
        File ret = fileDao.selectByPrimaryKey(fileId);
        return DozerHelper.map(mapper, ret, FileDto.class);
    }

    /**
     * プライマリーキーでファイルを検索する。（連携情報含む）
     * @param fileId ファイルID
     * @return 結果
     */
    public List<FileDto> selectAllByPrimaryKey(Integer fileId) {
        //プライマリーキーでファイル検索する。（連携情報含む）
        List<File> ret = fileDao.selectAllByPrimaryKey(fileId);
        return DozerHelper.mapList(mapper, ret, FileDto.class);
    }

    /**
     * 条件でファイルを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<FileDto> selectByExample(FileDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("file.file_id", dto.getFileId())
            .andEqualTo("file.store_path", dto.getStorePath())
            .andEqualTo("file.category", dto.getCategory())
            .andEqualTo("file.state", dto.getState())
            .andEqualTo("file.staff_id", dto.getStaffId())
            .andEqualTo("file.delete_flag", dto.getDeleteFlag())
            .andEqualTo("file.create_user", dto.getCreateUser())
            .andEqualTo("file.update_user", dto.getUpdateUser())
            .andEqualTo("file.create_time", dto.getCreateTime())
            .andEqualTo("file.update_time", dto.getUpdateTime());
        }
        //条件でファイルを検索する。（連携情報含む）)
        List<File> ret = fileDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, FileDto.class);
    }

    /**
     * 条件でファイルを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<FileDto> selectAllByExample(FileDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("file.file_id", dto.getFileId())
            .andEqualTo("file.store_path", dto.getStorePath())
            .andEqualTo("file.category", dto.getCategory())
            .andEqualTo("file.state", dto.getState())
            .andEqualTo("file.staff_id", dto.getStaffId())
            .andEqualTo("file.delete_flag", dto.getDeleteFlag())
            .andEqualTo("file.create_user", dto.getCreateUser())
            .andEqualTo("file.update_user", dto.getUpdateUser())
            .andEqualTo("file.create_time", dto.getCreateTime())
            .andEqualTo("file.update_time", dto.getUpdateTime());
        }
        //条件でファイルを検索する。（連携情報含む）)
        List<File> ret = fileDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, FileDto.class);
    }

    /**
     * ファイルを新規追加する。
     * @param dto ファイル
     * @return 結果
     */
    public int insertSelective(FileDto dto) {
        File file  =  mapper.map(dto, File.class);
        //ファイルを新規追加する。
        int ret = fileDao.insertSelective(file);
        return ret;
    }

    /**
     * ファイルを新規追加する。
     * @param dto ファイル
     * @return 結果
     */
    public int insert(FileDto dto) {
        File file  =  mapper.map(dto, File.class);
        //ファイルを新規追加する。
        int ret = fileDao.insert(file);
        return ret;
    }

    /**
     * プライマリーキーでファイルを更新する。
     * @param dto ファイル
     * @return 結果
     */
    public int updateByPrimaryKey(FileDto dto) {
        File file  =  mapper.map(dto, File.class);
        //プライマリーキーでファイルを更新する。
        int ret = fileDao.updateByPrimaryKey(file);
        return ret;
    }

    /**
     * プライマリーキーでファイルを更新する。
     * @param dto ファイル
     * @return 結果
     */
    public int updateByPrimaryKeySelective(FileDto dto) {
        File file  =  mapper.map(dto, File.class);
        //プライマリーキーでファイルを更新する。
        int ret = fileDao.updateByPrimaryKeySelective(file);
        return ret;
    }

    /**
     * ファイルを削除する。
     * @param fileId ファイルID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer fileId) {
        //ファイルを削除する。
        int ret = fileDao.deleteByPrimaryKey(fileId);
        return ret;
    }
}
