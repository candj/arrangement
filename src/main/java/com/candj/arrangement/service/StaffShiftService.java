package com.candj.arrangement.service;

import com.candj.arrangement.dao.mapper.StaffDao;
import com.candj.arrangement.dao.mapper.StaffShiftDao;
import com.candj.arrangement.dto.StaffShiftDto;
import com.candj.arrangement.dto.StaffVisaDto;
import com.candj.arrangement.mapper.model.Staff;
import com.candj.arrangement.mapper.model.StaffShift;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * スタッフ_シフト情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class StaffShiftService {
    @Autowired
    StaffShiftDao staffShiftDao;

    @Autowired
    Mapper mapper;

    @Autowired
    StaffDao staffDao;

    @Autowired
    StaffVisaService staffVisaService;

    /**
     * プライマリーキーでスタッフ_シフトを検索する。
     * @param id ID
     * @return 結果
     */
    public StaffShiftDto selectByPrimaryKey(Integer id) {
        //プライマリーキーでスタッフ_シフトを検索する。
        StaffShift ret = staffShiftDao.selectByPrimaryKey(id);
        return DozerHelper.map(mapper, ret, StaffShiftDto.class);
    }

    /**
     * プライマリーキーでスタッフ_シフトを検索する。（連携情報含む）
     * @param id ID
     * @return 結果
     */
    public List<StaffShiftDto> selectAllByPrimaryKey(Integer id) {
        //プライマリーキーでスタッフ_シフト検索する。（連携情報含む）
        List<StaffShift> ret = staffShiftDao.selectAllByPrimaryKey(id);
        return DozerHelper.mapList(mapper, ret, StaffShiftDto.class);
    }

    /**
     * 条件でスタッフ_シフトを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<StaffShiftDto> selectByExample(StaffShiftDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("staff_shift.id", dto.getId())
            .andEqualTo("staff_shift.shiftwork_id", dto.getShiftworkId())
            .andEqualTo("staff_shift.staff_id", dto.getStaffId())
            .andEqualTo("staff_shift.state", dto.getState())
            .andEqualTo("staff_shift.delete_flag", dto.getDeleteFlag())
            .andEqualTo("staff_shift.create_user", dto.getCreateUser())
            .andEqualTo("staff_shift.update_user", dto.getUpdateUser())
            .andEqualTo("staff_shift.create_time", dto.getCreateTime())
            .andEqualTo("staff_shift.update_time", dto.getUpdateTime());
        }
        //条件でスタッフ_シフトを検索する。（連携情報含む）)
        List<StaffShift> ret = staffShiftDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, StaffShiftDto.class);
    }

    public List<StaffShiftDto> selectSuitables(Integer id) {
        //条件でスタッフ_シフトを検索する。（連携情報含む）)
        List<StaffShift> ret = staffShiftDao.selectSuitables(id);
        ret.stream().forEach(x->
                {
                    String staffCode = x.getStaff().getStaffCode();
                    WhereCondition whereCondition1 = new WhereCondition();
                    whereCondition1.createCriteria()
                            .andEqualTo("staff.staff_code", staffCode);
                    List<Staff> staffs = staffDao.selectByExample(whereCondition1);
                    StaffVisaDto staffVisaDto = new StaffVisaDto();
                    staffVisaDto.setStaffId(staffs.get(0).getStaffId());
                    List<StaffVisaDto> visaInfo = staffVisaService.selectAllByExample(staffVisaDto);
                    if(visaInfo.size() > 0){
                        List<StaffVisaDto>longestDurationVisa = visaInfo.stream().sorted((x1,x2)->(x1.getEndTime().compareTo(x2.getEndTime()))).collect(Collectors.toList());
                        x.setVisaEndTime(longestDurationVisa.get(0).getEndTime());
                        x.setVisaName(longestDurationVisa.get(0).getVisa().getVisaName());
                    }
                    if(x.getState()!=null){
                        x.setStateForScrollBar(x.getState());
                    }
                    x.setStaffCode(x.getStaff().getStaffCode());
                }
        );
        return DozerHelper.mapList(mapper, ret, StaffShiftDto.class);
    }

    /**
     * 条件でスタッフ_シフトを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<StaffShiftDto> selectAllByExample(StaffShiftDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("staff_shift.id", dto.getId())
            .andEqualTo("staff_shift.shiftwork_id", dto.getShiftworkId())
            .andEqualTo("staff_shift.staff_id", dto.getStaffId())
            .andEqualTo("staff_shift.state", dto.getState())
            .andEqualTo("staff_shift.delete_flag", dto.getDeleteFlag())
            .andEqualTo("staff_shift.create_user", dto.getCreateUser())
            .andEqualTo("staff_shift.update_user", dto.getUpdateUser())
            .andEqualTo("staff_shift.create_time", dto.getCreateTime())
            .andEqualTo("staff_shift.update_time", dto.getUpdateTime());
        }
        //条件でスタッフ_シフトを検索する。（連携情報含む）)
        List<StaffShift> ret = staffShiftDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, StaffShiftDto.class);
    }

    /**
     * スタッフ_シフトを新規追加する。
     * @param dto スタッフ_シフト
     * @return 結果
     */
    public int insertSelective(StaffShiftDto dto) {
        StaffShift staffShift  =  mapper.map(dto, StaffShift.class);
        //スタッフ_シフトを新規追加する。
        int ret = staffShiftDao.insertSelective(staffShift);
        return ret;
    }

    /**
     * スタッフ_シフトを新規追加する。
     * @param dto スタッフ_シフト
     * @return 結果
     */
    public int insert(StaffShiftDto dto) {
        StaffShift staffShift  =  mapper.map(dto, StaffShift.class);
        //スタッフ_シフトを新規追加する。
        int ret = staffShiftDao.insert(staffShift);
        return ret;
    }

    /**
     * プライマリーキーでスタッフ_シフトを更新する。
     * @param dto スタッフ_シフト
     * @return 結果
     */
    public int updateByPrimaryKey(StaffShiftDto dto) {
        StaffShift staffShift  =  mapper.map(dto, StaffShift.class);
        //プライマリーキーでスタッフ_シフトを更新する。
        int ret = staffShiftDao.updateByPrimaryKey(staffShift);
        return ret;
    }

    /**
     * プライマリーキーでスタッフ_シフトを更新する。
     * @param dto スタッフ_シフト
     * @return 結果
     */
    public int updateByPrimaryKeySelective(StaffShiftDto dto) {
        StaffShift staffShift  =  mapper.map(dto, StaffShift.class);
        //プライマリーキーでスタッフ_シフトを更新する。
        int ret = staffShiftDao.updateByPrimaryKeySelective(staffShift);
        return ret;
    }

    /**
     * スタッフ_シフトを削除する。
     * @param id ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer id) {
        //スタッフ_シフトを削除する。
        int ret = staffShiftDao.deleteByPrimaryKey(id);
        return ret;
    }


}
