package com.candj.arrangement.service;

import com.candj.arrangement.dao.mapper.UserRoleDao;
import com.candj.arrangement.dto.UserRoleDto;
import com.candj.arrangement.mapper.model.UserRole;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ユーザールール情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class UserRoleService {
    @Autowired
    UserRoleDao userRoleDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーでユーザールールを検索する。
     * @param id ID
     * @return 結果
     */
    public UserRoleDto selectByPrimaryKey(Integer id) {
        //プライマリーキーでユーザールールを検索する。
        UserRole ret = userRoleDao.selectByPrimaryKey(id);
        return DozerHelper.map(mapper, ret, UserRoleDto.class);
    }

    /**
     * プライマリーキーでユーザールールを検索する。（連携情報含む）
     * @param id ID
     * @return 結果
     */
    public List<UserRoleDto> selectAllByPrimaryKey(Integer id) {
        //プライマリーキーでユーザールール検索する。（連携情報含む）
        List<UserRole> ret = userRoleDao.selectAllByPrimaryKey(id);
        return DozerHelper.mapList(mapper, ret, UserRoleDto.class);
    }

    /**
     * 条件でユーザールールを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<UserRoleDto> selectByExample(UserRoleDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("user_role.id", dto.getId())
            .andEqualTo("user_role.user_id", dto.getUserId())
            .andEqualTo("user_role.role_id", dto.getRoleId())
            .andEqualTo("user_role.delete_flag", dto.getDeleteFlag())
            .andEqualTo("user_role.create_user", dto.getCreateUser())
            .andEqualTo("user_role.update_user", dto.getUpdateUser())
            .andEqualTo("user_role.create_time", dto.getCreateTime())
            .andEqualTo("user_role.update_time", dto.getUpdateTime());
        }
        //条件でユーザールールを検索する。（連携情報含む）)
        List<UserRole> ret = userRoleDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, UserRoleDto.class);
    }

    /**
     * 条件でユーザールールを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<UserRoleDto> selectAllByExample(UserRoleDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("user_role.id", dto.getId())
            .andEqualTo("user_role.user_id", dto.getUserId())
            .andEqualTo("user_role.role_id", dto.getRoleId())
            .andEqualTo("user_role.delete_flag", dto.getDeleteFlag())
            .andEqualTo("user_role.create_user", dto.getCreateUser())
            .andEqualTo("user_role.update_user", dto.getUpdateUser())
            .andEqualTo("user_role.create_time", dto.getCreateTime())
            .andEqualTo("user_role.update_time", dto.getUpdateTime());
        }
        //条件でユーザールールを検索する。（連携情報含む）)
        List<UserRole> ret = userRoleDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, UserRoleDto.class);
    }

    /**
     * ユーザールールを新規追加する。
     * @param dto ユーザールール
     * @return 結果
     */
    public int insertSelective(UserRoleDto dto) {
        UserRole userRole  =  mapper.map(dto, UserRole.class);
        //ユーザールールを新規追加する。
        int ret = userRoleDao.insertSelective(userRole);
        return ret;
    }

    /**
     * ユーザールールを新規追加する。
     * @param dto ユーザールール
     * @return 結果
     */
    public int insert(UserRoleDto dto) {
        UserRole userRole  =  mapper.map(dto, UserRole.class);
        //ユーザールールを新規追加する。
        int ret = userRoleDao.insert(userRole);
        return ret;
    }

    /**
     * プライマリーキーでユーザールールを更新する。
     * @param dto ユーザールール
     * @return 結果
     */
    public int updateByPrimaryKey(UserRoleDto dto) {
        UserRole userRole  =  mapper.map(dto, UserRole.class);
        //プライマリーキーでユーザールールを更新する。
        int ret = userRoleDao.updateByPrimaryKey(userRole);
        return ret;
    }

    /**
     * プライマリーキーでユーザールールを更新する。
     * @param dto ユーザールール
     * @return 結果
     */
    public int updateByPrimaryKeySelective(UserRoleDto dto) {
        UserRole userRole  =  mapper.map(dto, UserRole.class);
        //プライマリーキーでユーザールールを更新する。
        int ret = userRoleDao.updateByPrimaryKeySelective(userRole);
        return ret;
    }

    /**
     * ユーザールールを削除する。
     * @param id ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer id) {
        //ユーザールールを削除する。
        int ret = userRoleDao.deleteByPrimaryKey(id);
        return ret;
    }
}
