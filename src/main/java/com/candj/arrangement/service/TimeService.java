package com.candj.arrangement.service;

import com.candj.arrangement.dao.mapper.AppUserDao;
import com.candj.arrangement.dao.mapper.TimeDao;
import com.candj.arrangement.dto.TimeDto;
import com.candj.arrangement.identifyCurrentUser.CompanyIdentify;
import com.candj.arrangement.mapper.model.AppUser;
import com.candj.arrangement.mapper.model.Time;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 時間情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class TimeService {
    @Autowired
    TimeDao timeDao;

    @Autowired
    Mapper mapper;

    @Autowired
    AppUserDao appUserDao;

    @Autowired
    AppUserService appUserService;
    @Autowired CompanyIdentify companyIdentify;

    /**
     * プライマリーキーで時間を検索する。
     * @param timeId シフトID
     * @return 結果
     */
    public TimeDto selectByPrimaryKey(Integer timeId) {
        //プライマリーキーで時間を検索する。
        Time ret = timeDao.selectByPrimaryKey(timeId);
        return DozerHelper.map(mapper, ret, TimeDto.class);
    }

    /**
     * プライマリーキーで時間を検索する。（連携情報含む）
     * @param timeId シフトID
     * @return 結果
     */
    public List<TimeDto> selectAllByPrimaryKey(Integer timeId) {
        //プライマリーキーで時間検索する。（連携情報含む）
        List<Time> ret = timeDao.selectAllByPrimaryKey(timeId);
        return DozerHelper.mapList(mapper, ret, TimeDto.class);
    }

    /**
     * 条件で時間を検索する。（連携情報含む）
     * @param dto 検索条件
     * @param currentCompanyId
     * @return 結果
     */
    public List<TimeDto> selectByExample(TimeDto dto, int currentCompanyID) {
        WhereCondition whereCondition= new WhereCondition() ;
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        WhereCondition findUser = new WhereCondition();D
//        String userName = auth.getName();
//        findUser.createCriteria()
//                .andEqualTo("app_user.user_name", userName);
//        List<AppUser> user = appUserDao.selectByExample(findUser);
//        CompanyIdentify companyIdentify = new CompanyIdentify();
//        int currentCompanyID = companyIdentify.identifyCompany();
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("time.time_id", dto.getTimeId())
            .andEqualTo("time.start_time", dto.getStartTime())
            .andEqualTo("time.end_time", dto.getEndTime())
            .andEqualTo("time.company_id", currentCompanyID)
            .andEqualTo("time.delete_flag", false)
            .andEqualTo("time.create_user", dto.getCreateUser())
            .andEqualTo("time.update_user", dto.getUpdateUser())
            .andEqualTo("time.create_time", dto.getCreateTime())
            .andEqualTo("time.update_time", dto.getUpdateTime());
        }
        //条件で時間を検索する。（連携情報含む）)
        List<Time> ret = timeDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, TimeDto.class);
    }

    /**
     * 条件で時間を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<TimeDto> selectAllByExample(TimeDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("time.time_id", dto.getTimeId())
            .andEqualTo("time.start_time", dto.getStartTime())
            .andEqualTo("time.end_time", dto.getEndTime())
            .andEqualTo("time.company_id", dto.getCompanyId())
            .andEqualTo("time.delete_flag", dto.getDeleteFlag())
            .andEqualTo("time.create_user", dto.getCreateUser())
            .andEqualTo("time.update_user", dto.getUpdateUser())
            .andEqualTo("time.create_time", dto.getCreateTime())
            .andEqualTo("time.update_time", dto.getUpdateTime());
        }
        //条件で時間を検索する。（連携情報含む）)
        List<Time> ret = timeDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, TimeDto.class);
    }

    /**
     * 時間を新規追加する。
     * @param dto 時間
     * @return 結果
     */
    public int insertSelective(TimeDto dto) {
        Time time  =  mapper.map(dto, Time.class);
        time.setDeleteFlag(false);
        int currentCompany = companyIdentify.identifyCompany();
        time.setCompanyId(currentCompany);
        //時間を新規追加する。
        int ret = timeDao.insertSelective(time);
        return ret;
    }

    /**
     * 時間を新規追加する。
     * @param dto 時間
     * @return 結果
     */
    public int insert(TimeDto dto) {
        Time time  =  mapper.map(dto, Time.class);
        //時間を新規追加する。
        int ret = timeDao.insert(time);
        return ret;
    }

    /**
     * プライマリーキーで時間を更新する。
     * @param dto 時間
     * @return 結果
     */
    public int updateByPrimaryKey(TimeDto dto) {
        Time time  =  mapper.map(dto, Time.class);
        //プライマリーキーで時間を更新する。
        int ret = timeDao.updateByPrimaryKey(time);
        return ret;
    }

    /**
     * プライマリーキーで時間を更新する。
     * @param dto 時間
     * @return 結果
     */
    public int updateByPrimaryKeySelective(TimeDto dto) {
        Time time  =  mapper.map(dto, Time.class);
        //プライマリーキーで時間を更新する。
        int ret = timeDao.updateByPrimaryKeySelective(time);
        return ret;
    }

    /**
     * 時間を削除する。
     * @param timeId シフトID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer timeId) {
        //時間を削除する。
        int ret = timeDao.deleteByPrimaryKey(timeId);
        return ret;
    }
}
