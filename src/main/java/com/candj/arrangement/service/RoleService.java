package com.candj.arrangement.service;

import com.candj.arrangement.dao.mapper.RoleDao;
import com.candj.arrangement.dto.RoleDto;
import com.candj.arrangement.mapper.model.Role;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ロール情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class RoleService {
    @Autowired
    RoleDao roleDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーでロールを検索する。
     * @param roleId ロールID
     * @return 結果
     */
    public RoleDto selectByPrimaryKey(Integer roleId) {
        //プライマリーキーでロールを検索する。
        Role ret = roleDao.selectByPrimaryKey(roleId);
        return DozerHelper.map(mapper, ret, RoleDto.class);
    }

    /**
     * プライマリーキーでロールを検索する。（連携情報含む）
     * @param roleId ロールID
     * @return 結果
     */
    public List<RoleDto> selectAllByPrimaryKey(Integer roleId) {
        //プライマリーキーでロール検索する。（連携情報含む）
        List<Role> ret = roleDao.selectAllByPrimaryKey(roleId);
        return DozerHelper.mapList(mapper, ret, RoleDto.class);
    }

    /**
     * 条件でロールを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<RoleDto> selectByExample(RoleDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("role.role_id", dto.getRoleId())
            .andEqualTo("role.role_code", dto.getRoleCode())
            .andEqualTo("role.role_name", dto.getRoleName())
            .andEqualTo("role.delete_flag", dto.getDeleteFlag())
            .andEqualTo("role.create_user", dto.getCreateUser())
            .andEqualTo("role.update_user", dto.getUpdateUser())
            .andEqualTo("role.create_time", dto.getCreateTime())
            .andEqualTo("role.update_time", dto.getUpdateTime());
        }
        //条件でロールを検索する。（連携情報含む）)
        List<Role> ret = roleDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, RoleDto.class);
    }

    /**
     * 条件でロールを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<RoleDto> selectAllByExample(RoleDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("role.role_id", dto.getRoleId())
            .andEqualTo("role.role_code", dto.getRoleCode())
            .andEqualTo("role.role_name", dto.getRoleName())
            .andEqualTo("role.delete_flag", dto.getDeleteFlag())
            .andEqualTo("role.create_user", dto.getCreateUser())
            .andEqualTo("role.update_user", dto.getUpdateUser())
            .andEqualTo("role.create_time", dto.getCreateTime())
            .andEqualTo("role.update_time", dto.getUpdateTime());
        }
        //条件でロールを検索する。（連携情報含む）)
        List<Role> ret = roleDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, RoleDto.class);
    }

    /**
     * ロールを新規追加する。
     * @param dto ロール
     * @return 結果
     */
    public int insertSelective(RoleDto dto) {
        Role role  =  mapper.map(dto, Role.class);
        //ロールを新規追加する。
        int ret = roleDao.insertSelective(role);
        return ret;
    }

    /**
     * ロールを新規追加する。
     * @param dto ロール
     * @return 結果
     */
    public int insert(RoleDto dto) {
        Role role  =  mapper.map(dto, Role.class);
        //ロールを新規追加する。
        int ret = roleDao.insert(role);
        return ret;
    }

    /**
     * プライマリーキーでロールを更新する。
     * @param dto ロール
     * @return 結果
     */
    public int updateByPrimaryKey(RoleDto dto) {
        Role role  =  mapper.map(dto, Role.class);
        //プライマリーキーでロールを更新する。
        int ret = roleDao.updateByPrimaryKey(role);
        return ret;
    }

    /**
     * プライマリーキーでロールを更新する。
     * @param dto ロール
     * @return 結果
     */
    public int updateByPrimaryKeySelective(RoleDto dto) {
        Role role  =  mapper.map(dto, Role.class);
        //プライマリーキーでロールを更新する。
        int ret = roleDao.updateByPrimaryKeySelective(role);
        return ret;
    }

    /**
     * ロールを削除する。
     * @param roleId ロールID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer roleId) {
        //ロールを削除する。
        int ret = roleDao.deleteByPrimaryKey(roleId);
        return ret;
    }
}
