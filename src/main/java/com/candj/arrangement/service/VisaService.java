package com.candj.arrangement.service;

import com.candj.arrangement.dao.mapper.VisaDao;
import com.candj.arrangement.dto.TaskDto;
import com.candj.arrangement.dto.VisaDto;
import com.candj.arrangement.mapper.model.Visa;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 签证種類情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class VisaService {
    @Autowired
    VisaDao visaDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで签证種類を検索する。
     * @param visaId 签证ID
     * @return 結果
     */
    public VisaDto selectByPrimaryKey(Integer visaId) {
        //プライマリーキーで签证種類を検索する。
        Visa ret = visaDao.selectByPrimaryKey(visaId);
        return DozerHelper.map(mapper, ret, VisaDto.class);
    }

    /**
     * プライマリーキーで签证種類を検索する。（連携情報含む）
     * @param visaId 签证ID
     * @return 結果
     */
    public List<VisaDto> selectAllByPrimaryKey(Integer visaId) {
        //プライマリーキーで签证種類検索する。（連携情報含む）
        List<Visa> ret = visaDao.selectAllByPrimaryKey(visaId);
        return DozerHelper.mapList(mapper, ret, VisaDto.class);
    }

    /**
     * 条件で签证種類を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<VisaDto> selectByExample(VisaDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("visa.visa_id", dto.getVisaId())
            .andEqualTo("visa.visa_name", dto.getVisaName())
            .andEqualTo("visa.permitted_work_hour_per_week", dto.getPermittedWorkHourPerWeek())
            .andEqualTo("visa.delete_flag", false)
            .andEqualTo("visa.create_user", dto.getCreateUser())
            .andEqualTo("visa.update_user", dto.getUpdateUser())
            .andEqualTo("visa.create_time", dto.getCreateTime())
            .andEqualTo("visa.update_time", dto.getUpdateTime());
        }
        //条件で签证種類を検索する。（連携情報含む）)
        List<Visa> ret = visaDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, VisaDto.class);
    }

    /**
     * 条件で签证種類を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<VisaDto> selectAllByExample(VisaDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("visa.visa_id", dto.getVisaId())
            .andEqualTo("visa.visa_name", dto.getVisaName())
            .andEqualTo("visa.permitted_work_hour_per_week", dto.getPermittedWorkHourPerWeek())
            .andEqualTo("visa.delete_flag", dto.getDeleteFlag())
            .andEqualTo("visa.create_user", dto.getCreateUser())
            .andEqualTo("visa.update_user", dto.getUpdateUser())
            .andEqualTo("visa.create_time", dto.getCreateTime())
            .andEqualTo("visa.update_time", dto.getUpdateTime());
        }
        //条件で签证種類を検索する。（連携情報含む）)
        List<Visa> ret = visaDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, VisaDto.class);
    }

    /**
     * 签证種類を新規追加する。
     * @param dto 签证種類
     * @return 結果
     */
    public int insertSelective(VisaDto dto) {
        Visa visa  =  mapper.map(dto, Visa.class);
        dto.setDeleteFlag(false);
        //签证種類を新規追加する。
        int ret = visaDao.insertSelective(visa);
        return ret;
    }

    /**
     * 签证種類を新規追加する。
     * @param dto 签证種類
     * @return 結果
     */
    public int insert(VisaDto dto) {
        Visa visa  =  mapper.map(dto, Visa.class);
        //签证種類を新規追加する。
        int ret = visaDao.insert(visa);
        return ret;
    }

    /**
     * プライマリーキーで签证種類を更新する。
     * @param dto 签证種類
     * @return 結果
     */
    public int updateByPrimaryKey(VisaDto dto) {
        Visa visa  =  mapper.map(dto, Visa.class);
        //プライマリーキーで签证種類を更新する。
        int ret = visaDao.updateByPrimaryKey(visa);
        return ret;
    }

    /**
     * プライマリーキーで签证種類を更新する。
     * @param dto 签证種類
     * @return 結果
     */
    public int updateByPrimaryKeySelective(VisaDto dto) {
        Visa visa  =  mapper.map(dto, Visa.class);
        //プライマリーキーで签证種類を更新する。
        int ret = visaDao.updateByPrimaryKeySelective(visa);
        return ret;
    }

    /**
     * 签证種類を削除する。
     * @param visaId 签证ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer visaId) {
        //签证種類を削除する。

        int ret = visaDao.deleteByPrimaryKey(visaId);
        return ret;
    }

    public boolean visaExists(VisaDto visaDto) {
        WhereCondition whereCondition = new WhereCondition();
        WhereCondition.Criteria criteria = whereCondition.createCriteria();
        if(visaDto.getVisaName() != null){
            criteria.andEqualTo("visa.visa_name", visaDto.getVisaName());
        }
        int count =  visaDao.selectCountByExample(whereCondition);
        return count>0;
    }
}
