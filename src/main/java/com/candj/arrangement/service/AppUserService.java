package com.candj.arrangement.service;

import com.candj.arrangement.dao.mapper.AppUserDao;
import com.candj.arrangement.dto.AppUserDto;
import com.candj.arrangement.mapper.model.AppUser;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 管理者情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class AppUserService {
    @Autowired
    AppUserDao appUserDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで管理者を検索する。
     * @param userId 管理者ID
     * @return 結果
     */
    public AppUserDto selectByPrimaryKey(Integer userId) {
        //プライマリーキーで管理者を検索する。
        AppUser ret = appUserDao.selectByPrimaryKey(userId);
        return DozerHelper.map(mapper, ret, AppUserDto.class);
    }

    /**
     * プライマリーキーで管理者を検索する。（連携情報含む）
     * @param userId 管理者ID
     * @return 結果
     */
    public List<AppUserDto> selectAllByPrimaryKey(Integer userId) {
        //プライマリーキーで管理者検索する。（連携情報含む）
        List<AppUser> ret = appUserDao.selectAllByPrimaryKey(userId);
        return DozerHelper.mapList(mapper, ret, AppUserDto.class);
    }

    /**
     * 条件で管理者を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<AppUserDto> selectByExample(AppUserDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("app_user.user_id", dto.getUserId())
            .andEqualTo("app_user.user_code", dto.getUserCode())
            .andEqualTo("app_user.user_name", dto.getUserName())
            .andEqualTo("app_user.password", dto.getPassword())
            .andEqualTo("app_user.company_id", dto.getCompanyId())
            .andEqualTo("app_user.delete_flag", dto.getDeleteFlag())
            .andEqualTo("app_user.create_user", dto.getCreateUser())
            .andEqualTo("app_user.update_user", dto.getUpdateUser())
            .andEqualTo("app_user.create_time", dto.getCreateTime())
            .andEqualTo("app_user.update_time", dto.getUpdateTime());
        }
        //条件で管理者を検索する。（連携情報含む）)
        List<AppUser> ret = appUserDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, AppUserDto.class);
    }

    /**
     * 条件で管理者を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<AppUserDto> selectAllByExample(AppUserDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("app_user.user_id", dto.getUserId())
            .andEqualTo("app_user.user_code", dto.getUserCode())
            .andEqualTo("app_user.user_name", dto.getUserName())
            .andEqualTo("app_user.password", dto.getPassword())
            .andEqualTo("app_user.company_id", dto.getCompanyId())
            .andEqualTo("app_user.delete_flag", dto.getDeleteFlag())
            .andEqualTo("app_user.create_user", dto.getCreateUser())
            .andEqualTo("app_user.update_user", dto.getUpdateUser())
            .andEqualTo("app_user.create_time", dto.getCreateTime())
            .andEqualTo("app_user.update_time", dto.getUpdateTime());
        }
        //条件で管理者を検索する。（連携情報含む）)
        List<AppUser> ret = appUserDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, AppUserDto.class);
    }

    /**
     * 管理者を新規追加する。
     * @param dto 管理者
     * @return 結果
     */
    public int insertSelective(AppUserDto dto) {
        AppUser appUser  =  mapper.map(dto, AppUser.class);
        //管理者を新規追加する。
        int ret = appUserDao.insertSelective(appUser);
        return ret;
    }

    /**
     * 管理者を新規追加する。
     * @param dto 管理者
     * @return 結果
     */
    public int insert(AppUserDto dto) {
        AppUser appUser  =  mapper.map(dto, AppUser.class);
        //管理者を新規追加する。
        int ret = appUserDao.insert(appUser);
        return ret;
    }

    /**
     * プライマリーキーで管理者を更新する。
     * @param dto 管理者
     * @return 結果
     */
    public int updateByPrimaryKey(AppUserDto dto) {
        AppUser appUser  =  mapper.map(dto, AppUser.class);
        //プライマリーキーで管理者を更新する。
        int ret = appUserDao.updateByPrimaryKey(appUser);
        return ret;
    }

    /**
     * プライマリーキーで管理者を更新する。
     * @param dto 管理者
     * @return 結果
     */
    public int updateByPrimaryKeySelective(AppUserDto dto) {
        AppUser appUser  =  mapper.map(dto, AppUser.class);
        //プライマリーキーで管理者を更新する。
        int ret = appUserDao.updateByPrimaryKeySelective(appUser);
        return ret;
    }

    /**
     * 管理者を削除する。
     * @param userId 管理者ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer userId) {
        //管理者を削除する。
        int ret = appUserDao.deleteByPrimaryKey(userId);
        return ret;
    }


}
