package com.candj.arrangement.service;

import com.candj.arrangement.dao.mapper.ShiftworkDao;
import com.candj.arrangement.dao.mapper.TaskDao;
import com.candj.arrangement.dao.mapper.TimeDao;
import com.candj.arrangement.dto.ShiftworkDto;
import com.candj.arrangement.dto.TaskDto;
import com.candj.arrangement.identifyCurrentUser.CompanyIdentify;
import com.candj.arrangement.mapper.model.Shiftwork;
import com.candj.arrangement.mapper.model.Task;
import com.candj.arrangement.mapper.model.Time;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;

import jdk.nashorn.internal.objects.annotations.Where;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * タスク情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class TaskService {
    @Autowired
    TaskDao taskDao;

    @Autowired
    Mapper mapper;

    @Autowired
    CompanyIdentify companyIdentify;

    @Autowired
    TimeDao timeDao;

    @Autowired
    ShiftworkDao shiftworkDao;
    /**
     * プライマリーキーでタスクを検索する。
     * @param taskId タスクID
     * @return 結果
     */
    public TaskDto selectByPrimaryKey(Integer taskId) {
        //プライマリーキーでタスクを検索する。
        Task ret = taskDao.selectByPrimaryKey(taskId);
        return DozerHelper.map(mapper, ret, TaskDto.class);
    }

    /**
     * プライマリーキーでタスクを検索する。（連携情報含む）
     * @param taskId タスクID
     * @return 結果
     */
    public List<TaskDto> selectAllByPrimaryKey(Integer taskId) {
        //プライマリーキーでタスク検索する。（連携情報含む）
        List<Task> ret = taskDao.selectAllByPrimaryKey(taskId);
        return DozerHelper.mapList(mapper, ret, TaskDto.class);
    }

    /**
     * 条件でタスクを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<TaskDto> selectByExample(TaskDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        int currentCompanyId = companyIdentify.identifyCompany();
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("task.task_id", dto.getTaskId())
                    .andEqualTo("task.task_name", dto.getTaskName())
                    .andEqualTo("task.start_time", dto.getStartTime())
                    .andEqualTo("task.end_time", dto.getEndTime())
                    .andEqualTo("task.expect_workforce", dto.getExpectWorkforce())
                    .andEqualTo("task.company_id", currentCompanyId)
                    .andEqualTo("task.delete_flag", false)
                    .andEqualTo("task.create_user", dto.getCreateUser())
                    .andEqualTo("task.update_user", dto.getUpdateUser())
                    .andEqualTo("task.create_time", dto.getCreateTime())
                    .andEqualTo("task.update_time", dto.getUpdateTime());
        }
        //条件でタスクを検索する。（連携情報含む）)
        List<Task> ret = taskDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, TaskDto.class);
    }

    /**
     * 条件でタスクを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<TaskDto> selectByExample(TaskDto dto, int currentCompanyId, DateTime timeSearch) {
        WhereCondition whereCondition= new WhereCondition() ;
        WhereCondition.Criteria criteria = whereCondition.createCriteria();
        if(dto != null) {
            criteria
            .andEqualTo("task.task_id", dto.getTaskId())
            .andEqualTo("task.task_name", dto.getTaskName())
            .andEqualTo("task.start_time", dto.getStartTime())
            .andEqualTo("task.end_time", dto.getEndTime())
            .andEqualTo("task.expect_workforce", dto.getExpectWorkforce())
            .andEqualTo("task.company_id", currentCompanyId)
            .andEqualTo("task.delete_flag", false)
            .andEqualTo("task.create_user", dto.getCreateUser())
            .andEqualTo("task.update_user", dto.getUpdateUser())
            .andEqualTo("task.create_time", dto.getCreateTime())
            .andEqualTo("task.update_time", dto.getUpdateTime());
        }
        if(timeSearch != null){
            DateTime start = timeSearch.withTime(0,0,0,0);
            DateTime finish = start.plusDays(1);
            criteria.andGreaterThan("task.end_time", start)
                    .andLessThan("task.start_time", start);
        }
        //条件でタスクを検索する。（連携情報含む）)
        List<Task> ret = taskDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, TaskDto.class);
    }

    /**
     * 条件でタスクを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<TaskDto> selectAllByExample(TaskDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("task.task_id", dto.getTaskId())
            .andEqualTo("task.task_name", dto.getTaskName())
            .andEqualTo("task.start_time", dto.getStartTime())
            .andEqualTo("task.end_time", dto.getEndTime())
            .andEqualTo("task.expect_workforce", dto.getExpectWorkforce())
            .andEqualTo("task.company_id", dto.getCompanyId())
            .andEqualTo("task.delete_flag", dto.getDeleteFlag())
            .andEqualTo("task.create_user", dto.getCreateUser())
            .andEqualTo("task.update_user", dto.getUpdateUser())
            .andEqualTo("task.create_time", dto.getCreateTime())
            .andEqualTo("task.update_time", dto.getUpdateTime());
        }
        //条件でタスクを検索する。（連携情報含む）)
        List<Task> ret = taskDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, TaskDto.class);
    }

    /**
     * タスクを新規追加する。
     * @param dto タスク
     * @return 結果
     */
    public int insertSelective(TaskDto dto) {
        Task task  =  mapper.map(dto, Task.class);
        int currentCompanyID = companyIdentify.identifyCompany();
        task.setCompanyId(currentCompanyID);
        task.setDeleteFlag(false);
        int ret = taskDao.insertSelective(task);


        WhereCondition getTask = new WhereCondition();
        getTask.createCriteria()
                .andEqualTo("task.task_name",task.getTaskName())
                .andEqualTo("task.start_time", task.getStartTime())
                .andEqualTo("task.end_time", task.getEndTime());
        List<Task> tasks = taskDao.selectByExample(getTask);
        Task specificTask = tasks.get(0);
        generateShifts(task.getStartTime(),task.getEndTime(),specificTask,currentCompanyID);
        return ret;
    }

    /**
     * タスクを新規追加する。
     * @param dto タスク
     * @return 結果
     */
    public int insert(TaskDto dto) {
        Task task  =  mapper.map(dto, Task.class);
        //タスクを新規追加する。
        int ret = taskDao.insert(task);
        return ret;
    }

    /**
     * プライマリーキーでタスクを更新する。
     * @param dto タスク
     * @return 結果
     */
    public int updateByPrimaryKey(TaskDto dto) {
        Task task  =  mapper.map(dto, Task.class);
        //プライマリーキーでタスクを更新する。
        int ret = taskDao.updateByPrimaryKey(task);
        return ret;
    }

    /**
     * プライマリーキーでタスクを更新する。
     * @param dto タスク
     * @return 結果
     */
    public int updateByPrimaryKeySelective(TaskDto dto) {
        Task task  =  mapper.map(dto, Task.class);
        //プライマリーキーでタスクを更新する。
        int ret = taskDao.updateByPrimaryKeySelective(task);

//        Shiftwork changeToDeleted = new Shiftwork();
//        changeToDeleted.setDeleteFlag(false);
//        WhereCondition whereCondition = new WhereCondition();
//        whereCondition.createCriteria()
//                .andEqualTo("shiftwork.task_id", task.getTaskId());
//        int returnVal = shiftworkDao.updateByExampleSelective(changeToDeleted,whereCondition);
//        int currentCompanyID = companyIdentify.identifyCompany();
//        generateShifts(task.getStartTime(),task.getEndTime(), task, currentCompanyID);
        return ret;
    }

    /**
     * タスクを削除する。
     * @param taskId タスクID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer taskId) {
        //タスクを削除する。
        int ret = taskDao.deleteByPrimaryKey(taskId);
        return ret;
    }

    private void generateShifts(DateTime start, DateTime end, Task specificTask, int currentCompanyID){
        List<Time> times = getAllTimePieces(currentCompanyID);
        int day = 0;
        while(start.isBefore(end)){
            int order = 0;
            for (Time t:times) {
                Shiftwork shift = new Shiftwork();
                shift.setShiftworkName(specificTask.getTaskName() + "_" + start.toString("yyyy-MM-dd")
                        + "_" + t.getStartTime().toString("HHmm") + "~" + t.getEndTime().toString("HHmm"));
                shift.setStartTime(timeCombine(start, t.getStartTime()));
                shift.setEndTime(timeCombine(start, t.getEndTime()));
                shift.setCompanyId(currentCompanyID);
                shift.setTaskId(specificTask.getTaskId());
                shift.setDeleteFlag(false);
                shiftworkDao.insertSelective(shift);
                order ++;
            }
            start = start.plusDays(1);
            day ++;
        }
    }

    private DateTime timeCombine(DateTime dayTime, DateTime minuteTime){
        return new DateTime(dayTime.getYear(), dayTime.getMonthOfYear(), dayTime.getDayOfMonth(), minuteTime.getMinuteOfHour(),
                minuteTime.getSecondOfMinute());
    }

    private List<Time> getAllTimePieces(int currentCompanyID){
        WhereCondition whereCondition = new WhereCondition();
        whereCondition.createCriteria()
                .andEqualTo("time.company_id", currentCompanyID);
        return timeDao.selectAllByExample(whereCondition);
    }

    public boolean taskExists(TaskDto taskDto) {
        WhereCondition whereCondition = new WhereCondition();
        WhereCondition.Criteria criteria = whereCondition.createCriteria();
        if(taskDto.getTaskName() != null){
            criteria.andEqualTo("task.task_name", taskDto.getTaskName());
        }
        int count =  taskDao.selectCountByExample(whereCondition);
        return count>0;
    }
}
