package com.candj.arrangement.service;

import com.candj.arrangement.dao.mapper.StaffVisaDao;
import com.candj.arrangement.dto.StaffVisaDto;
import com.candj.arrangement.mapper.model.StaffVisa;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * スタッフ_签证種類情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class StaffVisaService {
    @Autowired
    StaffVisaDao staffVisaDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーでスタッフ_签证種類を検索する。
     * @param id ID
     * @return 結果
     */
    public StaffVisaDto selectByPrimaryKey(Integer id) {
        //プライマリーキーでスタッフ_签证種類を検索する。
        StaffVisa ret = staffVisaDao.selectByPrimaryKey(id);
        return DozerHelper.map(mapper, ret, StaffVisaDto.class);
    }

    /**
     * プライマリーキーでスタッフ_签证種類を検索する。（連携情報含む）
     * @param id ID
     * @return 結果
     */
    public List<StaffVisaDto> selectAllByPrimaryKey(Integer id) {
        //プライマリーキーでスタッフ_签证種類検索する。（連携情報含む）
        List<StaffVisa> ret = staffVisaDao.selectAllByPrimaryKey(id);
        return DozerHelper.mapList(mapper, ret, StaffVisaDto.class);
    }

    /**
     * 条件でスタッフ_签证種類を検索する。（連携情報含む）
     * @param dto 検索条件
     * @param currentCompanyId
     * @return 結果
     */
    public List<StaffVisaDto> selectByExample(StaffVisaDto dto, int currentCompanyId) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("staff_visa.id", dto.getId())
            .andEqualTo("staff_visa.staff_id", dto.getStaffId())
            .andEqualTo("staff_visa.visa_id", dto.getVisaId())
            .andEqualTo("staff_visa.end_time", dto.getEndTime())
            .andEqualTo("staff_visa.delete_flag", dto.getDeleteFlag())
            .andEqualTo("staff_visa.create_user", dto.getCreateUser())
            .andEqualTo("staff_visa.update_user", dto.getUpdateUser())
            .andEqualTo("staff_visa.create_time", dto.getCreateTime())
            .andEqualTo("staff_visa.update_time", dto.getUpdateTime())
            .andEqualTo("staff.company_id", currentCompanyId);
        }
        //条件でスタッフ_签证種類を検索する。（連携情報含む）)
        List<StaffVisa> ret = staffVisaDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, StaffVisaDto.class);
    }

    /**
     * 条件でスタッフ_签证種類を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<StaffVisaDto> selectAllByExample(StaffVisaDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("staff_visa.id", dto.getId())
            .andEqualTo("staff_visa.staff_id", dto.getStaffId())
            .andEqualTo("staff_visa.visa_id", dto.getVisaId())
            .andEqualTo("staff_visa.end_time", dto.getEndTime())
            .andEqualTo("staff_visa.delete_flag", dto.getDeleteFlag())
            .andEqualTo("staff_visa.create_user", dto.getCreateUser())
            .andEqualTo("staff_visa.update_user", dto.getUpdateUser())
            .andEqualTo("staff_visa.create_time", dto.getCreateTime())
            .andEqualTo("staff_visa.update_time", dto.getUpdateTime());
        }
        //条件でスタッフ_签证種類を検索する。（連携情報含む）)
        List<StaffVisa> ret = staffVisaDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, StaffVisaDto.class);
    }

    /**
     * スタッフ_签证種類を新規追加する。
     * @param dto スタッフ_签证種類
     * @return 結果
     */
    public int insertSelective(StaffVisaDto dto) {
        StaffVisa staffVisa  =  mapper.map(dto, StaffVisa.class);
        //スタッフ_签证種類を新規追加する。
        int ret = staffVisaDao.insertSelective(staffVisa);
        return ret;
    }

    /**
     * スタッフ_签证種類を新規追加する。
     * @param dto スタッフ_签证種類
     * @return 結果
     */
    public int insert(StaffVisaDto dto) {
        StaffVisa staffVisa  =  mapper.map(dto, StaffVisa.class);
        //スタッフ_签证種類を新規追加する。
        int ret = staffVisaDao.insert(staffVisa);
        return ret;
    }

    /**
     * プライマリーキーでスタッフ_签证種類を更新する。
     * @param dto スタッフ_签证種類
     * @return 結果
     */
    public int updateByPrimaryKey(StaffVisaDto dto) {
        StaffVisa staffVisa  =  mapper.map(dto, StaffVisa.class);
        //プライマリーキーでスタッフ_签证種類を更新する。
        int ret = staffVisaDao.updateByPrimaryKey(staffVisa);
        return ret;
    }

    /**
     * プライマリーキーでスタッフ_签证種類を更新する。
     * @param dto スタッフ_签证種類
     * @return 結果
     */
    public int updateByPrimaryKeySelective(StaffVisaDto dto) {
        StaffVisa staffVisa  =  mapper.map(dto, StaffVisa.class);
        //プライマリーキーでスタッフ_签证種類を更新する。
        int ret = staffVisaDao.updateByPrimaryKeySelective(staffVisa);
        return ret;
    }

    /**
     * スタッフ_签证種類を削除する。
     * @param id ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer id) {
        //スタッフ_签证種類を削除する。
        int ret = staffVisaDao.deleteByPrimaryKey(id);
        return ret;
    }
}
