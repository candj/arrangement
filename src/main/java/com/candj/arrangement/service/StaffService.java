package com.candj.arrangement.service;

import com.candj.arrangement.dao.mapper.StaffDao;
import com.candj.arrangement.dto.StaffDto;
import com.candj.arrangement.dto.StaffVisaDto;
import com.candj.arrangement.dto.VisaDto;
import com.candj.arrangement.identifyCurrentUser.CompanyIdentify;
import com.candj.arrangement.mapper.model.Staff;
import com.candj.arrangement.mapper.model.StaffVisa;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * スタッフ情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class StaffService {
    @Autowired
    StaffDao staffDao;

    @Autowired
    Mapper mapper;

    @Autowired
    CompanyIdentify companyIdentify;

    @Autowired
    StaffVisaService staffVisaService;

    /**
     * プライマリーキーでスタッフを検索する。
     * @param staffId スタッフID
     * @return 結果
     */
    public StaffDto selectByPrimaryKey(Integer staffId) {
        //プライマリーキーでスタッフを検索する。
        Staff ret = staffDao.selectByPrimaryKey(staffId);
        return DozerHelper.map(mapper, ret, StaffDto.class);
    }

    /**
     * プライマリーキーでスタッフを検索する。（連携情報含む）
     * @param staffId スタッフID
     * @return 結果
     */
    public List<StaffDto> selectAllByPrimaryKey(Integer staffId) {
        //プライマリーキーでスタッフ検索する。（連携情報含む）
        List<Staff> ret = staffDao.selectAllByPrimaryKey(staffId);
        return DozerHelper.mapList(mapper, ret, StaffDto.class);
    }

    /**
     * 条件でスタッフを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<StaffDto> selectByExample(StaffDto dto, int currentCompanyId) {
        WhereCondition whereCondition= new WhereCondition();
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("staff.staff_id", dto.getStaffId())
            .andEqualTo("staff.staff_code", dto.getStaffCode())
            .andEqualTo("staff.staff_name", dto.getStaffName())
            .andEqualTo("staff.staff_password", dto.getStaffPassword())
            .andEqualTo("staff.katakana", dto.getKatakana())
            .andEqualTo("staff.birth_date", dto.getBirthDate())
            .andEqualTo("staff.short_term_flag", dto.getShortTermFlag())
            .andEqualTo("staff.diploma", dto.getDiploma())
            .andEqualTo("staff.school_status", dto.getSchoolStatus())
            .andEqualTo("staff.school_name", dto.getSchoolName())
            .andEqualTo("staff.expected_grad_date", dto.getExpectedGradDate())
            .andEqualTo("staff.priority", dto.getPriority())
            .andEqualTo("staff.allowance", dto.getAllowance())
            .andEqualTo("staff.salary", dto.getSalary())
            .andEqualTo("staff.pay_method", dto.getPayMethod())
            .andEqualTo("staff.bank_name", dto.getBankName())
            .andEqualTo("staff.branch_no", dto.getBranchNo())
            .andEqualTo("staff.account", dto.getAccount())
            .andEqualTo("staff.contract_flag", dto.getContractFlag())
            .andEqualTo("staff.NDA_flag", dto.getNdaFlag())
            .andEqualTo("staff.year_end_adjustment_flag", dto.getYearEndAdjustmentFlag())
            .andEqualTo("staff.resident_card_flag", dto.getResidentCardFlag())
            .andEqualTo("staff.resident_card_copy_flag", dto.getResidentCardCopyFlag())
            .andEqualTo("staff.expire_date_of_40_hour", dto.getExpireDateOf40Hour())
            .andEqualTo("staff.company_id", currentCompanyId)
            .andEqualTo("staff.delete_flag", false)
            .andEqualTo("staff.create_user", dto.getCreateUser())
            .andEqualTo("staff.update_user", dto.getUpdateUser())
            .andEqualTo("staff.create_time", dto.getCreateTime())
            .andEqualTo("staff.update_time", dto.getUpdateTime());
        }
        //条件でスタッフを検索する。（連携情報含む）)
        List<Staff> ret = staffDao.selectByExample(whereCondition);
        ret.stream().forEach(x->
                {
                    int staffId = x.getStaffId();
                    StaffVisaDto staffVisaDto = new StaffVisaDto();
                    staffVisaDto.setStaffId(staffId);
                    List<StaffVisaDto> visaInfo = staffVisaService.selectAllByExample(staffVisaDto);
                    if(visaInfo.size() > 0){
                        List<StaffVisaDto>longestDurationVisa = visaInfo.stream().sorted((x1,x2)->(x1.getEndTime().compareTo(x2.getEndTime()))).collect(Collectors.toList());
                        x.setVisaEndTime(longestDurationVisa.get(0).getEndTime());
                        x.setVisaName(longestDurationVisa.get(0).getVisa().getVisaName());
                    }
                }
        );
        return DozerHelper.mapList(mapper, ret, StaffDto.class);
    }

    /**
     * 条件でスタッフを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<StaffDto> selectByExample(StaffDto dto) {
        WhereCondition whereCondition= new WhereCondition();
        int currentCompanyId = companyIdentify.identifyCompany();
        if(dto != null) {
            whereCondition.createCriteria()
                    .andEqualTo("staff.staff_id", dto.getStaffId())
                    .andEqualTo("staff.staff_code", dto.getStaffCode())
                    .andEqualTo("staff.staff_name", dto.getStaffName())
                    .andEqualTo("staff.staff_password", dto.getStaffPassword())
                    .andEqualTo("staff.katakana", dto.getKatakana())
                    .andEqualTo("staff.birth_date", dto.getBirthDate())
                    .andEqualTo("staff.short_term_flag", dto.getShortTermFlag())
                    .andEqualTo("staff.diploma", dto.getDiploma())
                    .andEqualTo("staff.school_status", dto.getSchoolStatus())
                    .andEqualTo("staff.school_name", dto.getSchoolName())
                    .andEqualTo("staff.expected_grad_date", dto.getExpectedGradDate())
                    .andEqualTo("staff.priority", dto.getPriority())
                    .andEqualTo("staff.allowance", dto.getAllowance())
                    .andEqualTo("staff.salary", dto.getSalary())
                    .andEqualTo("staff.pay_method", dto.getPayMethod())
                    .andEqualTo("staff.bank_name", dto.getBankName())
                    .andEqualTo("staff.branch_no", dto.getBranchNo())
                    .andEqualTo("staff.account", dto.getAccount())
                    .andEqualTo("staff.contract_flag", dto.getContractFlag())
                    .andEqualTo("staff.NDA_flag", dto.getNdaFlag())
                    .andEqualTo("staff.year_end_adjustment_flag", dto.getYearEndAdjustmentFlag())
                    .andEqualTo("staff.resident_card_flag", dto.getResidentCardFlag())
                    .andEqualTo("staff.resident_card_copy_flag", dto.getResidentCardCopyFlag())
                    .andEqualTo("staff.expire_date_of_40_hour", dto.getExpireDateOf40Hour())
                    .andEqualTo("staff.company_id", currentCompanyId)
                    .andEqualTo("staff.delete_flag", false)
                    .andEqualTo("staff.create_user", dto.getCreateUser())
                    .andEqualTo("staff.update_user", dto.getUpdateUser())
                    .andEqualTo("staff.create_time", dto.getCreateTime())
                    .andEqualTo("staff.update_time", dto.getUpdateTime());
        }
        //条件でスタッフを検索する。（連携情報含む）)
        List<Staff> ret = staffDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, StaffDto.class);
    }

    /**
     * 条件でスタッフを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<StaffDto> selectAllByExample(StaffDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("staff.staff_id", dto.getStaffId())
            .andEqualTo("staff.staff_code", dto.getStaffCode())
            .andEqualTo("staff.staff_name", dto.getStaffName())
            .andEqualTo("staff.staff_password", dto.getStaffPassword())
            .andEqualTo("staff.katakana", dto.getKatakana())
            .andEqualTo("staff.birth_date", dto.getBirthDate())
            .andEqualTo("staff.short_term_flag", dto.getShortTermFlag())
            .andEqualTo("staff.diploma", dto.getDiploma())
            .andEqualTo("staff.school_status", dto.getSchoolStatus())
            .andEqualTo("staff.school_name", dto.getSchoolName())
            .andEqualTo("staff.expected_grad_date", dto.getExpectedGradDate())
            .andEqualTo("staff.priority", dto.getPriority())
            .andEqualTo("staff.allowance", dto.getAllowance())
            .andEqualTo("staff.salary", dto.getSalary())
            .andEqualTo("staff.pay_method", dto.getPayMethod())
            .andEqualTo("staff.bank_name", dto.getBankName())
            .andEqualTo("staff.branch_no", dto.getBranchNo())
            .andEqualTo("staff.account", dto.getAccount())
            .andEqualTo("staff.contract_flag", dto.getContractFlag())
            .andEqualTo("staff.NDA_flag", dto.getNdaFlag())
            .andEqualTo("staff.year_end_adjustment_flag", dto.getYearEndAdjustmentFlag())
            .andEqualTo("staff.resident_card_flag", dto.getResidentCardFlag())
            .andEqualTo("staff.resident_card_copy_flag", dto.getResidentCardCopyFlag())
            .andEqualTo("staff.expire_date_of_40_hour", dto.getExpireDateOf40Hour())
            .andEqualTo("staff.company_id", dto.getCompanyId())
            .andEqualTo("staff.delete_flag", dto.getDeleteFlag())
            .andEqualTo("staff.create_user", dto.getCreateUser())
            .andEqualTo("staff.update_user", dto.getUpdateUser())
            .andEqualTo("staff.create_time", dto.getCreateTime())
            .andEqualTo("staff.update_time", dto.getUpdateTime());
        }
        //条件でスタッフを検索する。（連携情報含む）)
        List<Staff> ret = staffDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, StaffDto.class);
    }

    /**
     * スタッフを新規追加する。
     * @param dto スタッフ
     * @return 結果
     */
    public int insertSelective(StaffDto dto) {
        Staff staff  =  mapper.map(dto, Staff.class);
        int currentCompanyId = companyIdentify.identifyCompany();
        staff.setCompanyId(currentCompanyId);
        //スタッフを新規追加する。
        int ret = staffDao.insertSelective(staff);
        return ret;
    }

    /**
     * スタッフを新規追加する。
     * @param dto スタッフ
     * @return 結果
     */
    public int insert(StaffDto dto) {
        Staff staff  =  mapper.map(dto, Staff.class);
        //スタッフを新規追加する。
        int ret = staffDao.insert(staff);
        return ret;
    }

    /**
     * プライマリーキーでスタッフを更新する。
     * @param dto スタッフ
     * @return 結果
     */
    public int updateByPrimaryKey(StaffDto dto) {
        Staff staff  =  mapper.map(dto, Staff.class);
        //プライマリーキーでスタッフを更新する。
        int ret = staffDao.updateByPrimaryKey(staff);
        return ret;
    }

    /**
     * プライマリーキーでスタッフを更新する。
     * @param dto スタッフ
     * @return 結果
     */
    public int updateByPrimaryKeySelective(StaffDto dto) {
        Staff staff  =  mapper.map(dto, Staff.class);
        //プライマリーキーでスタッフを更新する。
        int ret = staffDao.updateByPrimaryKeySelective(staff);
        return ret;
    }

    /**
     * スタッフを削除する。
     * @param staffId スタッフID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer staffId) {
        //スタッフを削除する。
        int ret = staffDao.deleteByPrimaryKey(staffId);
        return ret;
    }

    public boolean staffExists(StaffDto staffDto) {
        WhereCondition whereCondition = new WhereCondition();
        WhereCondition.Criteria criteria = whereCondition.createCriteria();
        if(staffDto.getStaffCode() != null){
            criteria.andEqualTo("staff.staff_code", staffDto.getStaffCode());
        }
        int count =  staffDao.selectCountByExample(whereCondition);
        return count>0;
    }

}
