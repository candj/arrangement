package com.candj.arrangement.service;

import com.candj.arrangement.dao.mapper.CompanyDao;
import com.candj.arrangement.dto.CompanyDto;
import com.candj.arrangement.mapper.model.Company;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 会社情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class CompanyService {
    @Autowired
    CompanyDao companyDao;

    @Autowired
    Mapper mapper;

    /**
     * プライマリーキーで会社を検索する。
     * @param companyId 会社ID
     * @return 結果
     */
    public CompanyDto selectByPrimaryKey(Integer companyId) {
        //プライマリーキーで会社を検索する。
        Company ret = companyDao.selectByPrimaryKey(companyId);
        return DozerHelper.map(mapper, ret, CompanyDto.class);
    }

    /**
     * プライマリーキーで会社を検索する。（連携情報含む）
     * @param companyId 会社ID
     * @return 結果
     */
    public List<CompanyDto> selectAllByPrimaryKey(Integer companyId) {
        //プライマリーキーで会社検索する。（連携情報含む）
        List<Company> ret = companyDao.selectAllByPrimaryKey(companyId);
        return DozerHelper.mapList(mapper, ret, CompanyDto.class);
    }

    /**
     * 条件で会社を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<CompanyDto> selectByExample(CompanyDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("company.company_id", dto.getCompanyId())
            .andEqualTo("company.company_name", dto.getCompanyName())
            .andEqualTo("company.delete_flag", dto.getDeleteFlag())
            .andEqualTo("company.create_user", dto.getCreateUser())
            .andEqualTo("company.update_user", dto.getUpdateUser())
            .andEqualTo("company.create_time", dto.getCreateTime())
            .andEqualTo("company.update_time", dto.getUpdateTime());
        }
        //条件で会社を検索する。（連携情報含む）)
        List<Company> ret = companyDao.selectByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, CompanyDto.class);
    }

    /**
     * 条件で会社を検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<CompanyDto> selectAllByExample(CompanyDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("company.company_id", dto.getCompanyId())
            .andEqualTo("company.company_name", dto.getCompanyName())
            .andEqualTo("company.delete_flag", dto.getDeleteFlag())
            .andEqualTo("company.create_user", dto.getCreateUser())
            .andEqualTo("company.update_user", dto.getUpdateUser())
            .andEqualTo("company.create_time", dto.getCreateTime())
            .andEqualTo("company.update_time", dto.getUpdateTime());
        }
        //条件で会社を検索する。（連携情報含む）)
        List<Company> ret = companyDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, CompanyDto.class);
    }

    /**
     * 会社を新規追加する。
     * @param dto 会社
     * @return 結果
     */
    public int insertSelective(CompanyDto dto) {
        Company company  =  mapper.map(dto, Company.class);
        //会社を新規追加する。
        int ret = companyDao.insertSelective(company);
        return ret;
    }

    /**
     * 会社を新規追加する。
     * @param dto 会社
     * @return 結果
     */
    public int insert(CompanyDto dto) {
        Company company  =  mapper.map(dto, Company.class);
        //会社を新規追加する。
        int ret = companyDao.insert(company);
        return ret;
    }

    /**
     * プライマリーキーで会社を更新する。
     * @param dto 会社
     * @return 結果
     */
    public int updateByPrimaryKey(CompanyDto dto) {
        Company company  =  mapper.map(dto, Company.class);
        //プライマリーキーで会社を更新する。
        int ret = companyDao.updateByPrimaryKey(company);
        return ret;
    }

    /**
     * プライマリーキーで会社を更新する。
     * @param dto 会社
     * @return 結果
     */
    public int updateByPrimaryKeySelective(CompanyDto dto) {
        Company company  =  mapper.map(dto, Company.class);
        //プライマリーキーで会社を更新する。
        int ret = companyDao.updateByPrimaryKeySelective(company);
        return ret;
    }

    /**
     * 会社を削除する。
     * @param companyId 会社ID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer companyId) {
        //会社を削除する。
        int ret = companyDao.deleteByPrimaryKey(companyId);
        return ret;
    }
}
