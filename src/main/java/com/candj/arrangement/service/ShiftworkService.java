package com.candj.arrangement.service;

import com.candj.arrangement.dao.mapper.ShiftworkDao;
import com.candj.arrangement.dao.mapper.StaffShiftDao;
import com.candj.arrangement.dao.mapper.TaskDao;
import com.candj.arrangement.dto.ShiftworkDto;
import com.candj.arrangement.identifyCurrentUser.CompanyIdentify;
import com.candj.arrangement.mapper.model.Shiftwork;
import com.candj.arrangement.mapper.model.Task;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.model.WhereCondition.Criteria;
import com.candj.webpower.web.core.util.DozerHelper;
import com.github.dozermapper.core.Mapper;

import java.util.Date;
import java.util.List;

import jdk.nashorn.internal.objects.annotations.Where;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * シフト情報追加、更新、削除、検索処理を行うビジネスクラス。
 *
 * @author C&J株式会社
 *
 */
@Service
@Transactional
public class ShiftworkService {
    @Autowired
    ShiftworkDao shiftworkDao;

    @Autowired
    TaskDao taskDao;

    @Autowired
    StaffShiftDao staffShiftDao;

    @Autowired
    Mapper mapper;

    @Autowired
    CompanyIdentify companyIdentify;

    /**
     * プライマリーキーでシフトを検索する。
     * @param shiftworkId シフトID
     * @return 結果
     */
    public ShiftworkDto selectByPrimaryKey(Integer shiftworkId) {
        //プライマリーキーでシフトを検索する。
        Shiftwork ret = shiftworkDao.selectByPrimaryKey(shiftworkId);

        return DozerHelper.map(mapper, ret, ShiftworkDto.class);
    }

    /**
     * プライマリーキーでシフトを検索する。（連携情報含む）
     * @param shiftworkId シフトID
     * @return 結果
     */
    public List<ShiftworkDto> selectAllByPrimaryKey(Integer shiftworkId) {
        //プライマリーキーでシフト検索する。（連携情報含む）
        List<Shiftwork> ret = shiftworkDao.selectAllByPrimaryKey(shiftworkId);
        return DozerHelper.mapList(mapper, ret, ShiftworkDto.class);
    }

    /**
     * 条件でシフトを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<ShiftworkDto> selectByExample(ShiftworkDto dto, DateTime timeSearch) {
        WhereCondition whereCondition= new WhereCondition() ;
        WhereCondition.Criteria criteria = whereCondition.createCriteria();
        if(dto != null) {
            criteria
            .andEqualTo("shiftwork.shiftwork_id", dto.getShiftworkId())
            .andEqualTo("shiftwork.shiftwork_name", dto.getShiftworkName())
            .andEqualTo("shiftwork.start_time", dto.getStartTime())
            .andEqualTo("shiftwork.end_time", dto.getEndTime())
            .andEqualTo("shiftwork.company_id", dto.getCompanyId())
            .andEqualTo("shiftwork.task_id", dto.getTaskId())
            .andEqualTo("shiftwork.delete_flag", dto.getDeleteFlag())
            .andEqualTo("shiftwork.create_user", dto.getCreateUser())
            .andEqualTo("shiftwork.update_user", dto.getUpdateUser())
            .andEqualTo("shiftwork.create_time", dto.getCreateTime())
            .andEqualTo("shiftwork.update_time", dto.getUpdateTime());
        }
        //条件でシフトを検索する。（連携情報含む）)
        if(timeSearch != null){
            DateTime start = timeSearch.withTime(0,0,0,0);
            DateTime finish = start.plusDays(1);
            criteria.andGreaterThan("shiftwork.start_time", start)
                    .andLessThan("shiftwork.start_time", finish);
        }
        List<Shiftwork> ret = shiftworkDao.selectByExample(whereCondition);
        for(Shiftwork r: ret){
            Task task = taskDao.selectByPrimaryKey(r.getTaskId());
            int required = task.getExpectWorkforce();
            WhereCondition whereCondition1 = new WhereCondition();
            whereCondition1.createCriteria()
                    .andEqualTo("staff_shift.shiftwork_id", r.getShiftworkId())
                    .andEqualTo("staff_shift.state", 2);
            int enrolled = staffShiftDao.selectCountByExample(whereCondition1);
            WhereCondition whereCondition2 = new WhereCondition();
            whereCondition2.createCriteria()
                    .andEqualTo("staff_shift.shiftwork_id", r.getShiftworkId())
                    .andEqualTo("staff_shift.state", 1);
            int rejected = staffShiftDao.selectCountByExample(whereCondition2);
            WhereCondition whereCondition3 = new WhereCondition();
            whereCondition3.createCriteria()
                    .andEqualTo("staff_shift.shiftwork_id", r.getShiftworkId())
                    .andEqualTo("staff_shift.state", 0);
            int unread = staffShiftDao.selectCountByExample(whereCondition3);
            r.setRequired(required);
            r.setEnrolled(enrolled);
            r.setRejected(rejected);
            r.setUnread(unread);
        }
        return DozerHelper.mapList(mapper, ret, ShiftworkDto.class);
    }

    /**
     * 条件でシフトを検索する。（連携情報含む）
     * @param dto 検索条件
     * @return 結果
     */
    public List<ShiftworkDto> selectAllByExample(ShiftworkDto dto) {
        WhereCondition whereCondition= new WhereCondition() ;
        if(dto != null) {
            whereCondition.createCriteria()
            .andEqualTo("shiftwork.shiftwork_id", dto.getShiftworkId())
            .andEqualTo("shiftwork.shiftwork_name", dto.getShiftworkName())
            .andEqualTo("shiftwork.start_time", dto.getStartTime())
            .andEqualTo("shiftwork.end_time", dto.getEndTime())
            .andEqualTo("shiftwork.company_id", dto.getCompanyId())
            .andEqualTo("shiftwork.task_id", dto.getTaskId())
            .andEqualTo("shiftwork.delete_flag", dto.getDeleteFlag())
            .andEqualTo("shiftwork.create_user", dto.getCreateUser())
            .andEqualTo("shiftwork.update_user", dto.getUpdateUser())
            .andEqualTo("shiftwork.create_time", dto.getCreateTime())
            .andEqualTo("shiftwork.update_time", dto.getUpdateTime());
        }
        //条件でシフトを検索する。（連携情報含む）)
        List<Shiftwork> ret = shiftworkDao.selectAllByExample(whereCondition);
        return DozerHelper.mapList(mapper, ret, ShiftworkDto.class);
    }

    /**
     * シフトを新規追加する。
     * @param dto シフト
     * @return 結果
     */
    public int insertSelective(ShiftworkDto dto) {
        Shiftwork shiftwork  =  mapper.map(dto, Shiftwork.class);
        int currentCompanyID = companyIdentify.identifyCompany();
        shiftwork.setDeleteFlag(false);
        shiftwork.setCompanyId(currentCompanyID);
        //シフトを新規追加する。
        int ret = shiftworkDao.insertSelective(shiftwork);
        return ret;
    }

    /**
     * シフトを新規追加する。
     * @param dto シフト
     * @return 結果
     */
    public int insert(ShiftworkDto dto) {
        Shiftwork shiftwork  =  mapper.map(dto, Shiftwork.class);
        //シフトを新規追加する。
        int ret = shiftworkDao.insert(shiftwork);
        return ret;
    }

    /**
     * プライマリーキーでシフトを更新する。
     * @param dto シフト
     * @return 結果
     */
    public int updateByPrimaryKey(ShiftworkDto dto) {
        Shiftwork shiftwork  =  mapper.map(dto, Shiftwork.class);
        //プライマリーキーでシフトを更新する。
        int ret = shiftworkDao.updateByPrimaryKey(shiftwork);
        return ret;
    }

    /**
     * プライマリーキーでシフトを更新する。
     * @param dto シフト
     * @return 結果
     */
    public int updateByPrimaryKeySelective(ShiftworkDto dto) {
        Shiftwork shiftwork  =  mapper.map(dto, Shiftwork.class);
        //プライマリーキーでシフトを更新する。
        int ret = shiftworkDao.updateByPrimaryKeySelective(shiftwork);
        return ret;
    }

    /**
     * シフトを削除する。
     * @param shiftworkId シフトID
     * @return 結果
     */
    public int deleteByPrimaryKey(Integer shiftworkId) {
        //シフトを削除する。
        int ret = shiftworkDao.deleteByPrimaryKey(shiftworkId);
        return ret;
    }

    public boolean shiftworkExists(ShiftworkDto shiftworkDto) {
        WhereCondition whereCondition = new WhereCondition();
        Criteria criteria = whereCondition.createCriteria();
        if(shiftworkDto.getShiftworkName() != null){
            criteria.andEqualTo("shiftwork.shiftwork_name", shiftworkDto.getShiftworkName());
        }
        int count =  shiftworkDao.selectCountByExample(whereCondition);
        return count>0;
    }

    public List<ShiftworkDto> selectDayShift(DateTime startTime){
        List<ShiftworkDto> ret = shiftworkDao.selectDayShift(startTime, startTime.plusDays(1));
        return ret;
    }
}
