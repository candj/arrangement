package com.candj.arrangement.core.controller.exception;

import static java.util.regex.Pattern.*;
import static org.springframework.http.HttpStatus.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.RegexPatternTypeFilter;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.transaction.TransactionException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.candj.webpower.web.core.exception.AcceptedException;
import com.candj.webpower.web.core.exception.BusinessLogicException;
import com.candj.webpower.web.core.exception.ConflictException;
import com.candj.webpower.web.core.exception.ForbiddenException;
import com.candj.webpower.web.core.exception.InternalServerErrorException;
import com.candj.webpower.web.core.exception.NotFoundException;
import com.candj.webpower.web.core.exception.NotValidException;
import com.candj.webpower.web.core.exception.UnauthorizedException;
import com.candj.webpower.web.core.exception.UncheckedLogicException;
import com.candj.webpower.web.core.exception.UncheckedNotFoundException;
import com.candj.webpower.web.core.exception.dao.IncorrectDataUpdateException;
import com.candj.webpower.web.core.response.error.CommonErrorResponse;
import com.candj.webpower.web.core.response.error.ErrorResponse;
import com.candj.webpower.web.core.response.error.ExApiResponse;
import com.candj.webpower.web.core.util.LogUtil;
import com.candj.webpower.web.core.util.ResponseUtil;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import lombok.val;

/**
 * ApiControllerExceptionHandler.
 */
public abstract class AbstractApiControllerExceptionHandler extends ResponseEntityExceptionHandler {

    /** 前給WSのリクエストパケージ. */
    private static final String REQUEST_PACKAGE_MAEKYU = "com.candj.arrangement.controller.request";

    /** エラーハンドリング用のHTTPステータスコード接頭辞. */
    private static final String ERROR_HTTP_STATUS_CODE_PREFIX = "5";

    /** エラーコード: リクエストされたデータを正しくパースできなかった. */
    private static final String REQUEST_FORMAT_ERROR_CODE = "C000-0013";
    /** エラーコード: リクエストされたデータを日付/日時キャストできなかった. */
    private static final String DATE_FORMAT_ERROR_CODE = "C000-0022";
    /** エラーコード: リクエストされたデータを数値キャストできなかった. */
    private static final String NUMBER_ERROR_CODE = "C000-0023";
    /** エラーコード: データベース接続でエラーが発生. */
    private static final String DB_ERROR_CODE = "C000-0014";
    /** エラーコード: データベースエラー（insert,update,delete件数＝0件）. */
    private static final String DB_UPDATE_ERROR_CODE = "C010-0002";
    /** エラーコード: データ不整合(存在しているはずのデータない等). */
    private static final String INCONSISTENT_ERROR_CODE = "C000-0015";
    /** エラーログコード: データベース接続でエラーが発生. */
    private static final String DB_ERROR_LOGCODE = "007-00001";
    /** エラーログコード: データベースエラー（insert,update,delete件数＝0件）. */
    private static final String DB_UPDATE_ERROR_LOGCODE = "007-00002";
    /** エラーログコード: データ不整合(存在しているはずのデータない等). */
    private static final String INCONSISTENT_ERROR_LOGCODE = "007-00004";

    /** */
    private static final Pattern VALIDATION_MESSAGE_PATTERN
            = Pattern.compile("code:(?<code>[\\da-z\\-]+), msg:(?<message>.+)", CASE_INSENSITIVE);

    /**
     * メッセージリソース取得.
     * @return メッセージソース
     */
    protected abstract MessageSource getMessageSource();

    /**
     * メッセージ取得.
     * @param code .
     * @param args .
     * @return .
     */
    protected String getMessage(String code, Object... args) {
        return getMessageSource().getMessage(code, args, Locale.getDefault());
    }

    //-------------------------------------------------------
    // リクエストパラメータのバリデーションハンドラ関連
    //-------------------------------------------------------

    /**
     * 400 BAD REQUEST.
     *   リクエストパラメータのバリデーションエラーをハンドリングします.
     *
     * @param ex .
     * @param headers .
     * @param status .
     * @param request .
     * @return .
     */
    @Override
    @Nonnull
    protected ResponseEntity<Object> handleMethodArgumentNotValid(@Nonnull MethodArgumentNotValidException ex,
            @Nonnull HttpHeaders headers, @Nonnull HttpStatus status, @Nonnull WebRequest request) {
        val errors = ex.getBindingResult().getFieldErrors().stream().map(this::errorResponse).filter(Objects::nonNull)
                .collect(Collectors.toList());

        return handleExceptionInternal(ex, errors, headers, status, request);
    }

    /**
     * 400 BAD REQUEST. リクエストパースエラーをハンドリングします.
     *
     * @param ex .
     * @param headers .
     * @param status .
     * @param request .
     * @return .
     */
    @Override
    @Nonnull
    protected ResponseEntity<Object> handleHttpMessageNotReadable(@Nonnull HttpMessageNotReadableException ex,
            @Nonnull HttpHeaders headers, @Nonnull HttpStatus status, @Nonnull WebRequest request) {
        val cause = ex.getCause();
        if (cause instanceof JsonMappingException) {
            val errors = errorResponseList((JsonMappingException) cause);
            return handleExceptionInternal(ex, errors, headers, status, request);
        } else if (cause instanceof JsonParseException) {
            val error = errorResponse(REQUEST_FORMAT_ERROR_CODE, null);
            return handleExceptionInternal(ex, error, headers, status, request);
        }
        val error = errorResponse(REQUEST_FORMAT_ERROR_CODE, null);
        return handleExceptionInternal(ex, error, headers, status, request);
    }

    /**
     * 400 BAD REQUEST. Binderのバリデーションエラーをハンドリングします.
     *
     * @param ex .
     * @param headers .
     * @param status .
     * @param request .
     * @return .
     */
    @Override
    @Nonnull
    protected ResponseEntity<Object> handleBindException(@Nonnull BindException ex, @Nonnull HttpHeaders headers,
            @Nonnull HttpStatus status, @Nonnull WebRequest request) {
        val errors = ex.getBindingResult().getFieldErrors().stream().map(this::errorResponse).filter(Objects::nonNull)
                .collect(Collectors.toList());

        return handleExceptionInternal(ex, errors, headers, status, request);
    }

    /**
     * DB系エラー（insert,update,deleteの0件更新エラー）をハンドリングします.
     *
     * @param ex 永続化例外, SQLServer例外
     * @param request リクエスト
     * @return レスポンスエンティティ
     */
    @ExceptionHandler({ IncorrectDataUpdateException.class })
    public ResponseEntity<Object> handleIncorrectDataUpdateException(Exception ex, WebRequest request) {
        LogUtil.error(DB_UPDATE_ERROR_LOGCODE);
        val response = errorResponse(DB_UPDATE_ERROR_CODE, null);
        return handleExceptionInternal(ex, response, new HttpHeaders(), INTERNAL_SERVER_ERROR, request);
    }

    /**
     * DB系エラーをハンドリングします.
     *
     * @param ex 永続化例外, SQLServer例外
     * @param request リクエスト
     * @return レスポンスエンティティ
     */
    @ExceptionHandler({ TransactionException.class, DataAccessException.class, SQLException.class })
    public ResponseEntity<Object> handlePersistenceException(Exception ex, WebRequest request) {
        LogUtil.error(DB_ERROR_LOGCODE);
        val response = errorResponse(DB_ERROR_CODE, null);
        return handleExceptionInternal(ex, response, new HttpHeaders(), INTERNAL_SERVER_ERROR, request);
    }


    /**
     * 業務ロジックエラーをハンドリングします.
     *
     * @param ex 業務ロジック例外
     * @param request リクエスト
     * @return レスポンスエンティティ
     */
    @ExceptionHandler(UncheckedLogicException.class)
    public ResponseEntity<Object> handleBusinessLogicException(UncheckedLogicException ex, WebRequest request) {
        val errors = this.commonErrorResponse(ex);
        val headers = new HttpHeaders();

        if (ex instanceof UncheckedNotFoundException) {
            // 404 NOT FOUND. 未検出
            return handleExceptionInternal(ex, errors, headers, HttpStatus.NOT_FOUND, request);
        }

        // 403 FORBIDDEN. サービス内で発生したその他業務ロジックエラー
        return handleExceptionInternal(ex, errors, headers, HttpStatus.FORBIDDEN, request);
    }



    /**
     * 業務ロジックエラーをハンドリングします.
     *
     * @param ex 業務ロジック例外
     * @param request リクエスト
     * @return レスポンスエンティティ
     */
    @ExceptionHandler(BusinessLogicException.class)
    public ResponseEntity<Object> handleBusinessLogicException(BusinessLogicException ex, WebRequest request) {
        val errors = this.commonErrorResponse(ex);
        val headers = new HttpHeaders();

        if (ex instanceof AcceptedException) {
            // 202 ACCEPTED. 受理可能な要求
            return handleExceptionInternal(ex, errors, headers, HttpStatus.ACCEPTED, request);
        } else if (ex instanceof NotValidException) {
            // 400 BAD REQUEST. 不正な要求
            return handleExceptionInternal(ex, errors, headers, HttpStatus.BAD_REQUEST, request);
        } else if (ex instanceof UnauthorizedException) {
            // 401 UNAUTHORIZED. 不許可・未認証
            return handleExceptionInternal(ex, errors, headers, HttpStatus.UNAUTHORIZED, request);
        } else if (ex instanceof ForbiddenException) {
            // 403 FORBIDDEN. アクセス制限
            return handleExceptionInternal(ex, errors, headers, HttpStatus.FORBIDDEN, request);
        } else if (ex instanceof NotFoundException) {
            // 404 NOT FOUND. 未検出
            return handleExceptionInternal(ex, errors, headers, HttpStatus.NOT_FOUND, request);
        } else if (ex instanceof ConflictException) {
            // 409 CONFLICT. 競合
            return handleExceptionInternal(ex, errors, headers, HttpStatus.CONFLICT, request);
        } else if (ex instanceof InternalServerErrorException) {
            // 500 INTERNAL SERVER ERROR. 未知のエラー
            return handleExceptionInternal(ex, errors, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
        }

        // 403 FORBIDDEN. サービス内で発生したその他業務ロジックエラー
        return handleExceptionInternal(ex, errors, headers, HttpStatus.FORBIDDEN, request);
    }

    /**
     * システムエラーをハンドリングします.<br>
     * NullPointerException等.
     *
     * @param ex 業務ロジック以外の例外
     * @param request リクエスト
     * @return レスポンスエンティティ
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleOutsideBusinessLogicException(Exception ex, WebRequest request) {
//        LogUtil.error("handleOutsideBusinessLogicException: ", ex);
        LogUtil.error(INCONSISTENT_ERROR_LOGCODE);
        return handleExceptionInternal(ex, dataCommonErrorResponse(), new HttpHeaders(), INTERNAL_SERVER_ERROR, request);
    }

    /** {@inheritDoc} */
    @Override
    @Nonnull
    protected ResponseEntity<Object> handleExceptionInternal(@Nonnull Exception ex, Object body, HttpHeaders headers,
            @Nonnull HttpStatus status, @Nonnull WebRequest request) {

        // HTTPステータスコードが「5**」の場合、エラーログ／スタックトレースを出力
        if (status.toString().startsWith(ERROR_HTTP_STATUS_CODE_PREFIX)) {
            logger.error(ex.toString(), ex);
        } else {
            logger.debug(ex.toString(), ex);
        }

        if (body == null) {
            try {
                String message = getMessage(ex.getClass().getName());
                //body = new CommonErrorResponse(Collections.singletonList(
                //        new ErrorResponse(status.toString(), message, null)
                //));

                /** エラー配列. */
                List<ErrorResponse> errors = new ArrayList<ErrorResponse>();
                ErrorResponse errorResponse = new ErrorResponse(status.toString(), message, null);
                errors.add(errorResponse);

                body = new CommonErrorResponse(errors);

            } catch (NoSuchMessageException e) {
                String code = "HttpStatus." + status.toString();
                String message = getMessageSource().getMessage(code, null, null, Locale.getDefault());
                if (message == null) {
                    if (logger.isWarnEnabled()) {
                        logger.warn("the exception has no message: " + ex.toString());
                    }
                    message = status.getReasonPhrase();
                }

                //body = new CommonErrorResponse(Collections.singletonList(
                //        new ErrorResponse(status.toString(), message, null)
                //));

                /** エラー配列. */
                List<ErrorResponse> errors = new ArrayList<ErrorResponse>();
                ErrorResponse errorResponse = new ErrorResponse(status.toString(), message, null);
                errors.add(errorResponse);

                body = new CommonErrorResponse(errors);
            }

        } else if (body instanceof List) {
            body = new CommonErrorResponse((List) body);

        } else if (body instanceof ErrorResponse) {
            body = new CommonErrorResponse(Collections.singletonList(body));
        }

        return super.handleExceptionInternal(ex, body, headers, status, request);
    }

    /**
     * エラーレスポンス生成(JSONパースエラー版：JsonMappingException).
     * @param cause the cause
     * @return errors
     */
    private List<ErrorResponse> errorResponseList(@Nonnull JsonMappingException cause) {
        val paths = cause.getPath();
        AtomicReference<String> classPathToField = new AtomicReference<>("");
        AtomicReference<String> fieldName = new AtomicReference<>("");
        return paths.stream().map(e -> {
            val from = e.getFrom();
            val canonicalName = from.getClass().getCanonicalName();

            String[] split = canonicalName.split("Request\\.", 2);
            classPathToField.set(split.length > 1 ? split[1] + "." : "");

            val camel = ResponseUtil.snakeCaseToCamelCase(e.getFieldName());
            if (camel != null) {
                fieldName.set(camel);
            }

            // XXX:hiradate 深さ2以上ネストされたリクエストに対しては未対応
            String localizedFieldName = fieldName.get();
            String fullFieldName = classPathToField.get() + fieldName.get();
            if (from instanceof Collection | from instanceof Map) {
                fullFieldName = classPathToField.get() + fieldName.get() + "[" + e.getIndex() + "]";
            } else {
                if (StringUtils.isEmpty(fieldName.get())) {
                    return errorResponse(REQUEST_FORMAT_ERROR_CODE, null);
                }
                try {
                    localizedFieldName = getMessage(fieldName.get());
                } catch (NoSuchMessageException e1) {
                    logger.error(e.toString(), e1);
                }
            }

            return jsonErrorResponse(from, fullFieldName, localizedFieldName);
        }).collect(Collectors.toList());
    }

    /**
     * エラーレスポンス生成(JSONパースエラー版).
     * @param from field instance or class
     * @param fieldName field name
     * @param localizedFieldName field name localized
     * @return errors
     */
    private ErrorResponse jsonErrorResponse(Object from, String fieldName, String localizedFieldName) {
        val descriptor = BeanUtils.getPropertyDescriptor(from.getClass(), fieldName);
        if (descriptor != null) {
            Class fieldClass = descriptor.getPropertyType();
            if (LocalDateTime.class.isAssignableFrom(fieldClass) || LocalDate.class.isAssignableFrom(fieldClass)) {
                return errorResponse(DATE_FORMAT_ERROR_CODE, fieldName, localizedFieldName);

            } else if (Number.class.isAssignableFrom(fieldClass)) {
                return errorResponse(NUMBER_ERROR_CODE, fieldName, localizedFieldName);
            }
        }
        return errorResponse(REQUEST_FORMAT_ERROR_CODE, fieldName, localizedFieldName);
    }

    /**
     * エラーレスポンス生成(FieldError版).
     * @param error バリデーションエラー
     * @return エラーレスポンスDTO
     */
    protected ErrorResponse errorResponse(@Nonnull FieldError error) {
        try {
            String msg = getMessageSource().getMessage(error, Locale.getDefault());
            Matcher matcher = VALIDATION_MESSAGE_PATTERN.matcher(msg);
            if (matcher.find()) {
                String code = matcher.group("code");
                String message = matcher.group("message");
                return new ErrorResponse(code, message, convertFieldName(error.getObjectName(), error.getField()));
            }
        } catch (NoSuchMessageException e) {
            logger.error(e.toString(), e);
        }
        return null;
    }


    /**
     * エラーレスポンス生成.
     * @param code エラーコード
     * @param fieldName フィールド名
     * @param args パラメータ
     * @return ErrorResponse
     */
    protected ErrorResponse errorResponse(@Nonnull String code, @Nullable String fieldName, Object... args) {
        String message = "";

        try {
            message = getMessage(code, args);
        } catch (NoSuchMessageException e1) {
            logger.error(e1.toString(), e1);
        }

        return new ErrorResponse(code, message, convertFieldName(null, fieldName));
    }

    // -------------------------------------------------------
    // CommonErrorResponse作成関連
    //-------------------------------------------------------

    /**
     * 共通エラーレスポンス生成（データ不整合エラー版）.
     * @return CommonErrorResponse list
     */
    protected CommonErrorResponse dataCommonErrorResponse() {
        return this.commonErrorResponse(INCONSISTENT_ERROR_CODE, null, null);
    }

    /**
     * 共通エラーレスポンス生成（BusinessLogicException版）.
     * @param ex BusinessLogicException
     * @return CommonErrorResponse list
     */
    protected CommonErrorResponse commonErrorResponse(@Nonnull UncheckedLogicException ex) {
        List<ExApiResponse> ExApiResponseList = null;
//        // 外部APIのエラーの場合
//        if (ex instanceof ExApiErrorException) {
//            // 外部APIのエラー情報を取得
//            ExApiErrorException exApiErrorException = (ExApiErrorException) ex;
//            ExApiResponseList = exApiErrorException.getExApiResponse();
//        }

        if (ex.getErrors() == null) {
            return this.commonErrorResponse(ex.getCode(), ex.getField(), ExApiResponseList, ex.getMessageParameters());
        } else {
                List<ErrorResponse> errors = new ArrayList<ErrorResponse>();
                ErrorResponse errorResponse = new ErrorResponse(ex.getCode(), ex.getField(), null, ExApiResponseList);
                errors.add(errorResponse);

                return new CommonErrorResponse(errors);
        }
    }

    /**
     * 共通エラーレスポンス生成（BusinessLogicException版）.
     * @param ex BusinessLogicException
     * @return CommonErrorResponse list
     */
    protected CommonErrorResponse commonErrorResponse(@Nonnull BusinessLogicException ex) {
        List<ExApiResponse> ExApiResponseList = null;
//        // 外部APIのエラーの場合
//        if (ex instanceof ExApiErrorException) {
//            // 外部APIのエラー情報を取得
//            ExApiErrorException exApiErrorException = (ExApiErrorException) ex;
//            ExApiResponseList = exApiErrorException.getExApiResponse();
//        }

        if (ex.getErrors() == null) {
            return this.commonErrorResponse(ex.getCode(), ex.getField(), ExApiResponseList, ex.getMessageParameters());
        } else {
                List<ErrorResponse> errors = new ArrayList<ErrorResponse>();
                ErrorResponse errorResponse = new ErrorResponse(ex.getCode(), ex.getField(), null, ExApiResponseList);
                errors.add(errorResponse);

                return new CommonErrorResponse(errors);
        }
    }

    /**
     * 共通エラーレスポンス生成.
     * @param code エラーコード
     * @param fieldName フィールド名
     * @param ExApiResponseList
     * @param args パラメータ
     * @return CommonErrorResponse
     */
    protected CommonErrorResponse commonErrorResponse(@Nonnull String code, @Nullable String fieldName,
            List<ExApiResponse> ExApiResponseList, Object... args) {

        /** エラー配列. */
        List<ErrorResponse> errors = new ArrayList<ErrorResponse>();
        ErrorResponse errorResponse = errorResponse(code, fieldName, args);
        errorResponse.setExApiResponse(ExApiResponseList);
        errors.add(errorResponse);

        return new CommonErrorResponse(errors);

    }

    /**
     * jsonPropertiesが設定された項目に対して、エラーレスポンスのfieldを訂正する。.
     * @param objName オブジェクト名
     * @param fieldName フィールド名
     * @return jsonProperty 設定された場合：proterty名；そのたの場合、キャメルケースをスネークケースに変換された名
     */
    private String convertFieldName(String objName, String fieldName) {

        if (StringUtils.isEmpty(fieldName)) {
            return "";
        }
        if (!StringUtils.isEmpty(objName)) {

            ClassPathScanningCandidateComponentProvider provider = new ClassPathScanningCandidateComponentProvider(
                    false);
            provider.addIncludeFilter(new RegexPatternTypeFilter(Pattern.compile(".*")));

            Optional<?> clsOptional = provider.findCandidateComponents(REQUEST_PACKAGE_MAEKYU)
                    .stream().map(bean -> {
                        try {
                            return Class.forName(bean.getBeanClassName());
                        } catch (ClassNotFoundException e) {
                            return null;
                        }
                    }).filter(o -> o != null).filter(it -> {
                        return StringUtils.equals(it.getSimpleName(),
                                objName.substring(0, 1).toUpperCase() + objName.substring(1));
                    }).findFirst();

            if (clsOptional.isPresent()) {
                Class<?> cls = (Class<?>) clsOptional.get();
                Optional<Field> fidOptional = Arrays.stream(cls.getDeclaredFields())
                        .filter(it -> StringUtils.equals(it.getName(), fieldName)).findFirst();

                if (fidOptional.isPresent()) {
                    Field field = fidOptional.get();
                    Optional<Annotation> antOptional = Arrays.stream(field.getAnnotations())
                            .filter(it -> it instanceof JsonProperty).findFirst();

                    if (antOptional.isPresent()) {
                        val jsonP = (JsonProperty) antOptional.get();
                        return jsonP.value();
                    }
                }
            }
        }
        return ResponseUtil.camelCaseToSnakeCase(fieldName);
    }

//	@ExceptionHandler(IssueAccessTokenException.class)
//	public ResponseEntity<Object> handleIssueAccessTokenException(IssueAccessTokenException exception, WebRequest request) {
//		HttpHeaders headers = new HttpHeaders();
//
//		IssueAccessTokenErrorResponseBody issueAccessTokenErrorResponseBody = new IssueAccessTokenErrorResponseBody();
//		IssueAccessTokenErrorResponseNotBody issueAccessTokenErrorResponseNotBody = new IssueAccessTokenErrorResponseNotBody();
//		Object body = null;
//		HttpStatus httpStatus= null;
//		switch (exception.getErrorDetails().getErrCode()) {
//		case "6015":
//		case "6305":
//			issueAccessTokenErrorResponseBody.setErrorCode(exception.getErrorDetails().getErrCode());
//			issueAccessTokenErrorResponseBody.setErrorDetail("");
//			// 500
//			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
//			body = issueAccessTokenErrorResponseBody;
//			break;
//		case "6150":
//			issueAccessTokenErrorResponseBody.setErrorCode(exception.getErrorDetails().getErrCode());
//			issueAccessTokenErrorResponseBody.setErrorDetail("");
//			// 400
//			httpStatus = HttpStatus.BAD_REQUEST;
//			body = issueAccessTokenErrorResponseBody;
//			break;
//		case "6304":
//			issueAccessTokenErrorResponseBody.setErrorCode(exception.getErrorDetails().getErrCode());
//			issueAccessTokenErrorResponseBody.setErrorDetail("");
//			// 503
//			  httpStatus = HttpStatus.SERVICE_UNAVAILABLE;
//			body = issueAccessTokenErrorResponseBody;
//			break;
//		case "E001":
//			issueAccessTokenErrorResponseNotBody.setErrorCode(exception.getErrorDetails().getErrCode());
//			// 401
//			httpStatus = HttpStatus.UNAUTHORIZED;
//			body = issueAccessTokenErrorResponseNotBody;
//			break;
//		case "E010":
//		case "E011":
//		case "E012":
//			issueAccessTokenErrorResponseNotBody.setErrorCode(exception.getErrorDetails().getErrCode());
//			// 400
//			httpStatus = HttpStatus.BAD_REQUEST;
//			body = issueAccessTokenErrorResponseNotBody;
//			break;
//		}
//
//		return super.handleExceptionInternal(exception,
//				body,
//				headers,
//				httpStatus,
//				request);
//	}
}
