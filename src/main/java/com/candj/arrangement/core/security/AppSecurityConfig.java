package com.candj.arrangement.core.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import lombok.extern.slf4j.Slf4j;

/**
 * AppSecurityConfig.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Slf4j
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {
	/** proxyHost. */
	@Value("${proxy.host}")
	private String proxyHost;

	/** proxyPort. */
	@Value("${proxy.port}")
	private String proxyPort;

	@Autowired
	UserDetailsService userDetailsService;

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/js/**", "/css/**", "/fonts/**", "/plugin/**", "/img/**");
	}

	/** {@inheritDoc} */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		log.debug(this.getClass().getSimpleName() + ".configure(HttpSecurity)");
		http

				.csrf().disable() // CSRF対策無効
				//.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // リクエスト毎に認証実施
				//.and()

				// 認可の設定
				.authorizeRequests() // 認証なしでアクセス可能なURLを設定
				.antMatchers(this.unUseAuthTokens()).permitAll() // 上記パスへのアクセスを許可
				.anyRequest().authenticated() // 上記以外は認証が必要となる設定
				.and()
				// エラー時の処理
				.exceptionHandling().authenticationEntryPoint(this.authenticationEntryPoint())
				.and()
				.formLogin().loginProcessingUrl("/login").loginPage("/signin")
				.failureUrl("/admin").defaultSuccessUrl("/main", false)
				.usernameParameter("username").passwordParameter("password")
				.and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/signout"))
				.logoutSuccessUrl("/admin")
				.deleteCookies("JSESSIONID")
				.invalidateHttpSession(true).permitAll();
		http.sessionManagement().invalidSessionUrl("/admin");

		http.headers()
	      .frameOptions()
	         .sameOrigin();
	}

	/** {@inheritDoc}
	 * @throws Exception */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
				.userDetailsService(userDetailsService)
				.passwordEncoder(passwordEncoder())
				;
	}

	/**
	 * @return {@link SessionExpiredDetectingLoginUrlAuthenticationEntryPoint}.
	 */
	@Bean
	public SessionExpiredDetectingLoginUrlAuthenticationEntryPoint authenticationEntryPoint() {
		return new SessionExpiredDetectingLoginUrlAuthenticationEntryPoint("/admin");
	}

	/**
	 * @return {@link PasswordEncoder}.
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new PasswordEncoder() {
			@Override
			public String encode(CharSequence rawPassword) {
				// TODO 自動生成されたメソッド・スタブ
				return String.valueOf(rawPassword);
			}

			@Override
			public boolean matches(CharSequence rawPassword, String encodedPassword) {
				return encodedPassword.equals(String.valueOf(rawPassword));
			}

		};
	}

	/**
	 * クライアントを使用しないAPI.
	 *
	 * @return {@link String[]}.
	 */
	private String[] unUseAuthTokens() {
		return new String[] {
				"/admin/**",
				"/login/**",
				"/login?**",
				"/logout/**",
				"/user/**"
				,"/clientRest/**"
		};
	}
}
