package com.candj.arrangement.core.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

/**
 * AppAuthenticationEntryPoint.
 */
public class SessionExpiredDetectingLoginUrlAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint {

    public SessionExpiredDetectingLoginUrlAuthenticationEntryPoint(String loginFormUrl) {
		super(loginFormUrl);
	}

	/** {@inheritDoc} */
    @Override
    public void commence(HttpServletRequest httpServletRequest,
                         HttpServletResponse httpServletResponse,
                         AuthenticationException e) throws IOException, ServletException {
//        log.debug(this.getClass().getSimpleName() + ".commence");
//
//
        System.out.println(httpServletRequest.getRequestURL());
//        System.out.println(httpServletRequest.getHeader("X-Requested-With"));
//
//        if ("XMLHttpRequest".equals(httpServletRequest.getHeader("X-Requested-With"))) {
//        	httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//            return;
//        }
//
//        String msg = "";
//        httpServletResponse.setContentType("application/json");
//        httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//        httpServletResponse.getOutputStream().println(
//                "{ \"errors\": [{\"code\": \"C000-0003\", \"message\": \"" + msg + "\", \"field\": \"null\"}]}");
    	if ("XMLHttpRequest".equals(httpServletRequest.getHeader("X-Requested-With"))) {
    		httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        super.commence(httpServletRequest, httpServletResponse, e);
    }

    @Override
    protected String buildRedirectUrlToLoginPage(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) {

        String redirectUrl = super.buildRedirectUrlToLoginPage(request, response, authException);
        if (isRequestedSessionInvalid(request)) {
            redirectUrl += redirectUrl.contains("?") ? "&" : "?";
            redirectUrl += "timeout";
        }
        return redirectUrl;
    }

    private boolean isRequestedSessionInvalid(HttpServletRequest request) {
        return request.getRequestedSessionId() != null && !request.isRequestedSessionIdValid();
    }

}
