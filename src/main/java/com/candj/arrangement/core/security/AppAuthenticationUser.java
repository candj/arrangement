package com.candj.arrangement.core.security;

import java.util.Collection;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.candj.webpower.web.core.security.Principal;
import com.candj.arrangement.mapper.model.AppUser;

import lombok.Getter;

/**
 * AppAuthenticationUser.
 */
@Getter
public class AppAuthenticationUser extends User implements Principal {

	/** アプリユーザ. */
	@Nonnull
	private AppUser appUser;

	/**
	 * コンストラクタです.
	 *
	 * @param tokenString トークン文字列.
	 * @param indirectAuthSession 間接認証セッション.
	 * @param appUserAuthToken アプリユーザ認証トークン.
	 * @param appUser アプリユーザ.
	 * @param isUseTempAuthToken 仮認証トークン判別フラグ.
	 * @param walletService ウォレットサービス
	 */
	public AppAuthenticationUser(
			@Nonnull AppUser appUser, Collection<GrantedAuthority> authList) {
		super(appUser.getUserCode(),
				StringUtils.isEmpty(appUser.getPassword()) ? "" : appUser.getPassword(),
				authList);

		this.appUser = appUser;

	}

	/** {@inheritDoc} */
	@Nullable
	@Override
	public String getId() {
		return Optional.ofNullable(appUser).map(AppUser::getUserId).map(String::valueOf).orElse(null);
	}

}
