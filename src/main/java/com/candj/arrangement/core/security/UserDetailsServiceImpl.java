package com.candj.arrangement.core.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.arrangement.dao.mapper.AppUserDao;
import com.candj.arrangement.dto.UserRoleDto;
import com.candj.arrangement.service.UserRoleService;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	AppUserDao userDao;
	@Autowired
	UserRoleService userRoleService;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

		try {
			WhereCondition whereCondition = new WhereCondition();
			whereCondition.createCriteria()
					.andEqualTo("user_code", userName);
			//条件でユーザーを検索する。（連携情報含む）)
			List<com.candj.arrangement.mapper.model.AppUser> users = userDao.selectByExample(whereCondition);
			com.candj.arrangement.mapper.model.AppUser user = users.stream().findFirst().orElseThrow(() -> {
				throw new UsernameNotFoundException("user not exist. userName=" + userName);
			});

			Collection<GrantedAuthority> authList = getAuthorities(user.getUserId());

			return new AppAuthenticationUser(user, authList);

		} catch (Throwable e) {
			e.printStackTrace();
			throw new UsernameNotFoundException(e.getMessage());
		}

	}

	private Collection<GrantedAuthority> getAuthorities(Integer userId) throws UsernameNotFoundException {
		UserRoleDto dto = new UserRoleDto();
		dto.setUserId(userId);
		List<UserRoleDto> list = userRoleService.selectAllByExample(dto);
		if (CollectionUtils.isEmpty(list)) {
			throw new UsernameNotFoundException("has no Authority");
		}
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
		for (UserRoleDto d : list) {
			authList.add(new SimpleGrantedAuthority(d.getRole().getRoleCode()));
		}
		return authList;
	}

}
