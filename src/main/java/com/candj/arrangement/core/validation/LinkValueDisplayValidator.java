package com.candj.arrangement.core.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 前給表示対象検証.
 */
public class LinkValueDisplayValidator implements ConstraintValidator<LinkValueDisplay, Object> {

    /** リンク区分=4（懸賞）の場合、表示対象. */
    private static final String[] PRIZE_DISPLAYS = { "login", "menu", "order-complete" };

    /** リンク区分=5（メインメニュー）の場合、表示対象. */
    private static final String[] MAIN_MENU_DISPLAYS = { "pc-menu", "pc-order-complete", "app-menu", "app-order-complete" };

    /** リンク区分=4（懸賞）. */
    private static final String LINK_TYPE_PRIZE = "4";

    /** リンク区分=5（メインメニュー）. */
    private static final String LINK_TYPE_MAINMENU = "5";
    
    /** リンク区分. */
    private String linkTypeProperty;
    
    /** 表示対象. */
    private String displayProperty;

    @Override
    public void initialize(LinkValueDisplay constraintAnnotation) {
        this.linkTypeProperty = constraintAnnotation.linkTypeProperty();
        this.displayProperty = constraintAnnotation.displayProperty();
    }

    public boolean isValid(Object form, ConstraintValidatorContext context) {
        // フォームクラスから比較対象項目の値を得る
        BeanWrapper beanWrapper = new BeanWrapperImpl(form);
        String linkType = (String) beanWrapper.getPropertyValue(linkTypeProperty);
        String display = (String) beanWrapper.getPropertyValue(displayProperty);

        // リンク区分が４（懸賞）、５（メインメニュー等）、表示対象が空白
        if ((StringUtils.equals(linkType, LINK_TYPE_PRIZE) || StringUtils.equals(linkType, LINK_TYPE_MAINMENU)) && StringUtils.isBlank(display)) {
            context.disableDefaultConstraintViolation();
            // 必須入力項目です。
            context.buildConstraintViolationWithTemplate("{javax.validation.constraints.NotEmpty.message}").addPropertyNode(displayProperty).addConstraintViolation();
            return false;
        }

        if ((StringUtils.equals(LINK_TYPE_PRIZE, linkType) && !Arrays.asList(PRIZE_DISPLAYS).contains(display))
                || (StringUtils.equals(LINK_TYPE_MAINMENU, linkType) && !Arrays.asList(MAIN_MENU_DISPLAYS).contains(display))) {
            context.disableDefaultConstraintViolation();
            // 不正な項目、値が含まれています。
            context.buildConstraintViolationWithTemplate("{javax.validation.constraints.Pattern.message}").addPropertyNode(displayProperty).addConstraintViolation();
            return false;
        }

        return true;
    }
}
