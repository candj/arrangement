package com.candj.arrangement.core.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import com.candj.webpower.web.core.domain.service.LogConfiguration;

import java.util.Map;

/**
 * ウォレットアプ用の運用ログのメッセージ定義を読み込み保持します.
 */
@ConfigurationProperties(prefix = "operation-log")
@Data
public class LogConfig implements LogConfiguration {

    /** ログメッセージ<code>Map</code>. */
    private Map<String, String> messages;

    /**
     * コードを元にメッセージを返却します.
     *
     * @param code メッセージコード
     * @return メッセージ
     */
    public String message(String code) {
        return this.messages.getOrDefault(code, "対象のメッセージコードが存在しません。");
    }
}
