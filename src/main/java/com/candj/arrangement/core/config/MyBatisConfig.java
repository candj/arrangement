package com.candj.arrangement.core.config;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.candj.arrangement.dao.mapper")
public class MyBatisConfig {
}
