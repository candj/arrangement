package com.candj.arrangement.core.repository;
//package com.candj.arrangement.repository;
//
//import java.util.Optional;
//
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.stereotype.Repository;
//
//import com.candj.webpower.web.core.domain.enums.AppUserStatus;
//import com.candj.webpower.web.core.exception.dao.IncorrectDataUpdateException;
//import com.candj.webpower.web.core.repository.infra.mapper.AppUserMapper;
//import com.candj.webpower.web.core.repository.infra.table.AppUser;
//import com.candj.arrangement.repository.mapper.AppUserApiMapper;
//
///**
// * AppUserApiRepository
// */
//@Repository
//public class AppUserApiRepository {
//
//    // AppUserMapper
//   // @Autowired
//    private AppUserMapper coreMapper;
//
//    // AppUserApiMapper
//  //  @Autowired
//    private AppUserApiMapper apiMapper;
//
//    /**
//     * IDをキーとして、アプリユーザ情報を取得する.
//     *
//     * @param id
//     * @return アプリユーザ情報
//     */
//    public Optional<AppUser> findById(Long id) {
//        AppUser appUser = coreMapper.selectByPrimaryKey(id);
//        return Optional.ofNullable(appUser);
//    }
//
//    /**
//     *  フレンドコードからアプリユーザ情報を取得する
//     * @param friendCode フレンドコード
//     * @return　アプリユーザ
//     */
//    public Optional<AppUser> findByFriendCode(String friendCode) {
//        AppUser appUser = apiMapper.findByFriendCode(friendCode);
//        return Optional.ofNullable(appUser);
//    }
//
//    public Optional<AppUser> findNotWithdrawnByPhone(String walletServiceId, String phone) {
//        AppUser appUser = apiMapper.findNotWithdrawnByPhone(walletServiceId, phone, AppUserStatus.WITHDRAWAL.getCode());
//        return Optional.ofNullable(appUser);
//    }
//
//    /**
//     * ウォレットサービスID、電話番号をキーにアプリユーザ情報を取得します.
//     * （退会しているユーザは除く）
//     *
//     * @param walletServiceId ウォレットサービスID
//     * @param phone 電話番号
//     * @return アプリユーザ情報
//     */
//    public Optional<AppUser> findAvailableByPhone(String walletServiceId, String phone) {
//        AppUser appUser = apiMapper.findAvailableByPhone(walletServiceId, phone, AppUserStatus.AVAILABLE.getCode());
//        return Optional.ofNullable(appUser);
//    }
//
//    /**
//     * アプリユーザ情報を登録・更新します.
//     *
//     * @param appUser アプリユーザ情報
//     * @return アプリユーザ情報
//     */
//    public AppUser save(AppUser appUser) {
//        if (appUser.getId() == null) {
//            coreMapper.insert(appUser);
//        } else {
//            coreMapper.updateByPrimaryKey(appUser);
//        }
//        return appUser;
//    }
//
//    /**
//     * アプリユーザを取得する.
//     *
//     * @param walletServiceId ウォレットサービスID
//     * @param email メールアドレス
//     * @param status アプリユーザステータス
//     * @return walletService アプリユーザエンティティ
//     */
//    public Optional<AppUser> findByWalletServiceIdAndEmailAndStatus(String walletServiceId, String email, String status) {
//        AppUser appUser = apiMapper.findByWalletServiceIdAndEmailAndStatus(walletServiceId, email, status);
//        return Optional.ofNullable(appUser);
//    }
//
//    /**
//     * アプリユーザを一部更新する.
//     *
//     * @param appUser アプリユーザ情報
//     * @return 更新件数
//     */
//    public int update(AppUser appUser) {
//
//        if (appUser.getId() == null) {
//            throw new IncorrectDataUpdateException();
//        }
//        int ret = coreMapper.updateByPrimaryKeySelective(appUser);
//        return ret;
//    }
//
//    /**
//     * アプリユーザを更新する（paramのアプリユーザ情報に値がある項目のみ更新）.
//     *
//     * @param appUser アプリユーザ情報
//     */
//    public void updateByPrimaryKeySelective(AppUser appUser) {
//        coreMapper.updateByPrimaryKeySelective(appUser);
//    }
//
//    /**
//     * パスワード試行回数、アカウントロック日時を更新する.
//     *
//     * @param appUser アプリユーザ情報
//     */
//    public void updateLockedAtAndPasswordAttemps(AppUser appUser) {
//        apiMapper.updateLockedAtAndPasswordAttemps(appUser);
//    }
//
//    /**
//     * メールアドレスより指定されたID以外のアプリユーザを取得する.
//     *
//     * @param walletServiceId ウォレットサービスID
//     * @param id アプリユーザID
//     * @param email メールアドレス
//     * @param status アプリユーザステータス
//     * @return アプリユーザエンティティ
//     */
//    public Optional<AppUser> findNotIdByEmail(String walletServiceId, Long id, String email, String status) {
//        AppUser appUser = apiMapper.findNotIdByEmail(walletServiceId, id, email, status);
//        return Optional.ofNullable(appUser);
//    }
//
//    /**
//     * メールアドレスより指定されたID以外のアプリユーザを取得する.
//     * 退会済みユーザも検索対象とする
//     *
//     * @param walletServiceId ウォレットサービスID
//     * @param id アプリユーザID
//     * @param email メールアドレス
//     * @return アプリユーザエンティティ
//     */
//    public Optional<AppUser> findWalletServiceIdAndNotIdByEmail(String walletServiceId, Long id, String email) {
//        AppUser appUser = apiMapper.findWalletServiceIdAndNotIdByEmail(walletServiceId, id, email);
//        return Optional.ofNullable(appUser);
//    }
//
//    /**
//     * ウォレットサービスID、メールアドレス、端末識別子からアプリユーザ情報を取得.
//     *
//     * @param walletServiceId ウォレットサービスID
//     * @param email メールアドレス
//     * @param deviceId 端末識別子
//     * @return アプリユーザ情報
//     */
//    public AppUser findByWsIdEmailDeviceId(String walletServiceId, String email, String deviceId) {
//        return apiMapper.findByWsIdEmailDeviceId(walletServiceId, email, deviceId);
//    }
//
//    /**
//     * 電話番号から有効なアプリユーザ情報を取得.
//     *
//     * @param phone 電話番号
//     * @return アプリユーザ情報
//     */
//    public Optional<AppUser> findByPhoneStatus(String phone) {
//        AppUser appUser = apiMapper.findByPhoneStatus(phone, AppUserStatus.AVAILABLE.getCode());
//        return Optional.ofNullable(appUser);
//    }
//
//    /**
//     * メールアドレスから有効なアプリユーザ情報を取得.
//     *
//     * @param email メールアドレス
//     * @return アプリユーザ情報
//     */
//    public Optional<AppUser> findByEmailStatus(String email) {
//        AppUser appUser = apiMapper.findByEmailStatus(email, AppUserStatus.AVAILABLE.getCode());
//        return Optional.ofNullable(appUser);
//    }
//
//    /**
//     * ウォレットサービスIDとニックネームからアプリユーザ情報を取得.
//     *
//     * @param id アプリユーザID
//     * @param status ステータス
//     * @param walletServiceId ウォレットサービスID
//     * @param nickname ニックネーム
//     * @return アプリユーザ情報
//     */
//    public Optional<AppUser> findByIdWsIdNickname(Long id, String status, String walletServiceId, String nickname) {
//        AppUser appUser = apiMapper.findByIdWsIdNickname(id, status, walletServiceId, nickname);
//        return Optional.ofNullable(appUser);
//    }
//
//    /**
//     * IDとステータスよりアプリユーザを取得する.
//     *
//     * @param id ID
//     * @param status ステータス
//     * @return アプリユーザ情報
//     */
//    public Optional<AppUser> findByIdAndStatus(Long id, String status) {
//        return Optional.ofNullable(apiMapper.findByIdAndStatus(id, status));
//    }
//
//    /**
//     * アプリユーザIDよりアプリユーザ情報を更新.
//     *
//     * @param record アプリユーザ情報
//     */
//    public void updateByAppUserId(AppUser record) {
//        coreMapper.updateByPrimaryKeySelective(record);
//    }
//
//    /**
//     * 引数をキーとして、間接認証セッション情報を取得.
//     *
//     * @param walletServiceId ウォレットサービスID
//     * @param walletSecret ウォレットサービス鍵
//     * @param email メールアドレス
//     * @param status ステータス
//     * @return アプリユーザ情報
//     */
//    public Optional<AppUser> findEmailAddress(String walletServiceId, String walletSecret, String email, String status) {
//        return Optional.ofNullable(apiMapper.findEmailAddress(walletServiceId, walletSecret, email, status));
//    }
//
//    /**
//     * アプリユーザID、パスワード、機器情報、アカウントロック状態を更新.
//     *
//     * @param record アプリユーザ情報
//     */
//    public void updatePasswordAndDeviceinfoAndLockedAt(AppUser record) {
//        apiMapper.updatePasswordAndDeviceinfoAndLockedAt(record);
//    }
//
//    /**
//     * プリユーザ情報を取得
//     *
//     * @param appUserid アプリユーザID
//     * @return アプリユーザ情報
//     */
//    public Optional<AppUser> findAvailableById(long appUserid) {
//        AppUser appUser = coreMapper.selectByPrimaryKey(appUserid);
//        if (appUser != null) {
//            if (!StringUtils.equals(appUser.getStatus(), AppUserStatus.AVAILABLE.getCode())) {
//                return Optional.ofNullable(null);
//            }
//        }
//        return Optional.ofNullable(appUser);
//    }
//
//    /**
//     * ウォレット本人確認状態を更新する.
//     *
//     * @param id アプリユーザID
//     */
//    public void updatePendingStatus(Long id) {
//        apiMapper.updatePendingStatus(id);
//    }
//}
//
