package com.candj.arrangement.dao.mapper;

import com.candj.arrangement.dto.StaffDto;
import com.candj.arrangement.mapper.model.Staff;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * スタッフ情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface StaffDao {
    /**
     * プライマリーキーでスタッフを検索する。
     * @param staffId スタッフID
     * @return 結果
     */
    Staff selectByPrimaryKey(Integer staffId);

    /**
     * プライマリーキーでスタッフ検索する。（連携情報含む）
     * @param staffId スタッフID
     * @return 結果
     */
    List<Staff> selectAllByPrimaryKey(Integer staffId);

    /**
     * 条件でスタッフを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Staff> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件でスタッフを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Staff> selectByExample(@Param("example") WhereCondition example);

    /**
     * スタッフを新規追加する。
     * @param record スタッフ
     * @return 結果
     */
    int insertSelective(Staff record);

    /**
     * スタッフを新規追加する。
     * @param record スタッフ
     * @return 結果
     */
    int insert(Staff record);

    /**
     * プライマリーキーでスタッフを更新する。
     * @param record スタッフ
     * @return 結果
     */
    int updateByPrimaryKey(Staff record);

    /**
     * プライマリーキーでスタッフを更新する。
     * @param record スタッフ
     * @return 結果
     */
    int updateByPrimaryKeySelective(Staff record);

    /**
     * スタッフを更新する。
     * @param record スタッフ
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Staff record, @Param("example") WhereCondition example);

    /**
     * スタッフを削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Staff example);

    /**
     * スタッフを削除する。
     * @param staffId スタッフID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer staffId);

    int selectCountByExample(@Param("example") WhereCondition whereCondition);

}
