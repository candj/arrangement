package com.candj.arrangement.dao.mapper;

import com.candj.arrangement.mapper.model.Task;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.joda.time.DateTime;

/**
 * タスク情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface TaskDao {
    /**
     * プライマリーキーでタスクを検索する。
     * @param taskId タスクID
     * @return 結果
     */
    Task selectByPrimaryKey(Integer taskId);

    /**
     * プライマリーキーでタスク検索する。（連携情報含む）
     * @param taskId タスクID
     * @return 結果
     */
    List<Task> selectAllByPrimaryKey(Integer taskId);

    /**
     * 条件でタスクを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Task> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件でタスクを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Task> selectByExample(@Param("example") WhereCondition example);

    List<Task> selectByExampleWithTime(@Param("example") WhereCondition example, @Param("start") DateTime start, @Param("finish") DateTime finish);

    /**
     * タスクを新規追加する。
     * @param record タスク
     * @return 結果
     */
    int insertSelective(Task record);

    /**
     * タスクを新規追加する。
     * @param record タスク
     * @return 結果
     */
    int insert(Task record);

    /**
     * プライマリーキーでタスクを更新する。
     * @param record タスク
     * @return 結果
     */
    int updateByPrimaryKey(Task record);

    /**
     * プライマリーキーでタスクを更新する。
     * @param record タスク
     * @return 結果
     */
    int updateByPrimaryKeySelective(Task record);

    /**
     * タスクを更新する。
     * @param record タスク
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Task record, @Param("example") WhereCondition example);

    /**
     * タスクを削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Task example);

    /**
     * タスクを削除する。
     * @param taskId タスクID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer taskId);

    int selectCountByExample(@Param("example") WhereCondition whereCondition);
}
