package com.candj.arrangement.dao.mapper;

import com.candj.arrangement.mapper.model.StaffVisa;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * スタッフ_签证種類情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface StaffVisaDao {
    /**
     * プライマリーキーでスタッフ_签证種類を検索する。
     * @param id ID
     * @return 結果
     */
    StaffVisa selectByPrimaryKey(Integer id);

    /**
     * プライマリーキーでスタッフ_签证種類検索する。（連携情報含む）
     * @param id ID
     * @return 結果
     */
    List<StaffVisa> selectAllByPrimaryKey(Integer id);

    /**
     * 条件でスタッフ_签证種類を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<StaffVisa> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件でスタッフ_签证種類を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<StaffVisa> selectByExample(@Param("example") WhereCondition example);

    /**
     * スタッフ_签证種類を新規追加する。
     * @param record スタッフ_签证種類
     * @return 結果
     */
    int insertSelective(StaffVisa record);

    /**
     * スタッフ_签证種類を新規追加する。
     * @param record スタッフ_签证種類
     * @return 結果
     */
    int insert(StaffVisa record);

    /**
     * プライマリーキーでスタッフ_签证種類を更新する。
     * @param record スタッフ_签证種類
     * @return 結果
     */
    int updateByPrimaryKey(StaffVisa record);

    /**
     * プライマリーキーでスタッフ_签证種類を更新する。
     * @param record スタッフ_签证種類
     * @return 結果
     */
    int updateByPrimaryKeySelective(StaffVisa record);

    /**
     * スタッフ_签证種類を更新する。
     * @param record スタッフ_签证種類
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(StaffVisa record, @Param("example") WhereCondition example);

    /**
     * スタッフ_签证種類を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") StaffVisa example);

    /**
     * スタッフ_签证種類を削除する。
     * @param id ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer id);
}
