package com.candj.arrangement.dao.mapper;

import com.candj.arrangement.dto.ShiftworkDto;
import com.candj.arrangement.mapper.model.Shiftwork;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;

import jdk.nashorn.internal.objects.annotations.Where;
import org.apache.ibatis.annotations.Param;
import org.joda.time.DateTime;

/**
 * シフト情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface ShiftworkDao {
    /**
     * プライマリーキーでシフトを検索する。
     * @param shiftworkId シフトID
     * @return 結果
     */
    Shiftwork selectByPrimaryKey(Integer shiftworkId);

    /**
     * プライマリーキーでシフト検索する。（連携情報含む）
     * @param shiftworkId シフトID
     * @return 結果
     */
    List<Shiftwork> selectAllByPrimaryKey(Integer shiftworkId);

    /**
     * 条件でシフトを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Shiftwork> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件でシフトを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Shiftwork> selectByExample(@Param("example") WhereCondition example);

    /**
     * シフトを新規追加する。
     * @param record シフト
     * @return 結果
     */
    int insertSelective(Shiftwork record);

    /**
     * シフトを新規追加する。
     * @param record シフト
     * @return 結果
     */
    int insert(Shiftwork record);

    /**
     * プライマリーキーでシフトを更新する。
     * @param record シフト
     * @return 結果
     */
    int updateByPrimaryKey(Shiftwork record);

    /**
     * プライマリーキーでシフトを更新する。
     * @param record シフト
     * @return 結果
     */
    int updateByPrimaryKeySelective(Shiftwork record);

    /**
     * シフトを更新する。
     * @param record シフト
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Shiftwork record, @Param("example") WhereCondition example);

    /**
     * シフトを削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Shiftwork example);

    /**
     * シフトを削除する。
     * @param shiftworkId シフトID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer shiftworkId);

    int selectCountByExample(@Param("example") WhereCondition whereCondition);

    List<ShiftworkDto> selectDayShift(@Param("startTime") DateTime startTime, @Param("endTime") DateTime endTime);
}
