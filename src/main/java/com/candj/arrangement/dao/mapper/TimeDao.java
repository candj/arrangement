package com.candj.arrangement.dao.mapper;

import com.candj.arrangement.mapper.model.Time;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 時間情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface TimeDao {
    /**
     * プライマリーキーで時間を検索する。
     * @param timeId シフトID
     * @return 結果
     */
    Time selectByPrimaryKey(Integer timeId);

    /**
     * プライマリーキーで時間検索する。（連携情報含む）
     * @param timeId シフトID
     * @return 結果
     */
    List<Time> selectAllByPrimaryKey(Integer timeId);

    /**
     * 条件で時間を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Time> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で時間を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Time> selectByExample(@Param("example") WhereCondition example);

    /**
     * 時間を新規追加する。
     * @param record 時間
     * @return 結果
     */
    int insertSelective(Time record);

    /**
     * 時間を新規追加する。
     * @param record 時間
     * @return 結果
     */
    int insert(Time record);

    /**
     * プライマリーキーで時間を更新する。
     * @param record 時間
     * @return 結果
     */
    int updateByPrimaryKey(Time record);

    /**
     * プライマリーキーで時間を更新する。
     * @param record 時間
     * @return 結果
     */
    int updateByPrimaryKeySelective(Time record);

    /**
     * 時間を更新する。
     * @param record 時間
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Time record, @Param("example") WhereCondition example);

    /**
     * 時間を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Time example);

    /**
     * 時間を削除する。
     * @param timeId シフトID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer timeId);
}
