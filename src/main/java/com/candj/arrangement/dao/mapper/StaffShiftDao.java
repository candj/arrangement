package com.candj.arrangement.dao.mapper;

import com.candj.arrangement.mapper.model.StaffShift;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * スタッフ_シフト情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface StaffShiftDao {
    /**
     * プライマリーキーでスタッフ_シフトを検索する。
     * @param id ID
     * @return 結果
     */
    StaffShift selectByPrimaryKey(Integer id);

    /**
     * プライマリーキーでスタッフ_シフト検索する。（連携情報含む）
     * @param id ID
     * @return 結果
     */
    List<StaffShift> selectAllByPrimaryKey(Integer id);

    /**
     * 条件でスタッフ_シフトを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<StaffShift> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件でスタッフ_シフトを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<StaffShift> selectByExample(@Param("example") WhereCondition example);

    /**
     * スタッフ_シフトを新規追加する。
     * @param record スタッフ_シフト
     * @return 結果
     */
    int insertSelective(StaffShift record);

    /**
     * スタッフ_シフトを新規追加する。
     * @param record スタッフ_シフト
     * @return 結果
     */
    int insert(StaffShift record);

    /**
     * プライマリーキーでスタッフ_シフトを更新する。
     * @param record スタッフ_シフト
     * @return 結果
     */
    int updateByPrimaryKey(StaffShift record);

    /**
     * プライマリーキーでスタッフ_シフトを更新する。
     * @param record スタッフ_シフト
     * @return 結果
     */
    int updateByPrimaryKeySelective(StaffShift record);

    /**
     * スタッフ_シフトを更新する。
     * @param record スタッフ_シフト
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(StaffShift record, @Param("example") WhereCondition example);

    /**
     * スタッフ_シフトを削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") StaffShift example);

    /**
     * スタッフ_シフトを削除する。
     * @param id ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer id);

    int selectCountByExample(@Param("example") WhereCondition whereCondition);

    List<StaffShift> selectSuitables(Integer id);
}
