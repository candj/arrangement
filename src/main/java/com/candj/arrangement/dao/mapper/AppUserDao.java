package com.candj.arrangement.dao.mapper;

import com.candj.arrangement.mapper.model.AppUser;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 管理者情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface AppUserDao {
    /**
     * プライマリーキーで管理者を検索する。
     * @param userId 管理者ID
     * @return 結果
     */
    AppUser selectByPrimaryKey(Integer userId);

    /**
     * プライマリーキーで管理者検索する。（連携情報含む）
     * @param userId 管理者ID
     * @return 結果
     */
    List<AppUser> selectAllByPrimaryKey(Integer userId);

    /**
     * 条件で管理者を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<AppUser> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で管理者を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<AppUser> selectByExample(@Param("example") WhereCondition example);

    /**
     * 管理者を新規追加する。
     * @param record 管理者
     * @return 結果
     */
    int insertSelective(AppUser record);

    /**
     * 管理者を新規追加する。
     * @param record 管理者
     * @return 結果
     */
    int insert(AppUser record);

    /**
     * プライマリーキーで管理者を更新する。
     * @param record 管理者
     * @return 結果
     */
    int updateByPrimaryKey(AppUser record);

    /**
     * プライマリーキーで管理者を更新する。
     * @param record 管理者
     * @return 結果
     */
    int updateByPrimaryKeySelective(AppUser record);

    /**
     * 管理者を更新する。
     * @param record 管理者
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(AppUser record, @Param("example") WhereCondition example);

    /**
     * 管理者を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") AppUser example);

    /**
     * 管理者を削除する。
     * @param userId 管理者ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer userId);
}
