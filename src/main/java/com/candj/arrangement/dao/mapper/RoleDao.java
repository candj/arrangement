package com.candj.arrangement.dao.mapper;

import com.candj.arrangement.mapper.model.Role;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * ロール情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface RoleDao {
    /**
     * プライマリーキーでロールを検索する。
     * @param roleId ロールID
     * @return 結果
     */
    Role selectByPrimaryKey(Integer roleId);

    /**
     * プライマリーキーでロール検索する。（連携情報含む）
     * @param roleId ロールID
     * @return 結果
     */
    List<Role> selectAllByPrimaryKey(Integer roleId);

    /**
     * 条件でロールを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Role> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件でロールを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Role> selectByExample(@Param("example") WhereCondition example);

    /**
     * ロールを新規追加する。
     * @param record ロール
     * @return 結果
     */
    int insertSelective(Role record);

    /**
     * ロールを新規追加する。
     * @param record ロール
     * @return 結果
     */
    int insert(Role record);

    /**
     * プライマリーキーでロールを更新する。
     * @param record ロール
     * @return 結果
     */
    int updateByPrimaryKey(Role record);

    /**
     * プライマリーキーでロールを更新する。
     * @param record ロール
     * @return 結果
     */
    int updateByPrimaryKeySelective(Role record);

    /**
     * ロールを更新する。
     * @param record ロール
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Role record, @Param("example") WhereCondition example);

    /**
     * ロールを削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Role example);

    /**
     * ロールを削除する。
     * @param roleId ロールID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer roleId);
}
