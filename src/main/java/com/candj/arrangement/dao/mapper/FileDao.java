package com.candj.arrangement.dao.mapper;

import com.candj.arrangement.mapper.model.File;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * ファイル情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface FileDao {
    /**
     * プライマリーキーでファイルを検索する。
     * @param fileId ファイルID
     * @return 結果
     */
    File selectByPrimaryKey(Integer fileId);

    /**
     * プライマリーキーでファイル検索する。（連携情報含む）
     * @param fileId ファイルID
     * @return 結果
     */
    List<File> selectAllByPrimaryKey(Integer fileId);

    /**
     * 条件でファイルを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<File> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件でファイルを検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<File> selectByExample(@Param("example") WhereCondition example);

    /**
     * ファイルを新規追加する。
     * @param record ファイル
     * @return 結果
     */
    int insertSelective(File record);

    /**
     * ファイルを新規追加する。
     * @param record ファイル
     * @return 結果
     */
    int insert(File record);

    /**
     * プライマリーキーでファイルを更新する。
     * @param record ファイル
     * @return 結果
     */
    int updateByPrimaryKey(File record);

    /**
     * プライマリーキーでファイルを更新する。
     * @param record ファイル
     * @return 結果
     */
    int updateByPrimaryKeySelective(File record);

    /**
     * ファイルを更新する。
     * @param record ファイル
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(File record, @Param("example") WhereCondition example);

    /**
     * ファイルを削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") File example);

    /**
     * ファイルを削除する。
     * @param fileId ファイルID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer fileId);
}
