package com.candj.arrangement.dao.mapper;

import com.candj.arrangement.mapper.model.Visa;
import com.candj.webpower.web.core.model.WhereCondition;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 签证種類情報追加、更新、削除、検索処理を行うDAOクラス。
 *
 * @author C&J株式会社
 *
 */
public interface VisaDao {
    /**
     * プライマリーキーで签证種類を検索する。
     * @param visaId 签证ID
     * @return 結果
     */
    Visa selectByPrimaryKey(Integer visaId);

    /**
     * プライマリーキーで签证種類検索する。（連携情報含む）
     * @param visaId 签证ID
     * @return 結果
     */
    List<Visa> selectAllByPrimaryKey(Integer visaId);

    /**
     * 条件で签证種類を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Visa> selectAllByExample(@Param("example") WhereCondition example);

    /**
     * 条件で签证種類を検索する。（連携情報含む）)
     * @param example 検索条件
     * @return 結果
     */
    List<Visa> selectByExample(@Param("example") WhereCondition example);

    /**
     * 签证種類を新規追加する。
     * @param record 签证種類
     * @return 結果
     */
    int insertSelective(Visa record);

    /**
     * 签证種類を新規追加する。
     * @param record 签证種類
     * @return 結果
     */
    int insert(Visa record);

    /**
     * プライマリーキーで签证種類を更新する。
     * @param record 签证種類
     * @return 結果
     */
    int updateByPrimaryKey(Visa record);

    /**
     * プライマリーキーで签证種類を更新する。
     * @param record 签证種類
     * @return 結果
     */
    int updateByPrimaryKeySelective(Visa record);

    /**
     * 签证種類を更新する。
     * @param record 签证種類
     * @param example 更新条件
     * @return 結果
     */
    int updateByExampleSelective(Visa record, @Param("example") WhereCondition example);

    /**
     * 签证種類を削除する。
     * @param example 削除条件
     * @return 結果
     */
    int deleteByExample(@Param("example") Visa example);

    /**
     * 签证種類を削除する。
     * @param visaId 签证ID
     * @return 結果
     */
    int deleteByPrimaryKey(Integer visaId);

    int selectCountByExample(@Param("example") WhereCondition whereCondition);
}
