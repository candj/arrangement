package com.candj.arrangement.prerequisites;


import com.candj.arrangement.dto.ShiftworkDto;
import com.candj.arrangement.mapper.model.Shiftwork;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CheckPrerequisites {
    public boolean labourHourCheck(List<Shiftwork> shiftworkDto){
        return true;
    }

    public boolean visaExpireCheck(List<Shiftwork> shiftworkDto){

        return true;
    }

}
