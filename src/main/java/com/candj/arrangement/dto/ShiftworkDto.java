package com.candj.arrangement.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * シフト
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class ShiftworkDto {
    /** シフトID */
    @Digits(integer=10, fraction=0)
    private Integer shiftworkId;

    /** スタッフ_シフト */
    private java.util.List<StaffShiftDto> staffShift;

    /** 名前 */
    @Length(max=128)
    private String shiftworkName;

    /** 開始時間 */
    private org.joda.time.DateTime startTime;

    /** 終了時間 */
    private org.joda.time.DateTime endTime;

    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer companyId;

    /** シフト */
    private com.candj.arrangement.mapper.model.Company company;

    /** タスクID */
    @Digits(integer=10, fraction=0)
    private Integer taskId;

    /** シフト */
    private com.candj.arrangement.mapper.model.Task task;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    private int enrolled;
    private int required;
    private int rejected;
    private int unread;
    private int setNull;
}
