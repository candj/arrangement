package com.candj.arrangement.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 签证種類
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class VisaDto {
    /** 签证ID */
    @Digits(integer=10, fraction=0)
    private Integer visaId;

    /** スタッフ_签证種類 */
    private java.util.List<StaffVisaDto> staffVisa;

    /** 签证名前 */
    @Length(max=128)
    private String visaName;

    /** シフトID */
    @Digits(integer=10, fraction=0)
    private Integer permittedWorkHourPerWeek;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
