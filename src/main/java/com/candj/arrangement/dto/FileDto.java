package com.candj.arrangement.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * ファイル
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class FileDto {
    /** ファイルID */
    @Digits(integer=10, fraction=0)
    private Integer fileId;

    /** ストレージ パス */
    @Length(max=128)
    private String storePath;

    /** カテゴリ:1:資格外証明書  2:40時間证明  */
    @Digits(integer=10, fraction=0)
    private Integer category;

    /** 状態 0:未読 1:未承認　2:承認 */
    @Digits(integer=10, fraction=0)
    private Integer state;

    /** スタッフID */
    @Digits(integer=10, fraction=0)
    private Integer staffId;

    /** ファイル */
    private com.candj.arrangement.mapper.model.Staff staff;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
