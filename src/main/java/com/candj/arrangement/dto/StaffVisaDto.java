package com.candj.arrangement.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;

/**
 * スタッフ_签证種類
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class StaffVisaDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer id;

    /** スタッフID */
    @Digits(integer=10, fraction=0)
    private Integer staffId;

    /** スタッフ_签证種類 */
    private com.candj.arrangement.mapper.model.Staff staff;

    /** 签证ID */
    @Digits(integer=10, fraction=0)
    private Integer visaId;

    /** スタッフ_签证種類 */
    private com.candj.arrangement.mapper.model.Visa visa;

    /** 终了日 */
    private org.joda.time.DateTime endTime;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
