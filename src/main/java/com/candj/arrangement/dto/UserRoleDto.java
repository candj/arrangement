package com.candj.arrangement.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;

/**
 * ユーザールール
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class UserRoleDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer id;

    /** ユーザーID */
    @Digits(integer=10, fraction=0)
    private Integer userId;

    /** ユーザールール */
    private com.candj.arrangement.mapper.model.AppUser appUser;

    /** ロールID */
    @Digits(integer=10, fraction=0)
    private Integer roleId;

    /** ユーザールール */
    private com.candj.arrangement.mapper.model.Role role;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
