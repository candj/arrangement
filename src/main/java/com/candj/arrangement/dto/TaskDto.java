package com.candj.arrangement.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * タスク
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class TaskDto {
    /** タスクID */
    @Digits(integer=10, fraction=0)
    private Integer taskId;

    /** シフト */
    private java.util.List<ShiftworkDto> shiftwork;

    /** 名前 */
    @Length(max=128)
    private String taskName;

    /** 開始日 */
    private org.joda.time.DateTime startTime;

    /** 终了日 */
    private org.joda.time.DateTime endTime;

    /** 予想人数 */
    @Digits(integer=10, fraction=0)
    private Integer expectWorkforce;

    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer companyId;

    /** タスク */
    private com.candj.arrangement.mapper.model.Company company;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
