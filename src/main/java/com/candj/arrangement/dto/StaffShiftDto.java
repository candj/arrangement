package com.candj.arrangement.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;

/**
 * スタッフ_シフト
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class StaffShiftDto {
    /** ID */
    @Digits(integer=10, fraction=0)
    private Integer id;

    /** シフトID */
    @Digits(integer=10, fraction=0)
    private Integer shiftworkId;

    /** スタッフ_シフト */
    private com.candj.arrangement.mapper.model.Shiftwork shiftwork;

    /** スタッフID */
    @Digits(integer=10, fraction=0)
    private Integer staffId;

    /** スタッフ_シフト */
    private com.candj.arrangement.mapper.model.Staff staff;

    /** 状態 0: 已申请 1:拒否 2:同意 */
    @Digits(integer=10, fraction=0)
    private Integer state;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    private String visaName;
    private org.joda.time.DateTime visaEndTime;
    private int stateForScrollBar;
    private String staffCode;
}
