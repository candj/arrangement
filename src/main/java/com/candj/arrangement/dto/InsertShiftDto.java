package com.candj.arrangement.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InsertShiftDto {
    private int shiftworkId;
    private int oriId;
    private int staffId;
    private int state;
}
