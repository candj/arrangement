package com.candj.arrangement.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 管理者
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class AppUserDto {
    /** 管理者ID */
    @Digits(integer=10, fraction=0)
    private Integer userId;

    /** ユーザールール */
    private java.util.List<UserRoleDto> userRole;

    /** ロールコード */
    @Length(max=16)
    private String userCode;

    /** 管理者名 */
    @Length(max=128)
    private String userName;

    /** パスワード */
    @Length(max=255)
    private String password;

    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer companyId;

    /** 管理者 */
    private com.candj.arrangement.mapper.model.Company company;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
