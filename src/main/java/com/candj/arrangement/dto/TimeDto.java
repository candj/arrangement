package com.candj.arrangement.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;

/**
 * 時間
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class TimeDto {
    /** シフトID */
    @Digits(integer=10, fraction=0)
    private Integer timeId;

    /** 開始時間 */
    private org.joda.time.DateTime startTime;

    /** 终了時間 */
    private org.joda.time.DateTime endTime;

    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer companyId;

    /** 時間 */
    private com.candj.arrangement.mapper.model.Company company;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
