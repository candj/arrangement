package com.candj.arrangement.dto;

import javax.validation.constraints.Digits;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * 会社
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class CompanyDto {
    /** 会社ID */
    @Digits(integer=10, fraction=0)
    private Integer companyId;

    /** 管理者 */
    private java.util.List<AppUserDto> appUser;

    /** 会社名 */
    @Length(max=128)
    private String companyName;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer createUser;

    /** 更新ユーザー */
    @Digits(integer=10, fraction=0)
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
