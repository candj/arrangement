package com.candj.arrangement.identifyCurrentUser;

import com.candj.arrangement.core.security.AppAuthenticationUser;
import com.candj.arrangement.dao.mapper.AppUserDao;
import com.candj.arrangement.dto.AppUserDto;
import com.candj.arrangement.mapper.model.AppUser;
import com.candj.webpower.web.core.model.WhereCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class CompanyIdentify {
    @Autowired
    AppUserDao appUserDao;
    public int identifyCompany(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppAuthenticationUser a = (AppAuthenticationUser) auth.getPrincipal();

        WhereCondition findUser = new WhereCondition();
        String userName = auth.getName();
        findUser.createCriteria()
                .andEqualTo("app_user.user_name", userName);
        List<AppUser> user = appUserDao.selectByExample(findUser);
        return user.get(0).getCompanyId();
    }
}
