package com.candj.arrangement.webservice;

import com.candj.arrangement.dto.TimeDto;
import com.candj.arrangement.identifyCurrentUser.CompanyIdentify;
import com.candj.arrangement.service.TimeService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * 時間情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/timeRest")
@RestController
public class TimeRestController {
    @Autowired
    TimeService timeService;

    @Autowired
    CompanyIdentify companyIdentify;

    /**
     * 時間を新規追加する。
     * @param timeId シフトID
     * @return 結果
     */
    @RequestMapping(value = "{timeId}", method = RequestMethod.GET)
    public TimeDto get(@PathVariable Integer timeId) {
        //プライマリーキーで時間を検索する。
        TimeDto ret = timeService.selectByPrimaryKey(timeId);
        return ret;
    }

    /**
     * 時間を新規追加する。
     * @param timeDto 時間
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody TimeDto timeDto, Errors rrrors) {
        //時間を新規追加する。
        int ret = timeService.insertSelective(timeDto);
        return ret;
    }

    /**
     * 時間を変更する。
     * @param timeDto 時間
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody TimeDto timeDto, Errors rrrors) {
        //プライマリーキーで時間を更新する。
        int ret = timeService.updateByPrimaryKeySelective(timeDto);
        return ret;
    }

    /**
     * 時間を新規追加する。
     * @param timeId シフトID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{timeId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer timeId) {
        //時間を削除する。
        int ret = timeService.deleteByPrimaryKey(timeId);
        return ret;
    }

    /**
     * 時間一覧画面を表示する。
     * @param timeDto 時間
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<TimeDto> list(TimeDto timeDto, JqgridPageReq pageReq) {
        int currentCompanyId = companyIdentify.identifyCompany();
        Page<TimeDto> page = PageUtil.startPage(pageReq);
        //条件で時間を検索する。（連携情報含む）
        List<TimeDto> ret = timeService.selectByExample(timeDto, currentCompanyId);
        return PageUtil.resp(page);
    }

    /**
     * 時間一覧画面を表示する。
     * @param timeDto 時間
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<TimeDto> listAll(TimeDto timeDto, JqgridPageReq pageReq) {
        Page<TimeDto> page = PageUtil.startPage(pageReq);
        //条件で時間を検索する。（連携情報含む）
        List<TimeDto> ret = timeService.selectAllByExample(timeDto);
        return PageUtil.resp(page);
    }
}
