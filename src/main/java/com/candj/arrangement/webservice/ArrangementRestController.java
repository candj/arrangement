package com.candj.arrangement.webservice;

import com.candj.arrangement.dao.mapper.ShiftworkDao;
import com.candj.arrangement.dao.mapper.StaffDao;
import com.candj.arrangement.dao.mapper.StaffShiftDao;
import com.candj.arrangement.dto.*;
import com.candj.arrangement.mapper.model.Shiftwork;
import com.candj.arrangement.mapper.model.Staff;
import com.candj.arrangement.mapper.model.StaffShift;
import com.candj.arrangement.mapper.model.StaffVisa;
import com.candj.arrangement.prerequisites.CheckPrerequisites;
import com.candj.arrangement.service.ShiftworkService;
import com.candj.arrangement.service.StaffShiftService;
import com.candj.arrangement.service.StaffVisaService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.model.WhereCondition;
import com.candj.webpower.web.core.util.LogUtil;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;

import com.mysql.cj.xdevapi.JsonArray;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

/**
 * スタッフ_シフト情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/ArrangementRest")
@RestController
public class ArrangementRestController {
    @Autowired
    StaffShiftService staffShiftService;

    @Autowired
    ShiftworkDao shiftworkDao;

    @Autowired
    CheckPrerequisites checkPrerequisites;

    @Autowired
    StaffDao staffDao;

    @Autowired
    StaffShiftDao staffShiftDao;

    @Autowired
    StaffVisaService staffVisaService;

    protected final Log logger = LogFactory.getLog(this.getClass());

    /**
     * スタッフ_シフトを新規追加する。
     * @param id ID
     * @return 結果
     */
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public StaffShiftDto get(@PathVariable Integer id) {
        //プライマリーキーでスタッフ_シフトを検索する。
        StaffShiftDto ret = staffShiftService.selectByPrimaryKey(id);
        return ret;
    }

    /**
     * スタッフ_シフトを新規追加する。
     * @param staffShiftDto スタッフ_シフト
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody StaffShiftDto staffShiftDto, Errors rrrors) {
        //スタッフ_シフトを新規追加する。
        int ret = staffShiftService.insertSelective(staffShiftDto);
        return ret;
    }

    /**
     * スタッフ_シフトを変更する。
     * @param staffShiftDto スタッフ_シフト
     * @param staffShiftDto バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody StaffShiftDto staffShiftDto, Errors rrrors) {
        //プライマリーキーでスタッフ_シフトを更新する。
        int ret = staffShiftService.updateByPrimaryKeySelective(staffShiftDto);
        return ret;
    }

    /**
     * スタッフ_シフトを新規追加する。
     * @param id ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer id) {
        //スタッフ_シフトを削除する。
        int ret = staffShiftService.deleteByPrimaryKey(id);
        return ret;
    }

    /**
     * スタッフ_シフト一覧画面を表示する。
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<StaffShiftDto> list(String shiftworkName, JqgridPageReq pageReq) {
        WhereCondition whereCondition = new WhereCondition();
        whereCondition.createCriteria().andEqualTo("shiftwork.shiftwork_name", shiftworkName);
        List<Shiftwork> ret1 = shiftworkDao.selectByExample(whereCondition);
        StaffShiftDto prepare = new StaffShiftDto();
        prepare.setShiftworkId(ret1.get(0).getShiftworkId());
        Page<StaffShiftDto> page = PageUtil.startPage(pageReq);
        //条件でスタッフ_シフトを検索する。（連携情報含む）
        List<StaffShiftDto> ret = staffShiftService.selectSuitables(ret1.get(0).getShiftworkId());
        return PageUtil.resp(page);
    }

    /**
     * スタッフ_シフト一覧画面を表示する。
     * @param staffShiftDto スタッフ_シフト
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<StaffShiftDto> listAll(StaffShiftDto staffShiftDto, JqgridPageReq pageReq) {
        Page<StaffShiftDto> page = PageUtil.startPage(pageReq);
        //条件でスタッフ_シフトを検索する。（連携情報含む）
        List<StaffShiftDto> ret = staffShiftService.selectAllByExample(staffShiftDto);
        return PageUtil.resp(page);
    }
    public void insertInto(StaffShift staffShift) {
        //TODO moved to services
        Staff staff = staffDao.selectByPrimaryKey(staffShift.getStaffId());
        Shiftwork shiftwork = shiftworkDao.selectByPrimaryKey(staffShift.getShiftworkId());
        WhereCondition whereCondition = new WhereCondition();
        whereCondition.createCriteria()
                .andEqualTo("staff_shift.staff_id", staff.getStaffId());
        List<Shiftwork>shiftworks = shiftworkDao.selectByExample(whereCondition);
        if(prerequisite(shiftworks)){
            StaffShift newRecord = new StaffShift();
            newRecord.setStaffId(staff.getStaffId());
            newRecord.setShiftworkId(shiftwork.getShiftworkId());
            newRecord.setState(2);
            newRecord.setDeleteFlag(false);
            staffShiftDao.insertSelective(staffShift);
        } else{
            LogUtil.error("限られた労働時間を超える違反のため、この労働者のシフトを手配することはできません");
        }
    }

    @RequestMapping(value = "/status/{roleID}", method = RequestMethod.POST)
    public void statusBack(@PathVariable String roleID) {
        int i = 0;
    }

    @RequestMapping(value="/table")
    public void updateAll(@RequestParam("data")String data, @RequestParam("shiftworkName") String shiftworkName)
    {
        //data:"[{attr1:val1,attr2:val2 ...},+]"
        WhereCondition whereCondition = new WhereCondition();
        whereCondition.createCriteria()
                .andEqualTo("shiftwork.shiftwork_name",shiftworkName);
        List<Shiftwork> shiftwork = shiftworkDao.selectByExample(whereCondition);
        int shiftworkId = shiftwork.get(0).getShiftworkId();
        List<InsertShiftDto>toInserts = new LinkedList<>();
        String a[] = data.split("[{}]");
        for(int i = 1; i < a.length; i+=2){
            String attr[] = a[i].split(",");
            int placeholder = 0;
            InsertShiftDto toInsert = new InsertShiftDto();
            toInsert.setShiftworkId(shiftworkId);
            String oriStaffShiftId = attr[0].split(":")[1].replace("\"","");
            int oriId = 0;
            if(!oriStaffShiftId.equals("")){
                oriId = Integer.parseInt(oriStaffShiftId);
            }
            String toProcessStaffName = attr[1].split(":")[1].replace("\"","");
            WhereCondition whereCondition2 = new WhereCondition();
            whereCondition2.createCriteria()
                    .andEqualTo("staff.staff_code",toProcessStaffName);
            int staffId = staffDao.selectByExample(whereCondition2).get(0).getStaffId();
            String toProcessState = attr[13].split(":")[1].replace("\"","");
            int state = -1;
            if(!toProcessState.equals("")){
                state = Integer.parseInt(toProcessState);}
            toInsert.setOriId(oriId);
            toInsert.setStaffId(staffId);
            toInsert.setState(state);
            toInserts.add(toInsert);

        }
        for (InsertShiftDto x:toInserts) {
            StaffShift staffShift = new StaffShift();
            if(x.getState()> 1){
                if(x.getOriId() == 0){
                    staffShift.setShiftworkId(x.getShiftworkId());
                    staffShift.setStaffId(x.getStaffId());
                    staffShift.setState(x.getState());
                    staffShift.setDeleteFlag(false);
                    staffShiftDao.insertSelective(staffShift);
                } else{
                    staffShift.setId(x.getOriId());
                    staffShift.setShiftworkId(x.getShiftworkId());
                    staffShift.setStaffId(x.getStaffId());
                    staffShift.setState(x.getState());
                    staffShift.setDeleteFlag(false);
                    staffShiftDao.updateByPrimaryKeySelective(staffShift);
                }
            }
        }
        int placeholder = 0;
    }




    private boolean prerequisite(List<Shiftwork> shiftwork){
        //TODO
        // return checkPrerequisites.labourHourCheck(shiftwork)&&checkPrerequisites.visaExpireCheck(shiftwork);
        return true;
    }


}
