package com.candj.arrangement.webservice;

import com.candj.arrangement.dto.TaskDto;
import com.candj.arrangement.identifyCurrentUser.CompanyIdentify;
import com.candj.arrangement.service.TaskService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

/**
 * タスク情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/taskRest")
@RestController
public class TaskRestController {
    @Autowired
    TaskService taskService;

    @Autowired
    CompanyIdentify companyIdentify;

    /**
     * タスクを新規追加する。
     * @param taskId タスクID
     * @return 結果
     */
    @RequestMapping(value = "{taskId}", method = RequestMethod.GET)
    public TaskDto get(@PathVariable Integer taskId) {
        //プライマリーキーでタスクを検索する。
        TaskDto ret = taskService.selectByPrimaryKey(taskId);
        return ret;
    }

    /**
     * タスクを新規追加する。
     * @param taskDto タスク
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody TaskDto taskDto, Errors rrrors) {
        //タスクを新規追加する。
        int ret = taskService.insertSelective(taskDto);
        return ret;
    }

    /**
     * タスクを変更する。
     * @param taskDto タスク
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody TaskDto taskDto, Errors rrrors) {
        //プライマリーキーでタスクを更新する。
        int ret = taskService.updateByPrimaryKeySelective(taskDto);
        return ret;
    }

    /**
     * タスクを新規追加する。
     * @param taskId タスクID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{taskId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer taskId) {
        //タスクを削除する。
        int ret = taskService.deleteByPrimaryKey(taskId);
        return ret;
    }

    /**
     * タスク一覧画面を表示する。
     * @param taskDto タスク
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<TaskDto> list(TaskDto taskDto, @RequestParam(name="timeSearch", required=false) DateTime timeSearch, JqgridPageReq pageReq) {
        int currentCompanyId = companyIdentify.identifyCompany();
        Page<TaskDto> page = PageUtil.startPage(pageReq);
        //条件でタスクを検索する。（連携情報含む）
        List<TaskDto> ret = taskService.selectByExample(taskDto, currentCompanyId, timeSearch);
        return PageUtil.resp(page);
    }

    /**
     * タスク一覧画面を表示する。
     * @param taskDto タスク
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<TaskDto> listAll(TaskDto taskDto, JqgridPageReq pageReq) {
        Page<TaskDto> page = PageUtil.startPage(pageReq);
        //条件でタスクを検索する。（連携情報含む）
        List<TaskDto> ret = taskService.selectAllByExample(taskDto);
        return PageUtil.resp(page);
    }

    @RequestMapping(value = "/taskNameUnique", method = RequestMethod.GET)
    public Object taskNameUnique(String taskName) {
        TaskDto taskDto = new TaskDto();
        taskDto.setTaskName(taskName);
        if( taskService.taskExists(taskDto)) {
            return "taskNameExists";
        }
        return true;
    }
}
