package com.candj.arrangement.webservice;

import com.candj.arrangement.dto.StaffShiftDto;
import com.candj.arrangement.service.StaffShiftService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * スタッフ_シフト情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/staffShiftRest")
@RestController
public class StaffShiftRestController {
    @Autowired
    StaffShiftService staffShiftService;

    /**
     * スタッフ_シフトを新規追加する。
     * @param id ID
     * @return 結果
     */
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public StaffShiftDto get(@PathVariable Integer id) {
        //プライマリーキーでスタッフ_シフトを検索する。
        StaffShiftDto ret = staffShiftService.selectByPrimaryKey(id);
        return ret;
    }

    /**
     * スタッフ_シフトを新規追加する。
     * @param staffShiftDto スタッフ_シフト
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody StaffShiftDto staffShiftDto, Errors rrrors) {
        //スタッフ_シフトを新規追加する。
        int ret = staffShiftService.insertSelective(staffShiftDto);
        return ret;
    }

    /**
     * スタッフ_シフトを変更する。
     * @param staffShiftDto スタッフ_シフト
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody StaffShiftDto staffShiftDto, Errors rrrors) {
        //プライマリーキーでスタッフ_シフトを更新する。
        int ret = staffShiftService.updateByPrimaryKeySelective(staffShiftDto);
        return ret;
    }

    /**
     * スタッフ_シフトを新規追加する。
     * @param id ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer id) {
        //スタッフ_シフトを削除する。
        int ret = staffShiftService.deleteByPrimaryKey(id);
        return ret;
    }

    /**
     * スタッフ_シフト一覧画面を表示する。
     * @param staffShiftDto スタッフ_シフト
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<StaffShiftDto> list(StaffShiftDto staffShiftDto, JqgridPageReq pageReq) {
        Page<StaffShiftDto> page = PageUtil.startPage(pageReq);
        //条件でスタッフ_シフトを検索する。（連携情報含む）
        List<StaffShiftDto> ret = staffShiftService.selectByExample(staffShiftDto);
        return PageUtil.resp(page);
    }

    /**
     * スタッフ_シフト一覧画面を表示する。
     * @param staffShiftDto スタッフ_シフト
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<StaffShiftDto> listAll(StaffShiftDto staffShiftDto, JqgridPageReq pageReq) {
        Page<StaffShiftDto> page = PageUtil.startPage(pageReq);
        //条件でスタッフ_シフトを検索する。（連携情報含む）
        List<StaffShiftDto> ret = staffShiftService.selectAllByExample(staffShiftDto);
        return PageUtil.resp(page);
    }
}
