package com.candj.arrangement.webservice;

import com.candj.arrangement.dto.TaskDto;
import com.candj.arrangement.dto.VisaDto;
import com.candj.arrangement.service.VisaService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * 签证種類情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/visaRest")
@RestController
public class VisaRestController {
    @Autowired
    VisaService visaService;

    /**
     * 签证種類を新規追加する。
     * @param visaId 签证ID
     * @return 結果
     */
    @RequestMapping(value = "{visaId}", method = RequestMethod.GET)
    public VisaDto get(@PathVariable Integer visaId) {
        //プライマリーキーで签证種類を検索する。
        VisaDto ret = visaService.selectByPrimaryKey(visaId);
        return ret;
    }

    /**
     * 签证種類を新規追加する。
     * @param visaDto 签证種類
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody VisaDto visaDto, Errors rrrors) {
        //签证種類を新規追加する。
        int ret = visaService.insertSelective(visaDto);
        return ret;
    }

    /**
     * 签证種類を変更する。
     * @param visaDto 签证種類
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody VisaDto visaDto, Errors rrrors) {
        //プライマリーキーで签证種類を更新する。
        int ret = visaService.updateByPrimaryKeySelective(visaDto);
        return ret;
    }

    /**
     * 签证種類を新規追加する。
     * @param visaId 签证ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{visaId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer visaId) {
        //签证種類を削除する。
        int ret = visaService.deleteByPrimaryKey(visaId);
        return ret;
    }

    /**
     * 签证種類一覧画面を表示する。
     * @param visaDto 签证種類
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<VisaDto> list(VisaDto visaDto, JqgridPageReq pageReq) {
        Page<VisaDto> page = PageUtil.startPage(pageReq);
        //条件で签证種類を検索する。（連携情報含む）
        List<VisaDto> ret = visaService.selectByExample(visaDto);
        return PageUtil.resp(page);
    }

    /**
     * 签证種類一覧画面を表示する。
     * @param visaDto 签证種類
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<VisaDto> listAll(VisaDto visaDto, JqgridPageReq pageReq) {
        Page<VisaDto> page = PageUtil.startPage(pageReq);
        //条件で签证種類を検索する。（連携情報含む）
        List<VisaDto> ret = visaService.selectAllByExample(visaDto);
        return PageUtil.resp(page);
    }

    @RequestMapping(value = "/visaNameUnique", method = RequestMethod.GET)
    public Object visaNameUnique(String visaName) {
        VisaDto visaDto = new VisaDto();
        visaDto.setVisaName(visaName);
        if( visaService.visaExists(visaDto)) {
            return "visaNameExists";
        }
        return true;
    }
}
