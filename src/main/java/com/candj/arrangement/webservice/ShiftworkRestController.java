package com.candj.arrangement.webservice;

import com.candj.arrangement.dto.ShiftworkDto;
import com.candj.arrangement.service.ShiftworkService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

/**
 * シフト情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/shiftworkRest")
@RestController
public class ShiftworkRestController {
    @Autowired
    ShiftworkService shiftworkService;

    /**
     * シフトを新規追加する。
     * @param shiftworkId シフトID
     * @return 結果
     */
    @RequestMapping(value = "{shiftworkId}", method = RequestMethod.GET)
    public ShiftworkDto get(@PathVariable Integer shiftworkId) {
        //プライマリーキーでシフトを検索する。
        ShiftworkDto ret = shiftworkService.selectByPrimaryKey(shiftworkId);
        return ret;
    }

    /**
     * シフトを新規追加する。
     * @param shiftworkDto シフト
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody ShiftworkDto shiftworkDto, Errors rrrors) {
        //シフトを新規追加する。
        int ret = shiftworkService.insertSelective(shiftworkDto);
        return ret;
    }

    /**
     * シフトを変更する。
     * @param shiftworkDto シフト
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody ShiftworkDto shiftworkDto, Errors rrrors) {
        //プライマリーキーでシフトを更新する。
        int ret = shiftworkService.updateByPrimaryKeySelective(shiftworkDto);
        return ret;
    }

    /**
     * シフトを新規追加する。
     * @param shiftworkId シフトID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{shiftworkId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer shiftworkId) {
        //シフトを削除する。
        int ret = shiftworkService.deleteByPrimaryKey(shiftworkId);
        return ret;
    }

    /**
     * シフト一覧画面を表示する。
     * @param shiftworkDto シフト
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<ShiftworkDto> list(ShiftworkDto shiftworkDto, @RequestParam(name="startTime", required = false) DateTime startTime, JqgridPageReq pageReq) {
        Page<ShiftworkDto> page = PageUtil.startPage(pageReq);
        //条件でシフトを検索する。（連携情報含む）
        List<ShiftworkDto> ret = shiftworkService.selectByExample(shiftworkDto, startTime);
        return PageUtil.resp(page);
    }

    /**
     * シフト一覧画面を表示する。
     * @param shiftworkDto シフト
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<ShiftworkDto> listAll(ShiftworkDto shiftworkDto, JqgridPageReq pageReq) {
        Page<ShiftworkDto> page = PageUtil.startPage(pageReq);
        //条件でシフトを検索する。（連携情報含む）
        List<ShiftworkDto> ret = shiftworkService.selectAllByExample(shiftworkDto);
        return PageUtil.resp(page);
    }

    /**
     * スタッフコード唯一の検査
     *
     * @param shiftWorkDto
     *            スタッフ
     * @param pageReq
     *            改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/shiftworkNameUnique", method = RequestMethod.GET)
    public Object shiftworkNameUnique(String shiftworkName) {

        ShiftworkDto shiftworkDto = new ShiftworkDto();
        shiftworkDto.setShiftworkName(shiftworkName);
        if( shiftworkService.shiftworkExists(shiftworkDto)) {
            return "shiftworkNameExists";
        }
        return true;
    }
}
