package com.candj.arrangement.webservice;

import com.candj.arrangement.dto.RoleDto;
import com.candj.arrangement.service.RoleService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * ロール情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/roleRest")
@RestController
public class RoleRestController {
    @Autowired
    RoleService roleService;

    /**
     * ロールを新規追加する。
     * @param roleId ロールID
     * @return 結果
     */
    @RequestMapping(value = "{roleId}", method = RequestMethod.GET)
    public RoleDto get(@PathVariable Integer roleId) {
        //プライマリーキーでロールを検索する。
        RoleDto ret = roleService.selectByPrimaryKey(roleId);
        return ret;
    }

    /**
     * ロールを新規追加する。
     * @param roleDto ロール
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody RoleDto roleDto, Errors rrrors) {
        //ロールを新規追加する。
        int ret = roleService.insertSelective(roleDto);
        return ret;
    }

    /**
     * ロールを変更する。
     * @param roleDto ロール
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody RoleDto roleDto, Errors rrrors) {
        //プライマリーキーでロールを更新する。
        int ret = roleService.updateByPrimaryKeySelective(roleDto);
        return ret;
    }

    /**
     * ロールを新規追加する。
     * @param roleId ロールID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{roleId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer roleId) {
        //ロールを削除する。
        int ret = roleService.deleteByPrimaryKey(roleId);
        return ret;
    }

    /**
     * ロール一覧画面を表示する。
     * @param roleDto ロール
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<RoleDto> list(RoleDto roleDto, JqgridPageReq pageReq) {
        Page<RoleDto> page = PageUtil.startPage(pageReq);
        //条件でロールを検索する。（連携情報含む）
        List<RoleDto> ret = roleService.selectByExample(roleDto);
        return PageUtil.resp(page);
    }

    /**
     * ロール一覧画面を表示する。
     * @param roleDto ロール
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<RoleDto> listAll(RoleDto roleDto, JqgridPageReq pageReq) {
        Page<RoleDto> page = PageUtil.startPage(pageReq);
        //条件でロールを検索する。（連携情報含む）
        List<RoleDto> ret = roleService.selectAllByExample(roleDto);
        return PageUtil.resp(page);
    }
}
