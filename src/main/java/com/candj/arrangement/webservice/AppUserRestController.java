package com.candj.arrangement.webservice;

import com.candj.arrangement.dto.AppUserDto;
import com.candj.arrangement.service.AppUserService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * 管理者情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/appUserRest")
@RestController
public class AppUserRestController {
    @Autowired
    AppUserService appUserService;

    /**
     * 管理者を新規追加する。
     * @param userId 管理者ID
     * @return 結果
     */
    @RequestMapping(value = "{userId}", method = RequestMethod.GET)
    public AppUserDto get(@PathVariable Integer userId) {
        //プライマリーキーで管理者を検索する。
        AppUserDto ret = appUserService.selectByPrimaryKey(userId);
        return ret;
    }

    /**
     * 管理者を新規追加する。
     * @param appUserDto 管理者
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody AppUserDto appUserDto, Errors rrrors) {
        //管理者を新規追加する。
        int ret = appUserService.insertSelective(appUserDto);
        return ret;
    }

    /**
     * 管理者を変更する。
     * @param appUserDto 管理者
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody AppUserDto appUserDto, Errors rrrors) {
        //プライマリーキーで管理者を更新する。
        int ret = appUserService.updateByPrimaryKeySelective(appUserDto);
        return ret;
    }

    /**
     * 管理者を新規追加する。
     * @param userId 管理者ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{userId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer userId) {
        //管理者を削除する。
        int ret = appUserService.deleteByPrimaryKey(userId);
        return ret;
    }

    /**
     * 管理者一覧画面を表示する。
     * @param appUserDto 管理者
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<AppUserDto> list(AppUserDto appUserDto, JqgridPageReq pageReq) {
        Page<AppUserDto> page = PageUtil.startPage(pageReq);
        //条件で管理者を検索する。（連携情報含む）
        List<AppUserDto> ret = appUserService.selectByExample(appUserDto);
        return PageUtil.resp(page);
    }

    /**
     * 管理者一覧画面を表示する。
     * @param appUserDto 管理者
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<AppUserDto> listAll(AppUserDto appUserDto, JqgridPageReq pageReq) {
        Page<AppUserDto> page = PageUtil.startPage(pageReq);
        //条件で管理者を検索する。（連携情報含む）
        List<AppUserDto> ret = appUserService.selectAllByExample(appUserDto);
        return PageUtil.resp(page);
    }
}
