package com.candj.arrangement.webservice;

import com.candj.arrangement.dto.StaffVisaDto;
import com.candj.arrangement.identifyCurrentUser.CompanyIdentify;
import com.candj.arrangement.service.StaffVisaService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * スタッフ_签证種類情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/staffVisaRest")
@RestController
public class StaffVisaRestController {
    @Autowired
    StaffVisaService staffVisaService;

    @Autowired
    CompanyIdentify companyIdentify;

    /**
     * スタッフ_签证種類を新規追加する。
     * @param id ID
     * @return 結果
     */
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public StaffVisaDto get(@PathVariable Integer id) {
        //プライマリーキーでスタッフ_签证種類を検索する。
        StaffVisaDto ret = staffVisaService.selectByPrimaryKey(id);
        return ret;
    }

    /**
     * スタッフ_签证種類を新規追加する。
     * @param staffVisaDto スタッフ_签证種類
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody StaffVisaDto staffVisaDto, Errors rrrors) {
        //スタッフ_签证種類を新規追加する。
        int ret = staffVisaService.insertSelective(staffVisaDto);
        return ret;
    }

    /**
     * スタッフ_签证種類を変更する。
     * @param staffVisaDto スタッフ_签证種類
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody StaffVisaDto staffVisaDto, Errors rrrors) {
        //プライマリーキーでスタッフ_签证種類を更新する。
        int ret = staffVisaService.updateByPrimaryKeySelective(staffVisaDto);
        return ret;
    }

    /**
     * スタッフ_签证種類を新規追加する。
     * @param id ID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer id) {
        //スタッフ_签证種類を削除する。
        int ret = staffVisaService.deleteByPrimaryKey(id);
        return ret;
    }

    /**
     * スタッフ_签证種類一覧画面を表示する。
     * @param staffVisaDto スタッフ_签证種類
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<StaffVisaDto> list(StaffVisaDto staffVisaDto, JqgridPageReq pageReq) {
        int currentCompanyId = companyIdentify.identifyCompany();
        Page<StaffVisaDto> page = PageUtil.startPage(pageReq);
        //条件でスタッフ_签证種類を検索する。（連携情報含む）
        List<StaffVisaDto> ret = staffVisaService.selectByExample(staffVisaDto, currentCompanyId);
        return PageUtil.resp(page);
    }

    /**
     * スタッフ_签证種類一覧画面を表示する。
     * @param staffVisaDto スタッフ_签证種類
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<StaffVisaDto> listAll(StaffVisaDto staffVisaDto, JqgridPageReq pageReq) {
        Page<StaffVisaDto> page = PageUtil.startPage(pageReq);
        //条件でスタッフ_签证種類を検索する。（連携情報含む）
        List<StaffVisaDto> ret = staffVisaService.selectAllByExample(staffVisaDto);
        return PageUtil.resp(page);
    }
}
