package com.candj.arrangement.webservice;

import com.candj.arrangement.dto.FileDto;
import com.candj.arrangement.service.FileService;
import com.candj.webpower.web.core.model.JqgridPageReq;
import com.candj.webpower.web.core.util.PageUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * ファイル情報検索処理を行うAPIコントローラクラス。
 *
 * @author C&J株式会社
 *
 */
@RequestMapping(value = "/api/fileRest")
@RestController
public class FileRestController {
    @Autowired
    FileService fileService;

    /**
     * ファイルを新規追加する。
     * @param fileId ファイルID
     * @return 結果
     */
    @RequestMapping(value = "{fileId}", method = RequestMethod.GET)
    public FileDto get(@PathVariable Integer fileId) {
        //プライマリーキーでファイルを検索する。
        FileDto ret = fileService.selectByPrimaryKey(fileId);
        return ret;
    }

    /**
     * ファイルを新規追加する。
     * @param fileDto ファイル
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public int insert(@Valid @RequestBody FileDto fileDto, Errors rrrors) {
        //ファイルを新規追加する。
        int ret = fileService.insertSelective(fileDto);
        return ret;
    }

    /**
     * ファイルを変更する。
     * @param fileDto ファイル
     * @param bindingResult バンディング結果
     * @return 結果
     */
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.PUT)
    public int update(@Valid @RequestBody FileDto fileDto, Errors rrrors) {
        //プライマリーキーでファイルを更新する。
        int ret = fileService.updateByPrimaryKeySelective(fileDto);
        return ret;
    }

    /**
     * ファイルを新規追加する。
     * @param fileId ファイルID
     * @return 結果
     */
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "{fileId}", method = RequestMethod.DELETE)
    public int delete(@PathVariable Integer fileId) {
        //ファイルを削除する。
        int ret = fileService.deleteByPrimaryKey(fileId);
        return ret;
    }

    /**
     * ファイル一覧画面を表示する。
     * @param fileDto ファイル
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public PageInfo<FileDto> list(FileDto fileDto, JqgridPageReq pageReq) {
        Page<FileDto> page = PageUtil.startPage(pageReq);
        //条件でファイルを検索する。（連携情報含む）
        List<FileDto> ret = fileService.selectByExample(fileDto);
        return PageUtil.resp(page);
    }

    /**
     * ファイル一覧画面を表示する。
     * @param fileDto ファイル
     * @param pageReq 改ページ情報
     * @return 結果
     */
    @RequestMapping(value = "/listAll", method = RequestMethod.POST)
    public PageInfo<FileDto> listAll(FileDto fileDto, JqgridPageReq pageReq) {
        Page<FileDto> page = PageUtil.startPage(pageReq);
        //条件でファイルを検索する。（連携情報含む）
        List<FileDto> ret = fileService.selectAllByExample(fileDto);
        return PageUtil.resp(page);
    }
}
