package com.candj.arrangement.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 時間
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Time {
    /** シフトID */
    private Integer timeId;

    /** 開始時間 */
    private org.joda.time.DateTime startTime;

    /** 终了時間 */
    private org.joda.time.DateTime endTime;

    /** 会社 */
    private Company company;

    /** 会社ID */
    private Integer companyId;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
