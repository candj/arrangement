package com.candj.arrangement.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * タスク
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Task {
    /** シフト */
    private java.util.List<Shiftwork> shiftwork;

    /** タスクID */
    private Integer taskId;

    /** 名前 */
    private String taskName;

    /** 開始日 */
    private org.joda.time.DateTime startTime;

    /** 终了日 */
    private org.joda.time.DateTime endTime;

    /** 予想人数 */
    private Integer expectWorkforce;

    /** 会社 */
    private Company company;

    /** 会社ID */
    private Integer companyId;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
