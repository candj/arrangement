package com.candj.arrangement.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * スタッフ_签证種類
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class StaffVisa {
    /** ID */
    private Integer id;

    /** スタッフ */
    private Staff staff;

    /** スタッフID */
    private Integer staffId;

    /** 签证種類 */
    private Visa visa;

    /** 签证ID */
    private Integer visaId;

    /** 终了日 */
    private org.joda.time.DateTime endTime;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
