package com.candj.arrangement.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 管理者
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class AppUser {
    /** ユーザールール */
    private java.util.List<UserRole> userRole;

    /** 管理者ID */
    private Integer userId;

    /** ロールコード */
    private String userCode;

    /** 管理者名 */
    private String userName;

    /** パスワード */
    private String password;

    /** 会社 */
    private Company company;

    /** 会社ID */
    private Integer companyId;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
