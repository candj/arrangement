package com.candj.arrangement.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 会社
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Company {
    /** 管理者 */
    private java.util.List<AppUser> appUser;

    /** 会社ID */
    private Integer companyId;

    /** 会社名 */
    private String companyName;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
