package com.candj.arrangement.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * スタッフ
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Staff {
    /** ファイル */
    private java.util.List<File> file;

    /** スタッフID */
    private Integer staffId;

    /** 社員番号 */
    private String staffCode;

    /** 氏名 */
    private String staffName;

    /** パスワード */
    private String staffPassword;

    /** 片仮名 */
    private String katakana;

    /** 出生日期 */
    private org.joda.time.DateTime birthDate;

    /** FALSE:短期採用ではない　TRUE:短期採用 */
    private Boolean shortTermFlag;

    /** 中国最終学歴 */
    private String diploma;

    /** 就学状況 */
    private String schoolStatus;

    /** 学校名 */
    private String schoolName;

    /** 卒業予定日 */
    private org.joda.time.DateTime expectedGradDate;

    /** 排班优先登记 */
    private Integer priority;

    /** 组长津贴 */
    private Integer allowance;

    /** 時給単価 */
    private Integer salary;

    /** 希望支払い方式1:现金 2:銀行 */
    private Integer payMethod;

    /** 銀行名 */
    private String bankName;

    /** 支店番号 */
    private String branchNo;

    /** 口座 */
    private String account;

    /** 雇用契約書 false:无資格 true: 有資格 */
    private Boolean contractFlag;

    /** 秘密保守契約 false:无資格 true: 有資格 */
    private Boolean ndaFlag;

    /** 年末調整 false:无資格 true: 有資格 */
    private Boolean yearEndAdjustmentFlag;

    /** 在留カード確認 false:无資格 true: 有資格 */
    private Boolean residentCardFlag;

    /** 在留カードコピー false:无資格 true: 有資格 */
    private Boolean residentCardCopyFlag;

    /** 40時間证明期限 */
    private org.joda.time.DateTime expireDateOf40Hour;

    /** 会社 */
    private Company company;

    /** 会社ID */
    private Integer companyId;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    private String visaName;
    private org.joda.time.DateTime visaEndTime;
    private int stateForScrollBar;
}
