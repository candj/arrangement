package com.candj.arrangement.mapper.model;

import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

/**
 * スタッフ_シフト
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class StaffShift {
    /** ID */
    private Integer id;

    /** シフト */
    private Shiftwork shiftwork;

    /** シフトID */
    private Integer shiftworkId;

    /** スタッフ */
    private Staff staff;

    /** スタッフID */
    private Integer staffId;

    /** 状態 0: 已申请 1:拒否 2:同意 */
    private Integer state;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;

    private String visaName;
    private DateTime VisaEndTime;
    private int stateForScrollBar;
    private String staffCode;
}
