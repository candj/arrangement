package com.candj.arrangement.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * ユーザールール
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class UserRole {
    /** ID */
    private Integer id;

    /** 管理者 */
    private AppUser appUser;

    /** ユーザーID */
    private Integer userId;

    /** ロール */
    private Role role;

    /** ロールID */
    private Integer roleId;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
