package com.candj.arrangement.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * 签证種類
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class Visa {
    /** スタッフ_签证種類 */
    private java.util.List<StaffVisa> staffVisa;

    /** 签证ID */
    private Integer visaId;

    /** 签证名前 */
    private String visaName;

    /** シフトID */
    private Integer permittedWorkHourPerWeek;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
