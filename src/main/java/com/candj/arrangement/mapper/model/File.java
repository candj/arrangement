package com.candj.arrangement.mapper.model;

import lombok.Getter;
import lombok.Setter;

/**
 * ファイル
 *
 * @author C&J株式会社
 *
 */
@Getter
@Setter
public class File {
    /** ファイルID */
    private Integer fileId;

    /** ストレージ パス */
    private String storePath;

    /** カテゴリ:1:資格外証明書  2:40時間证明  */
    private Integer category;

    /** 状態 0:未読 1:未承認　2:承認 */
    private Integer state;

    /** スタッフ */
    private Staff staff;

    /** スタッフID */
    private Integer staffId;

    /** false:未削除 true:削除 */
    private Boolean deleteFlag;

    /** 登録ユーザー */
    private Integer createUser;

    /** 更新ユーザー */
    private Integer updateUser;

    /** 登録日付 */
    private org.joda.time.DateTime createTime;

    /** 更新日付 */
    private org.joda.time.DateTime updateTime;
}
